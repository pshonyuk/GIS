var config = require("./app/js/source/data.js"),
	resPath = process.cwd() + "\\app\\resourses\\",
	fs = require("fs"),
	lastKey = null,
	configData = {}, 
	textContent ='<h1>\n\t<span></span>\n</h1>\n<div class="body">\n</div>',
	ignoreKeys = {"content": 0, "mainMenu": 0, "main": 0, "bg": 0},
	isRoute = /^#?content\//i,
	defaultConfigOptions = function(){
		return [
			{
				textContent: []
			},
			{
				mediaContent: []
			},
			{
				gmaps: []
			},
			{
				fullPage: []
			},
			{
				contentNavigation: []
			}
		];
	};


var createFile = function(path){
	var filePath = path + "\\textContent.tpl";
	var mediaPath = path + "\\media";
	fs.stat(filePath, function(err, success){
		if(err){
			fs.open(filePath, "w", function(err, file){
				if(err){
					console.log("Error created " + path + "\\textContent.tpl");
				}else{
					fs.write(file, textContent);
				}
			});
			
		}
	});
	fs.stat(mediaPath, function(err){
		if(err){
			fs.mkdir(mediaPath, function(){});
		}
	});
};



JSON.stringify(config, function(key, val){
	var dirName = "";
	if(Object.prototype.toString.call(val) === "[object Array]" && !ignoreKeys.hasOwnProperty(key)){
		lastKey = key;
	}
	if(key === "route" && isRoute.test(val)){
		dirName = val.replace(isRoute, "");
		fs.stat(resPath + dirName, function(err, success){
			if(err){
				console.log("Create: ", dirName);
				fs.mkdir(resPath + dirName, function(err, success){
					if(err){
						console.log("errror create: ", resPath + dirName, err);
					}else{
						createFile(resPath + dirName);
					}
				});
			}else{
				console.log("Continue: ", dirName);
				createFile(resPath + dirName);
			}
		});

		configData[dirName] = defaultConfigOptions();
		if(!lastKey){
			configData[dirName][4].contentNavigation.push(dirName);
		}else{
			configData[dirName][4].contentNavigation.push(lastKey);
		}
	}
	return val;
});


fs.open(process.cwd() + "\\data.json", "w", function(err, file){
	fs.write(file, JSON.stringify(configData, null, 4));
});