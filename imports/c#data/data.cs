﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GIS
{
    public partial class data : Form
    {
        public data()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Просторова інформація в ГІС";
            string path = Application.StartupPath + @"\src\html\b291.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\doslid.jpg");
            browse.ShowDialog();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Географічні дані в ГІС";
            string path = Application.StartupPath + @"\src\html\b292.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\doslid.jpg");
            browse.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Атрибутивні дані у ГІС";
            string path = Application.StartupPath + @"\src\html\b293.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\doslid.jpg");
            browse.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Моделі і бази даних у ГІС";
            string path = Application.StartupPath + @"\src\html\b294.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\doslid.jpg");
            browse.ShowDialog();
        }
    }
}
