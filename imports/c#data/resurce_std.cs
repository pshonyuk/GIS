﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;

namespace GIS
{
    public partial class resurce_std : Form
    {
        public resurce_std()
        {
            InitializeComponent();
        }

        public string res_head
        {
            get { return head_label.Text; }
            set { head_label.Text = value; }
        }

        public string res_top
        {
            get { return top_label.Text; }
            set { top_label.Text = value; }
        }

        public System.Drawing.Font res_topfont
        {
            get { return top_label.Font; }
            set { top_label.Font = value; }
        }

        public string res_bot
        {
            get { return bottom_label.Text; }
            set { bottom_label.Text = value; }
        }

        public System.Drawing.Font res_botfont
        {
            get { return bottom_label.Font; }
            set { bottom_label.Font = value; }
        }

        public int res_pos
        {
            get { return pos.Value; }
            set { pos.Value = value; }
        }

        public System.Windows.Forms.ImageList img_db
        {
            get { return bg_list; }
            set { bg_list = value; }
        }

        public bool map_en
        {
            get { return to_map.Enabled; }
            set { to_map.Enabled = value; }
        }

        private void to_slideshow_Click(object sender, EventArgs e)
        {
            slideshow picture = new slideshow();
            if (res_pos == 1)
            {
                picture.Text = "Геологічні ресурси";
                string[] files = System.IO.Directory.GetFiles(@"src\001", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 40)
            {
                picture.Text = "Ботанічний сад";
                string[] files = System.IO.Directory.GetFiles(@"src\040", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 41)
            {
                picture.Text = "Софіївка";
                string[] files = System.IO.Directory.GetFiles(@"src\041", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 42)
            {
                picture.Text = "Олександрія";
                string[] files = System.IO.Directory.GetFiles(@"src\042", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 43)
            {
                picture.Text = "Тростянець";
                string[] files = System.IO.Directory.GetFiles(@"src\043", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 2)
            {
                picture.Text = "Ландшафтні ресурси";
                string[] files = System.IO.Directory.GetFiles(@"src\002", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 3)
            {
                picture.Text = "Флоро-фауністичні та лісові ресурси";
                string[] files = System.IO.Directory.GetFiles(@"src\003", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 4)
            {
                picture.Text = "Кліматичні ресурси";
                string[] files = System.IO.Directory.GetFiles(@"src\004", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 5)
            {
                picture.Text = "Погодні ресурси";
                string[] files = System.IO.Directory.GetFiles(@"src\005", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 6)
            {
                picture.Text = "Водні ресурси";
                string[] files = System.IO.Directory.GetFiles(@"src\006", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 7)
            {
                picture.Text = "Бальнеологічні ресурси";
                string[] files = System.IO.Directory.GetFiles(@"src\007", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 8)
            {
                picture.Text = "Лісові ресурси";
                string[] files = System.IO.Directory.GetFiles(@"src\008", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 9)
            {
                picture.Text = "Карпатський національний природний парк";
                string[] files = System.IO.Directory.GetFiles(@"src\009", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 10)
            {
                picture.Text = "Національний природний парк Шацький";
                string[] files = System.IO.Directory.GetFiles(@"src\010", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 11)
            {
                picture.Text = "Національний природний парк “Нижньодністровський”";
                string[] files = System.IO.Directory.GetFiles(@"src\011", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 12)
            {
                picture.Text = "Національний природний парк “Святі гори”";
                string[] files = System.IO.Directory.GetFiles(@"src\012", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 13)
            {
                picture.Text = "Національний природний парк “Синевир”";
                string[] files = System.IO.Directory.GetFiles(@"src\013", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 14)
            {
                picture.Text = "Національний природній парк “Голосіївський”";
                string[] files = System.IO.Directory.GetFiles(@"src\014", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 15)
            {
                picture.Text = "Національний природний парк “Гуцульщина”";
                string[] files = System.IO.Directory.GetFiles(@"src\015", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 16)
            {
                picture.Text = "Національний природний парк “Сколівські Бескиди”";
                string[] files = System.IO.Directory.GetFiles(@"src\016", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 17)
            {
                picture.Text = "Гетьманський національний природний парк";
                string[] files = System.IO.Directory.GetFiles(@"src\017", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 18)
            {
                picture.Text = "Дермансько-Острозький національний природний парк";
                string[] files = System.IO.Directory.GetFiles(@"src\018", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 19)
            {
                picture.Text = "Біосферний заповідник \"Асканія-Нова\"";
                string[] files = System.IO.Directory.GetFiles(@"src\019", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 20)
            {
                picture.Text = "Асканійський цілинний заповідний степ";
                string[] files = System.IO.Directory.GetFiles(@"src\020", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 21)
            {
                picture.Text = "Чорноморський біосферний заповідник";
                string[] files = System.IO.Directory.GetFiles(@"src\021", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 22)
            {
                picture.Text = "Карпатський біосферний заповідник";
                string[] files = System.IO.Directory.GetFiles(@"src\022", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 23)
            {
                picture.Text = "Дунайський біосферний заповідник";
                string[] files = System.IO.Directory.GetFiles(@"src\023", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 24)
            {
                picture.Text = "\"Бакайський\"";
                string[] files = System.IO.Directory.GetFiles(@"src\024", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 25)
            {
                picture.Text = "\"Березові гайки\"";
                string[] files = System.IO.Directory.GetFiles(@"src\025", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 26)
            {
                picture.Text = "\"Ягорлицький\"";
                string[] files = System.IO.Directory.GetFiles(@"src\026", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 27)
            {
                picture.Text = "\"Джарилгацький\"";
                string[] files = System.IO.Directory.GetFiles(@"src\027", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 28)
            {
                picture.Text = "\"Саги\"";
                string[] files = System.IO.Directory.GetFiles(@"src\028", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 29)
            {
                picture.Text = "\"Бакайський жолоб\"";
                string[] files = System.IO.Directory.GetFiles(@"src\029", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 30)
            {
                picture.Text = "\"Інгулецький лиман\"";
                string[] files = System.IO.Directory.GetFiles(@"src\030", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 31)
            {
                picture.Text = "\"Хрестова сага\"";
                string[] files = System.IO.Directory.GetFiles(@"src\031", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 32)
            {
                picture.Text = "\"Шаби\"";
                string[] files = System.IO.Directory.GetFiles(@"src\032", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 33)
            {
                picture.Text = "\"Інгулець\"";
                string[] files = System.IO.Directory.GetFiles(@"src\033", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 34)
            {
                picture.Text = "\"Корсунський\"";
                string[] files = System.IO.Directory.GetFiles(@"src\034", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 35)
            {
                picture.Text = "\"Асканійський\"";
                string[] files = System.IO.Directory.GetFiles(@"src\035", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 36)
            {
                picture.Text = "\"Софіївський\"";
                string[] files = System.IO.Directory.GetFiles(@"src\036", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 37)
            {
                picture.Text = "\"Широка Балка\"";
                string[] files = System.IO.Directory.GetFiles(@"src\037", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 38)
            {
                picture.Text = "\"Домузла\"";
                string[] files = System.IO.Directory.GetFiles(@"src\038", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 39)
            {
                picture.Text = "\"Каїрська балка\"";
                string[] files = System.IO.Directory.GetFiles(@"src\039", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 44)
            {
                picture.Text = "Кіровоградський";
                string[] files = System.IO.Directory.GetFiles(@"src\044", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 45)
            {
                picture.Text = "Чернівецький";
                string[] files = System.IO.Directory.GetFiles(@"src\045", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 46)
            {
                picture.Text = "Сторожинецький";
                string[] files = System.IO.Directory.GetFiles(@"src\046", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 47)
            {
                picture.Text = "Березнівський";
                string[] files = System.IO.Directory.GetFiles(@"src\047", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 48)
            {
                picture.Text = "Веселі Боковеньки";
                string[] files = System.IO.Directory.GetFiles(@"src\048", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 49)
            {
                picture.Text = "Полтавський";
                string[] files = System.IO.Directory.GetFiles(@"src\049", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 50)
            {
                picture.Text = "Клесівський";
                string[] files = System.IO.Directory.GetFiles(@"src\050", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 51)
            {
                picture.Text = "Краснокутський";
                string[] files = System.IO.Directory.GetFiles(@"src\051", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 52)
            {
                picture.Text = "Ялтинський зоопарк «Казка»";
                string[] files = System.IO.Directory.GetFiles(@"src\052", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 53)
            {
                picture.Text = "Миколаївський зоопарк";
                string[] files = System.IO.Directory.GetFiles(@"src\053", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 54)
            {
                picture.Text = "Одеський зоопарк";
                string[] files = System.IO.Directory.GetFiles(@"src\054", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 55)
            {
                picture.Text = "Харківський зоопарк";
                string[] files = System.IO.Directory.GetFiles(@"src\055", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 56)
            {
                picture.Text = "Луцький зоопарк";
                string[] files = System.IO.Directory.GetFiles(@"src\056", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 57)
            {
                picture.Text = "Пам'ятки археології";
                string[] files = System.IO.Directory.GetFiles(@"src\057", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 58)
            {
                picture.Text = "Києво-Печерська лавра";
                string[] files = System.IO.Directory.GetFiles(@"src\058", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 59)
            {
                picture.Text = "Державний історико-архітектурний заповідник \"Хотинська фортеця\"";
                string[] files = System.IO.Directory.GetFiles(@"src\059", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 60)
            {
                picture.Text = "Софія Київська";
                string[] files = System.IO.Directory.GetFiles(@"src\060", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 61)
            {
                picture.Text = "Херсонес Таврійський";
                string[] files = System.IO.Directory.GetFiles(@"src\061", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 62)
            {
                picture.Text = "Національний історико-архітектурний заповідник «Кам’янець»";
                string[] files = System.IO.Directory.GetFiles(@"src\062", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 63)
            {
                picture.Text = "До забутих пам’яток України, які дійсно є надзвичайними відносяться";
                string[] files = System.IO.Directory.GetFiles(@"src\063", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 64)
            {
                picture.Text = "Пам’ятки мистецтва";
                string[] files = System.IO.Directory.GetFiles(@"src\064", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 65)
            {
                picture.Text = "Замок Любарта";
                string[] files = System.IO.Directory.GetFiles(@"src\065", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 66)
            {
                picture.Text = "Музей бурштину";
                string[] files = System.IO.Directory.GetFiles(@"src\066", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 67)
            {
                picture.Text = "Кременець, Почаїв";
                string[] files = System.IO.Directory.GetFiles(@"src\067", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 68)
            {
                picture.Text = "Берестечко";
                string[] files = System.IO.Directory.GetFiles(@"src\068", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 69)
            {
                picture.Text = "Музей Лесі Українки";
                string[] files = System.IO.Directory.GetFiles(@"src\069", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 70)
            {
                picture.Text = "Острозький замок";
                string[] files = System.IO.Directory.GetFiles(@"src\070", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 71)
            {
                picture.Text = "Зимненський Свято-Успенський Святогірський жіночий монастир";
                string[] files = System.IO.Directory.GetFiles(@"src\071", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 72)
            {
                picture.Text = "Дубенський замок";
                string[] files = System.IO.Directory.GetFiles(@"src\072", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            else if (res_pos == 73)
            {
                picture.Text = "Пересопниця";
                string[] files = System.IO.Directory.GetFiles(@"src\073", "0*.jpg");
                picture.img_list.Items.Clear();
                foreach (string s in files)
                {
                    picture.img_list.Items.Add(s);
                }
                picture.playpause = Image.FromFile(@"src\play.png");
                picture.start = false;
            }
            picture.count.Maximum = picture.img_list.Items.Count - 1;
            picture.count.Value = 0;
            picture.img = Image.FromFile(picture.img_list.Items[0].ToString());
            picture.ShowDialog();
        }


        private void to_map_Click(object sender, EventArgs e)
        {
            maps maps = new maps();
            if (res_pos == 9) {
                maps.Text = "Карпатський національний природний парк";
                maps.center = new GMap.NET.PointLatLng(48.228986, 24.576759);
                maps.zoom = 10; maps.pos = 10;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(48.228986, 24.576759),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }else if (res_pos == 73)
            {
                maps.Text = "Пересопниця";
                maps.center = new GMap.NET.PointLatLng(50.670524, 25.991141);
                maps.zoom = 13; maps.pos = 13;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.670524, 25.991141),
                  GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 72)
            {
                maps.Text = "Дубенський замок";
                maps.center = new GMap.NET.PointLatLng(50.4194889, 25.7481767);
                maps.zoom = 17; maps.pos = 17;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.4194889, 25.7481767),
                  GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 71)
            {
                maps.Text = "Зимненський Свято-Успенський Святогірський жіночий монастир";
                maps.center = new GMap.NET.PointLatLng(50.801667, 24.3275);
                maps.zoom = 17; maps.pos = 17;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.801667, 24.3275),
                  GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 70)
            {
                maps.Text = "Острозький замок";
                maps.center = new GMap.NET.PointLatLng(50.326680, 26.521292);
                maps.zoom = 17; maps.pos = 17;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.326680, 26.521292),
                  GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 69)
            {
                maps.Text = "Музей Лесі Українки";
                maps.center = new GMap.NET.PointLatLng(51.181431, 24.800058);
                maps.zoom = 13; maps.pos = 13;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(51.181431, 24.800058),
                  GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 68)
            {
                maps.Text = "Берестечко";
                maps.center = new GMap.NET.PointLatLng(50.362936, 25.111581);
                maps.zoom = 13; maps.pos = 13;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.362936, 25.111581),
                  GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 67)
            {
                maps.Text = "Кременець, Почаїв";
                maps.center = new GMap.NET.PointLatLng(50.054921, 25.598660);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.107288, 25.723660),
                  GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                marker = new GMarkerGoogle(new PointLatLng(50.003754, 25.506134),
                  GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 66)
            {
                maps.Text = "Музей бурштину";
                maps.center = new GMap.NET.PointLatLng(50.623010, 26.248918);
                maps.zoom = 18; maps.pos = 18;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.623010, 26.248918),
                  GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 65)
            {
                maps.Text = "Замок Любарта";
                maps.center = new GMap.NET.PointLatLng(50.738914, 25.323439);
                maps.zoom = 18; maps.pos = 18;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.738914, 25.323439),
                  GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 63)
            {
                maps.Text = "До забутих пам’яток України, які дійсно є надзвичайними відносяться";
                maps.center = new GMap.NET.PointLatLng(49.194037, 23.582821);
                maps.zoom = 8; maps.pos = 8;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(48.620556, 22.307675),
                  GMarkerGoogleType.orange);
                marker.ToolTipText = "Лемківська церква Св. Михайла, 1777, село Шелестове";
                markersOverlay.Markers.Add(marker);
                marker = new GMarkerGoogle(new PointLatLng(49.3478, 23.4994),
                  GMarkerGoogleType.orange);
                marker.ToolTipText = "Дрогобич, церква Св. Юра.16 століття";
                markersOverlay.Markers.Add(marker);
                marker = new GMarkerGoogle(new PointLatLng(50.050120, 23.966727),
                  GMarkerGoogleType.orange);
                marker.ToolTipText = "Ц-ва Св. Трійці. м. Жовква";
                markersOverlay.Markers.Add(marker);
                marker = new GMarkerGoogle(new PointLatLng(48.136056, 23.510083),
                  GMarkerGoogleType.orange);
                marker.ToolTipText = "Церква св. Параскеви,с.Олександрівка, Закарпатська обл.";
                markersOverlay.Markers.Add(marker);
                marker = new GMarkerGoogle(new PointLatLng(49.232429, 22.980774),
                  GMarkerGoogleType.orange);
                marker.ToolTipText = "с. Розлуч, портал костелу Св. Франциска Борджія (Львівська обл. Турківський р-н )";
                markersOverlay.Markers.Add(marker);
                marker = new GMarkerGoogle(new PointLatLng(50.117430, 25.124830),
                  GMarkerGoogleType.orange);
                marker.ToolTipText = "с. Язлівчик. Римо-католицька каплиця, доба сецесії, 1936 р.";
                markersOverlay.Markers.Add(marker);
                marker = new GMarkerGoogle(new PointLatLng(48.461667, 23.640556),
                  GMarkerGoogleType.orange);
                marker.ToolTipText = "Церква Св. Архангела Михайла, XVIII ст., 1818 р.с. Негровець, Закарпатська обл.";
                markersOverlay.Markers.Add(marker);
                marker = new GMarkerGoogle(new PointLatLng(48.432531, 23.722352),
                  GMarkerGoogleType.orange);
                marker.ToolTipText = "Церква Св.Духа,1795 р., с. Колочава, Закарпатська обл.";
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 62)
            {
                maps.Text = "Національний історико-архітектурний заповідник «Кам’янець»";
                maps.center = new GMap.NET.PointLatLng(48.673318, 26.574511);
                maps.zoom = 12; maps.pos = 12;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(48.673318, 26.574511),
                  GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 61)
            {
                maps.Text = "Херсонес Таврійський";
                maps.center = new GMap.NET.PointLatLng(44.612479, 33.490233);
                maps.zoom = 16; maps.pos = 16;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(44.612479, 33.490233),
                  GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 60)
            {
                maps.Text = "Софія Київська";
                maps.center = new GMap.NET.PointLatLng(50.452951, 30.514307);
                maps.zoom = 16; maps.pos = 16;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.452951, 30.514307),
                  GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 59)
            {
                maps.Text = "Державний історико-архітектурний заповідник \"Хотинська фортеця\"";
                maps.center = new GMap.NET.PointLatLng(48.521922, 26.498265);
                maps.zoom = 16; maps.pos = 16;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(48.521922, 26.498265),
                  GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 58)
            {
                maps.Text = "Києво-Печерська лавра";
                maps.center = new GMap.NET.PointLatLng(50.434720, 30.557170);
                maps.zoom = 16; maps.pos = 16;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.434720, 30.557170),
                  GMarkerGoogleType.orange);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 56)
            {
                maps.Text = "Луцький зоопарк";
                maps.center = new GMap.NET.PointLatLng(50.741197, 25.338846);
                maps.zoom = 16; maps.pos = 16;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.741197, 25.338846),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 55)
            {
                maps.Text = "Харківський зоопарк";
                maps.center = new GMap.NET.PointLatLng(50.003151, 36.224816);
                maps.zoom = 16; maps.pos = 16;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.003151, 36.224816),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 54)
            {
                maps.Text = "Одеський зоопарк";
                maps.center = new GMap.NET.PointLatLng(46.466513, 30.731839);
                maps.zoom = 16; maps.pos = 16;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.466513, 30.731839),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 53)
            {
                maps.Text = "Миколаївський зоопарк";
                maps.center = new GMap.NET.PointLatLng(46.959837, 32.036744);
                maps.zoom = 16; maps.pos = 16;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.959837, 32.036744),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 52)
            {
                maps.Text = "Ялтинський зоопарк «Казка»";
                maps.center = new GMap.NET.PointLatLng(44.494454, 34.118520);
                maps.zoom = 16; maps.pos = 16;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(44.494454, 34.118520),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 51)
            {
                maps.Text = "Краснокутський";
                maps.center = new GMap.NET.PointLatLng(50.081656, 35.165364);
                maps.zoom = 14; maps.pos = 14;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.081656, 35.165364),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 50)
            {
                maps.Text = "Клесівський";
                maps.center = new GMap.NET.PointLatLng(51.317498, 26.865041);
                maps.zoom = 14; maps.pos = 14;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(51.317498, 26.865041),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 49)
            {
                maps.Text = "Полтавський";
                maps.center = new GMap.NET.PointLatLng(49.619504, 34.553531);
                maps.zoom = 14; maps.pos = 14;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(49.619504, 34.553531),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 41)
            {
                maps.Text = "Софіївка";
                maps.center = new GMap.NET.PointLatLng(48.763343, 30.233408);
                maps.zoom = 13; maps.pos = 13;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(48.763343, 30.233408),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 47)
            {
                maps.Text = "Березнівський";
                maps.center = new GMap.NET.PointLatLng(51.009379, 26.742589);
                maps.zoom = 15; maps.pos = 15;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(51.009379, 26.742589),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 48)
            {
                maps.Text = "Веселі Боковеньки";
                maps.center = new GMap.NET.PointLatLng(48.217389, 32.859344);
                maps.zoom = 13; maps.pos = 13;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(48.217389, 32.859344),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 42)
            {
                maps.Text = "Олександрія";
                maps.center = new GMap.NET.PointLatLng(49.812222, 30.067222);
                maps.zoom = 13; maps.pos = 13;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(49.812222, 30.067222),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 43)
            {
                maps.Text = "Тростянець";
                maps.center = new GMap.NET.PointLatLng(50.789990, 32.817660);
                maps.zoom = 13; maps.pos = 13;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.789990, 32.817660),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 44)
            {
                maps.Text = "Кіровоградський";
                maps.center = new GMap.NET.PointLatLng(48.499927, 32.231633);
                maps.zoom = 13; maps.pos = 13;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(48.499927, 32.231633),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 45)
            {
                maps.Text = "Чернівецький";
                maps.center = new GMap.NET.PointLatLng(48.298199, 25.922986);
                maps.zoom = 15; maps.pos = 15;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(48.298199, 25.922986),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 46)
            {
                maps.Text = "Сторожинецький";
                maps.center = new GMap.NET.PointLatLng(48.145832, 25.682266);
                maps.zoom = 15; maps.pos = 15;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(48.145832, 25.682266),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 10)
            {
                maps.Text = "Національний природний парк Шацький";
                maps.center = new GMap.NET.PointLatLng(51.530014, 23.810484);
                maps.zoom = 10; maps.pos = 10;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(51.530014, 23.810484),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            } else if (res_pos == 11)
            {
                maps.Text = "Національний природний парк “Нижньодністровський”";
                maps.center = new GMap.NET.PointLatLng(46.391824, 30.209398);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.391824, 30.209398),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
             } else if (res_pos == 12)
             {
                maps.Text = "Національний природний парк “Святі гори”";
                maps.center = new GMap.NET.PointLatLng(48.979434, 37.708751);
                maps.zoom = 10; maps.pos = 10;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(48.979434, 37.708751),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
             }
            else if (res_pos == 13)
            {
                maps.Text = "Національний природний парк “Синевир”";
                maps.center = new GMap.NET.PointLatLng(48.512494, 23.708839);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(48.512494, 23.708839),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 14)
            {
                maps.Text = "Національний природній парк “Голосіївський”";
                maps.center = new GMap.NET.PointLatLng(50.290538, 30.570147);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.290538, 30.570147),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 15)
            {
                maps.Text = "Національний природний парк “Гуцульщина”";
                maps.center = new GMap.NET.PointLatLng(48.309927, 24.973297);
                maps.zoom = 10; maps.pos = 10;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(48.309927, 24.973297),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 16)
            {
                maps.Text = "Національний природний парк “Сколівські Бескиди”";
                maps.center = new GMap.NET.PointLatLng(49.078023, 23.399803);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(49.078023, 23.399803),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 17)
            {
                maps.Text = "Гетьманський національний природний парк";
                maps.center = new GMap.NET.PointLatLng(50.348255, 34.939878);
                maps.zoom = 10; maps.pos = 10;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.348255, 34.939878),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 18)
            {
                maps.Text = "Дермансько-Острозький національний природний парк";
                maps.center = new GMap.NET.PointLatLng(50.291253, 26.242454);
                maps.zoom = 12; maps.pos = 12;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.291253, 26.242454),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 19)
            {
                maps.Text = "Біосферний заповідник \"Асканія-Нова\"";
                maps.center = new GMap.NET.PointLatLng(46.472759, 33.963216);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.472759, 33.963216),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 20)
            {
                maps.Text = "Асканійський цілинний заповідний степ";
                maps.center = new GMap.NET.PointLatLng(46.472759, 33.963216);
                maps.zoom = 10; maps.pos = 10;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.472759, 33.963216),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 21)
            {
                maps.Text = "Чорноморський біосферний заповідник";
                maps.center = new GMap.NET.PointLatLng(46.5375, 32.538889);
                maps.zoom = 10; maps.pos = 10;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.5375, 32.538889),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 22)
            {
                maps.Text = "Карпатський біосферний заповідник";
                maps.center = new GMap.NET.PointLatLng(48.119446, 24.480114);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(48.119446, 24.480114),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 23)
            {
                maps.Text = "Дунайський біосферний заповідник";
                maps.center = new GMap.NET.PointLatLng(45.412180, 29.644072);
                maps.zoom = 10; maps.pos = 10;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(45.412180, 29.644072),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 24)
            {
                maps.Text = "\"Бакайський\"";
                maps.center = new GMap.NET.PointLatLng(46.5, 32.3);
                maps.zoom = 12; maps.pos = 12;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.5, 32.3),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 25)
            {
                maps.Text = "\"Березові гайки\"";
                maps.center = new GMap.NET.PointLatLng(46.443766, 32.222501);
                maps.zoom = 12; maps.pos = 12;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.443766, 32.222501),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 26)
            {
                maps.Text = "\"Ягорлицький\"";
                maps.center = new GMap.NET.PointLatLng(46.4, 31.866667);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.4, 31.866667),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 27)
            {
                maps.Text = "\"Джарилгацький\"";
                maps.center = new GMap.NET.PointLatLng(46.027040, 32.894768);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.027040, 32.894768),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 28)
            {
                maps.Text = "\"Саги\"";
                maps.center = new GMap.NET.PointLatLng(46.601897, 32.786530);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.601897, 32.786530),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 29)
            {
                maps.Text = "\"Бакайський жолоб\"";
                maps.center = new GMap.NET.PointLatLng(46.533333, 32.316667);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.533333, 32.316667),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 30)
            {
                maps.Text = "\"Інгулецький лиман\"";
                maps.center = new GMap.NET.PointLatLng(46.697502, 32.829981);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.697502, 32.829981),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 31)
            {
                maps.Text = "\"Хрестова сага\"";
                maps.center = new GMap.NET.PointLatLng(46.447521, 32.007836);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.447521, 32.007836),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 32)
            {
                maps.Text = "\"Шаби\"";
                maps.center = new GMap.NET.PointLatLng(46.466667, 32.15);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.466667, 32.15),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 33)
            {
                maps.Text = "\"Інгулець\"";
                maps.center = new GMap.NET.PointLatLng(47.347778, 33.310278);
                maps.zoom = 12; maps.pos = 12;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(47.347778, 33.310278),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 34)
            {
                maps.Text = "\"Корсунський\"";
                maps.center = new GMap.NET.PointLatLng(46.7, 33.133333);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.7, 33.133333),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 35)
            {
                maps.Text = "\"Асканійський\"";
                maps.center = new GMap.NET.PointLatLng(46.416667, 33.983333);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.416667, 33.983333),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 36)
            {
                maps.Text = "\"Софіївський\"";
                maps.center = new GMap.NET.PointLatLng(46.587267, 32.225898);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.587267, 32.225898),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 37)
            {
                maps.Text = "\"Широка Балка\"";
                maps.center = new GMap.NET.PointLatLng(46.566667, 32.2);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.566667, 32.2),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 38)
            {
                maps.Text = "\"Домузла\"";
                maps.center = new GMap.NET.PointLatLng(46.15, 33.333333);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.15, 33.333333),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 39)
            {
                maps.Text = "\"Каїрська балка\"";
                maps.center = new GMap.NET.PointLatLng(46.928056, 33.785278);
                maps.zoom = 11; maps.pos = 11;
                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.928056, 33.785278),
                  GMarkerGoogleType.green);
                markersOverlay.Markers.Add(marker);
                maps.map.Overlays.Add(markersOverlay);
            }
            else if (res_pos == 1)
             {
                maps.Text = "Геологічні ресурси";
                maps.center = new GMap.NET.PointLatLng(49.783669, 30.112287);
                maps.zoom = 6; maps.pos = 6;
                GMapOverlay polyOverlay = new GMapOverlay("polygons");
                List<GMap.NET.PointLatLng> points = new List<GMap.NET.PointLatLng>();
                points.Add(new PointLatLng(48.469268, 27.528641)); points.Add(new PointLatLng(48.455202, 27.478183)); points.Add(new PointLatLng(48.411466, 27.439731));
                points.Add(new PointLatLng(48.415112, 27.384799)); points.Add(new PointLatLng(48.444271, 27.373813)); points.Add(new PointLatLng(48.444271, 27.318881));
                points.Add(new PointLatLng(48.378639, 27.285922)); points.Add(new PointLatLng(48.413445, 26.717634)); points.Add(new PointLatLng(48.256421, 26.613264));
                points.Add(new PointLatLng(48.186885, 26.327620)); points.Add(new PointLatLng(48.003442, 26.187544)); points.Add(new PointLatLng(47.977708, 25.822248));
                points.Add(new PointLatLng(47.904111, 25.234480)); points.Add(new PointLatLng(47.771372, 25.124617)); points.Add(new PointLatLng(47.811967, 24.808760));
                points.Add(new PointLatLng(47.870958, 24.671430)); points.Add(new PointLatLng(47.974030, 24.561567)); points.Add(new PointLatLng(48.054872, 24.638471));
                points.Add(new PointLatLng(48.296636, 24.468183)); points.Add(new PointLatLng(48.395211, 24.292402)); points.Add(new PointLatLng(48.369673, 24.182539));
                points.Add(new PointLatLng(48.736912, 23.539839)); points.Add(new PointLatLng(48.776748, 23.188276)); points.Add(new PointLatLng(48.859938, 23.100386));
                points.Add(new PointLatLng(48.841865, 22.985029)); points.Add(new PointLatLng(49.015097, 22.886152)); points.Add(new PointLatLng(49.180548, 22.726850));
                points.Add(new PointLatLng(49.524061, 22.666426)); points.Add(new PointLatLng(50.147165, 23.345003)); points.Add(new PointLatLng(50.382878, 23.712873));
                points.Add(new PointLatLng(50.417892, 23.976545)); points.Add(new PointLatLng(50.634405, 24.091902)); points.Add(new PointLatLng(50.529765, 24.569807));
                points.Add(new PointLatLng(50.407391, 24.602766)); points.Add(new PointLatLng(50.295229, 25.108137)); points.Add(new PointLatLng(50.006603, 25.333357));
                points.Add(new PointLatLng(49.886417, 25.388288)); points.Add(new PointLatLng(49.815580, 25.102644)); points.Add(new PointLatLng(49.491958, 24.729109));
                points.Add(new PointLatLng(49.018700, 24.959822)); points.Add(new PointLatLng(48.668031, 25.635481)); points.Add(new PointLatLng(48.529986, 26.371565));
                points.Add(new PointLatLng(48.689793, 26.234236)); points.Add(new PointLatLng(49.245139, 26.190290)); points.Add(new PointLatLng(50.161692, 26.234236));
                points.Add(new PointLatLng(50.470365, 26.734114)); points.Add(new PointLatLng(50.568160, 27.179060)); points.Add(new PointLatLng(50.487844, 27.283430));
                points.Add(new PointLatLng(50.239048, 27.536116)); points.Add(new PointLatLng(49.737539, 27.788801)); points.Add(new PointLatLng(49.875798, 28.870954));
                points.Add(new PointLatLng(49.613124, 28.991804)); points.Add(new PointLatLng(49.666483, 29.497175)); points.Add(new PointLatLng(49.808491, 29.436750));
                points.Add(new PointLatLng(49.950082, 29.738874)); points.Add(new PointLatLng(50.333814, 29.678450)); points.Add(new PointLatLng(50.435390, 29.475202));
                points.Add(new PointLatLng(50.655305, 29.464216)); points.Add(new PointLatLng(51.047190, 29.508161)); points.Add(new PointLatLng(51.260808, 29.271955));
                points.Add(new PointLatLng(51.411809, 29.403791)); points.Add(new PointLatLng(51.521316, 29.722395)); points.Add(new PointLatLng(51.442635, 29.782820));
                points.Add(new PointLatLng(51.507642, 30.183821)); points.Add(new PointLatLng(51.284865, 30.441999)); points.Add(new PointLatLng(51.019554, 30.513410));
                points.Add(new PointLatLng(50.787457, 30.749617)); points.Add(new PointLatLng(50.763141, 31.128645)); points.Add(new PointLatLng(50.547221, 31.265974));
                points.Add(new PointLatLng(50.578626, 31.936140)); points.Add(new PointLatLng(50.361857, 32.161360)); points.Add(new PointLatLng(50.421392, 32.359114));
                points.Add(new PointLatLng(50.347838, 32.793074)); points.Add(new PointLatLng(50.526273, 33.067732)); points.Add(new PointLatLng(50.466869, 33.770857));
                points.Add(new PointLatLng(50.529765, 34.232283)); points.Add(new PointLatLng(50.133531, 34.671736)); points.Add(new PointLatLng(50.151133, 34.924421));
                points.Add(new PointLatLng(49.932406, 34.891462)); points.Add(new PointLatLng(49.655816, 35.446272)); points.Add(new PointLatLng(49.291736, 35.275984));
                points.Add(new PointLatLng(49.323970, 35.028791)); points.Add(new PointLatLng(49.169774, 35.001326)); points.Add(new PointLatLng(48.946598, 35.792341));
                points.Add(new PointLatLng(48.989871, 35.962629)); points.Add(new PointLatLng(48.533624, 36.330671)); points.Add(new PointLatLng(48.649889, 36.495466));
                points.Add(new PointLatLng(48.540898, 36.896467)); points.Add(new PointLatLng(48.043856, 36.863508)); points.Add(new PointLatLng(48.084237, 36.605329));
                points.Add(new PointLatLng(47.845158, 36.577864)); points.Add(new PointLatLng(47.859902, 36.116438)); points.Add(new PointLatLng(48.073227, 36.012068));
                points.Add(new PointLatLng(48.127375, 34.883222)); points.Add(new PointLatLng(47.552205, 34.943647)); points.Add(new PointLatLng(47.478004, 33.976850));
                points.Add(new PointLatLng(47.615193, 33.004560)); points.Add(new PointLatLng(48.024609, 33.043012)); points.Add(new PointLatLng(48.237254, 33.537397));
                points.Add(new PointLatLng(48.554572, 33.482466)); points.Add(new PointLatLng(48.681671, 33.817549)); points.Add(new PointLatLng(48.909642, 33.894453));
                points.Add(new PointLatLng(48.967375, 33.323164)); points.Add(new PointLatLng(49.075444, 33.262739)); points.Add(new PointLatLng(49.348168, 32.477217));
                points.Add(new PointLatLng(49.526772, 32.762861)); points.Add(new PointLatLng(49.864328, 32.411299)); points.Add(new PointLatLng(50.125644, 32.284956));
                points.Add(new PointLatLng(50.220636, 32.103681)); points.Add(new PointLatLng(49.973971, 31.829023)); points.Add(new PointLatLng(49.857246, 31.664228));
                points.Add(new PointLatLng(49.867869, 31.219282)); points.Add(new PointLatLng(49.423261, 31.005049)); points.Add(new PointLatLng(49.358902, 30.461225));
                points.Add(new PointLatLng(49.251451, 30.367842)); points.Add(new PointLatLng(49.315950, 29.972334)); points.Add(new PointLatLng(49.140172, 29.725141));
                points.Add(new PointLatLng(48.945733, 29.648237)); points.Add(new PointLatLng(48.656277, 29.966841)); points.Add(new PointLatLng(48.485442, 29.966841));
                points.Add(new PointLatLng(48.372448, 29.791059)); points.Add(new PointLatLng(48.218958, 29.780073)); points.Add(new PointLatLng(48.112707, 29.659223));
                points.Add(new PointLatLng(48.101703, 29.313154)); points.Add(new PointLatLng(48.131042, 29.038496)); points.Add(new PointLatLng(48.079688, 28.857221));
                points.Add(new PointLatLng(48.175430, 28.563508)); points.Add(new PointLatLng(48.076430, 28.481111)); points.Add(new PointLatLng(48.292516, 28.102083));
                GMapPolygon polygon = new GMapPolygon(points, "mypolygon");
                polygon.Fill = new SolidBrush(Color.FromArgb(50, Color.Red));
                polygon.Stroke = new Pen(Color.Red, 1);
                polyOverlay.Polygons.Add(polygon);
                maps.map.Overlays.Add(polyOverlay);
            }
            else if (res_pos == 2) {
                maps.Text = "Ландшафтні ресурси";
                maps.center = new GMap.NET.PointLatLng(47.617257, 29.216922);
                maps.zoom = 6; maps.pos = 6;
                GMapOverlay polyOverlay = new GMapOverlay("polygons");
                List<GMap.NET.PointLatLng> points = new List<GMap.NET.PointLatLng>();
                points.Add(new PointLatLng(49.642719, 22.767948)); points.Add(new PointLatLng(49.528758, 22.658084)); points.Add(new PointLatLng(49.185278, 22.734989));
                points.Add(new PointLatLng(49.106226, 22.877811)); points.Add(new PointLatLng(49.005432, 22.877811)); points.Add(new PointLatLng(49.084645, 22.570194));
                points.Add(new PointLatLng(48.701822, 22.339481)); points.Add(new PointLatLng(48.571139, 22.427371)); points.Add(new PointLatLng(48.147747, 23.844608));
                points.Add(new PointLatLng(47.927370, 24.152225)); points.Add(new PointLatLng(47.964165, 24.415897)); points.Add(new PointLatLng(47.971521, 24.591678));
                points.Add(new PointLatLng(47.743001, 24.910282)); points.Add(new PointLatLng(47.772544, 25.119022)); points.Add(new PointLatLng(47.912644, 25.239872));
                points.Add(new PointLatLng(48.483830, 24.745487)); points.Add(new PointLatLng(48.853857, 24.130252)); points.Add(new PointLatLng(49.300038, 23.449100));
                List<GMap.NET.PointLatLng> points2 = new List<GMap.NET.PointLatLng>();
                points2.Add(new PointLatLng(44.867533, 33.600467)); points2.Add(new PointLatLng(44.976440, 34.325565)); points2.Add(new PointLatLng(45.116160, 34.973758));
                points2.Add(new PointLatLng(45.023051, 35.160526)); points2.Add(new PointLatLng(44.976440, 35.237430)); points2.Add(new PointLatLng(44.805074, 35.104532));
                points2.Add(new PointLatLng(44.797279, 34.676065)); points2.Add(new PointLatLng(44.719267, 34.434366)); points2.Add(new PointLatLng(44.562927, 34.313516));
                points2.Add(new PointLatLng(44.436697, 34.080110)); points2.Add(new PointLatLng(44.405311, 33.728548)); points2.Add(new PointLatLng(44.507255, 33.574739));
                points2.Add(new PointLatLng(44.585553, 33.409944)); points2.Add(new PointLatLng(44.655931, 33.530794));
                GMapPolygon polygon = new GMapPolygon(points, "mypolygon");
                GMapPolygon polygon2 = new GMapPolygon(points2, "mypolygon2");
                polygon.Fill = new SolidBrush(Color.FromArgb(60, Color.Green));
                polygon2.Fill = new SolidBrush(Color.FromArgb(60, Color.Green));
                polygon.Stroke = new Pen(Color.Green, 1);
                polygon2.Stroke = new Pen(Color.Green, 1);
                polyOverlay.Polygons.Add(polygon);
                polyOverlay.Polygons.Add(polygon2);
                maps.map.Overlays.Add(polyOverlay);
            }
            else if (res_pos == 3) {
                maps.Text = "Флоро-фауністичні та лісові ресурси";
                maps.center = new GMap.NET.PointLatLng(46.687957, 36.214656);
                maps.zoom = 8; maps.pos = 8;
                GMapOverlay polyOverlay = new GMapOverlay("polygons");
                List<GMap.NET.PointLatLng> points = new List<GMap.NET.PointLatLng>();
                points.Add(new PointLatLng(47.306248, 38.336639)); points.Add(new PointLatLng(47.306248, 38.185577)); points.Add(new PointLatLng(47.288552, 38.063354));
                points.Add(new PointLatLng(47.345344, 38.016662)); points.Add(new PointLatLng(47.315559, 37.820282)); points.Add(new PointLatLng(47.252211, 37.748871));
                points.Add(new PointLatLng(47.234497, 37.588196)); points.Add(new PointLatLng(47.368881, 37.584920)); points.Add(new PointLatLng(47.465523, 37.242971));
                points.Add(new PointLatLng(47.351206, 37.131734)); points.Add(new PointLatLng(47.315839, 36.927114)); points.Add(new PointLatLng(47.187201, 36.872182));
                points.Add(new PointLatLng(47.162929, 36.927114)); points.Add(new PointLatLng(47.183467, 37.119375)); points.Add(new PointLatLng(47.141449, 37.085042));
                points.Add(new PointLatLng(47.111550, 37.122121)); points.Add(new PointLatLng(47.119026, 37.302022)); points.Add(new PointLatLng(47.191867, 37.377553));
                points.Add(new PointLatLng(47.180667, 37.531362)); points.Add(new PointLatLng(47.163582, 37.654114)); points.Add(new PointLatLng(47.083222, 37.674713));
                points.Add(new PointLatLng(47.086028, 37.795563)); points.Add(new PointLatLng(47.111269, 37.894440)); points.Add(new PointLatLng(47.087898, 37.978210));
                points.Add(new PointLatLng(47.091638, 38.035889)); points.Add(new PointLatLng(47.108465, 38.086700)); points.Add(new PointLatLng(47.049547, 38.136139));
                points.Add(new PointLatLng(47.120614, 38.230896)); points.Add(new PointLatLng(47.230767, 38.236389)); points.Add(new PointLatLng(47.256871, 38.287201));
                points.Add(new PointLatLng(47.258735, 38.325653)); points.Add(new PointLatLng(47.282963, 38.321533));
                List<GMap.NET.PointLatLng> points2 = new List<GMap.NET.PointLatLng>();
                points2.Add(new PointLatLng(46.661986, 35.921112)); points2.Add(new PointLatLng(46.724155, 35.904633)); points2.Add(new PointLatLng(46.759917, 36.028229));
                points2.Add(new PointLatLng(46.812575, 36.030976)); points2.Add(new PointLatLng(46.827610, 36.091401)); points2.Add(new PointLatLng(46.872692, 36.138092));
                points2.Add(new PointLatLng(46.906479, 36.047455)); points2.Add(new PointLatLng(46.985231, 36.063935)); points2.Add(new PointLatLng(46.981484, 35.987030));
                points2.Add(new PointLatLng(47.037669, 35.995270)); points2.Add(new PointLatLng(47.073222, 35.923859)); points2.Add(new PointLatLng(47.002092, 35.888153));
                points2.Add(new PointLatLng(46.996472, 35.764557)); points2.Add(new PointLatLng(46.878325, 35.613495)); points2.Add(new PointLatLng(46.801295, 35.608002));
                points2.Add(new PointLatLng(46.692138, 35.344330)); points2.Add(new PointLatLng(46.727121, 35.349538)); points2.Add(new PointLatLng(46.759118, 35.176503));
                points2.Add(new PointLatLng(46.719590, 34.938924)); points2.Add(new PointLatLng(46.824934, 34.937550)); points2.Add(new PointLatLng(46.827752, 34.757649));
                points2.Add(new PointLatLng(46.759118, 34.741170)); points2.Add(new PointLatLng(46.724763, 34.626482)); points2.Add(new PointLatLng(46.688036, 34.633349));
                points2.Add(new PointLatLng(46.681441, 34.741839)); points2.Add(new PointLatLng(46.583369, 34.722613)); points2.Add(new PointLatLng(46.527652, 34.472674));
                points2.Add(new PointLatLng(46.465258, 34.482287)); points2.Add(new PointLatLng(46.469041, 34.559191)); points2.Add(new PointLatLng(46.427408, 34.567431));
                points2.Add(new PointLatLng(46.434033, 34.681414)); points2.Add(new PointLatLng(46.351629, 34.697893)); points2.Add(new PointLatLng(46.342150, 34.634722));
                points2.Add(new PointLatLng(46.269101, 34.633349)); points2.Add(new PointLatLng(46.183596, 34.401263)); points2.Add(new PointLatLng(46.095101, 34.393023));
                points2.Add(new PointLatLng(46.022676, 34.416369)); points2.Add(new PointLatLng(45.959704, 34.476794)); points2.Add(new PointLatLng(46.005509, 34.681414));
                points2.Add(new PointLatLng(45.818234, 34.822863)); points2.Add(new PointLatLng(45.759821, 34.964312)); points2.Add(new PointLatLng(45.765569, 34.975298));
                points2.Add(new PointLatLng(46.157918, 34.822863)); points2.Add(new PointLatLng(46.204509, 34.836596)); points2.Add(new PointLatLng(46.243463, 34.897021));
                points2.Add(new PointLatLng(46.254859, 35.002764)); points2.Add(new PointLatLng(46.270050, 35.061815)); points2.Add(new PointLatLng(46.308009, 35.114001));
                points2.Add(new PointLatLng(46.352577, 35.141466)); points2.Add(new PointLatLng(46.377216, 35.254076)); points2.Add(new PointLatLng(46.301368, 35.293902));
                points2.Add(new PointLatLng(46.177891, 35.167559)); points2.Add(new PointLatLng(46.152211, 35.039843)); points2.Add(new PointLatLng(46.107479, 34.990404));
                points2.Add(new PointLatLng(46.079861, 34.984911)); points2.Add(new PointLatLng(46.108432, 35.112627)); points2.Add(new PointLatLng(46.150308, 35.190905));
                points2.Add(new PointLatLng(46.304215, 35.317248)); points2.Add(new PointLatLng(46.442551, 35.460070)); points2.Add(new PointLatLng(46.478499, 35.583666));
                points2.Add(new PointLatLng(46.530487, 35.661944)); points2.Add(new PointLatLng(46.589031, 35.727862)); points2.Add(new PointLatLng(46.629461, 35.844215));
                GMapPolygon polygon = new GMapPolygon(points, "mypolygon");
                GMapPolygon polygon2 = new GMapPolygon(points2, "mypolygon2");
                polygon.Fill = new SolidBrush(Color.FromArgb(60, Color.Green));
                polygon.Stroke = new Pen(Color.Green, 1);
                polyOverlay.Polygons.Add(polygon);
                polygon2.Fill = new SolidBrush(Color.FromArgb(60, Color.Green));
                polygon2.Stroke = new Pen(Color.Green, 1);
                polyOverlay.Polygons.Add(polygon2);
                maps.map.Overlays.Add(polyOverlay);
            }
            else if (res_pos == 4) {
                maps.Text = "Кліматичні ресурси";
                maps.center = new GMap.NET.PointLatLng(47.617257, 29.216922);
                maps.zoom = 6; maps.pos = 6;
                GMapOverlay polyOverlay = new GMapOverlay("polygons");
                List<GMap.NET.PointLatLng> points = new List<GMap.NET.PointLatLng>();
                points.Add(new PointLatLng(49.642719, 22.767948));
                points.Add(new PointLatLng(49.528758, 22.658084));
                points.Add(new PointLatLng(49.185278, 22.734989));
                points.Add(new PointLatLng(49.106226, 22.877811));
                points.Add(new PointLatLng(49.005432, 22.877811));
                points.Add(new PointLatLng(49.084645, 22.570194));
                points.Add(new PointLatLng(48.701822, 22.339481));
                points.Add(new PointLatLng(48.571139, 22.427371));
                points.Add(new PointLatLng(48.147747, 23.844608));
                points.Add(new PointLatLng(47.927370, 24.152225));
                points.Add(new PointLatLng(47.964165, 24.415897));
                points.Add(new PointLatLng(47.971521, 24.591678));
                points.Add(new PointLatLng(47.743001, 24.910282));
                points.Add(new PointLatLng(47.772544, 25.119022));
                points.Add(new PointLatLng(47.912644, 25.239872));
                points.Add(new PointLatLng(48.483830, 24.745487));
                points.Add(new PointLatLng(48.853857, 24.130252));
                points.Add(new PointLatLng(49.300038, 23.449100));
                List<GMap.NET.PointLatLng> points2 = new List<GMap.NET.PointLatLng>();
                points2.Add(new PointLatLng(44.867533, 33.600467));
                points2.Add(new PointLatLng(44.976440, 34.325565));
                points2.Add(new PointLatLng(45.116160, 34.973758));
                points2.Add(new PointLatLng(45.023051, 35.160526));
                points2.Add(new PointLatLng(44.976440, 35.237430));
                points2.Add(new PointLatLng(44.805074, 35.104532));
                points2.Add(new PointLatLng(44.797279, 34.676065));
                points2.Add(new PointLatLng(44.719267, 34.434366));
                points2.Add(new PointLatLng(44.562927, 34.313516));
                points2.Add(new PointLatLng(44.436697, 34.080110));
                points2.Add(new PointLatLng(44.405311, 33.728548));
                points2.Add(new PointLatLng(44.507255, 33.574739));
                points2.Add(new PointLatLng(44.585553, 33.409944));
                points2.Add(new PointLatLng(44.655931, 33.530794));
                GMapPolygon polygon = new GMapPolygon(points, "mypolygon");
                GMapPolygon polygon2 = new GMapPolygon(points2, "mypolygon2");
                polygon.Fill = new SolidBrush(Color.FromArgb(60, Color.Blue));
                polygon2.Fill = new SolidBrush(Color.FromArgb(60, Color.Blue));
                polygon.Stroke = new Pen(Color.Blue, 1);
                polygon2.Stroke = new Pen(Color.Blue, 1);
                polyOverlay.Polygons.Add(polygon);
                polyOverlay.Polygons.Add(polygon2);
                maps.map.Overlays.Add(polyOverlay);
            }
            else if (res_pos == 7) {
                maps.Text = "Бальнеологічні ресурси";
                maps.center = new GMap.NET.PointLatLng(47.617257, 29.216922);
                maps.zoom = 6; maps.pos = 6;

                GMapOverlay polyOverlay = new GMapOverlay("polygons");
                List<GMap.NET.PointLatLng> points = new List<GMap.NET.PointLatLng>();
                points.Add(new PointLatLng(49.005145, 22.870902));
                points.Add(new PointLatLng(49.102344, 22.870902));
                points.Add(new PointLatLng(49.188583, 22.717093));
                points.Add(new PointLatLng(49.242406, 22.717093));
                points.Add(new PointLatLng(49.367766, 22.744559));
                points.Add(new PointLatLng(49.492807, 22.695120));
                points.Add(new PointLatLng(49.528475, 22.634696));
                points.Add(new PointLatLng(49.841228, 22.958792));
                points.Add(new PointLatLng(50.109715, 23.277396));
                points.Add(new PointLatLng(50.383711, 23.705863));
                points.Add(new PointLatLng(50.418725, 23.837698));
                points.Add(new PointLatLng(50.642202, 24.090384));
                points.Add(new PointLatLng(50.541070, 24.540824));
                points.Add(new PointLatLng(50.418725, 24.584769));
                points.Add(new PointLatLng(50.348672, 24.727591));
                points.Add(new PointLatLng(50.369699, 24.963797));
                points.Add(new PointLatLng(50.282025, 25.194510));
                points.Add(new PointLatLng(50.120283, 25.210990));
                points.Add(new PointLatLng(49.982724, 25.403250));
                points.Add(new PointLatLng(49.876641, 25.381278));
                points.Add(new PointLatLng(49.812879, 25.095633));
                points.Add(new PointLatLng(49.624647, 24.958304));
                points.Add(new PointLatLng(49.567679, 24.716605));
                points.Add(new PointLatLng(49.353455, 24.848441));
                points.Add(new PointLatLng(49.026761, 24.952811));
                points.Add(new PointLatLng(48.719657, 25.644949));
                points.Add(new PointLatLng(48.381488, 25.606497));
                points.Add(new PointLatLng(48.407020, 25.463675));
                points.Add(new PointLatLng(48.026364, 24.914359));
                points.Add(new PointLatLng(47.860779, 24.996756));
                points.Add(new PointLatLng(47.731624, 24.925345));
                points.Add(new PointLatLng(47.823911, 24.815482));
                points.Add(new PointLatLng(47.864465, 24.667166));
                points.Add(new PointLatLng(47.971228, 24.562796));
                points.Add(new PointLatLng(48.052074, 24.628714));
                points.Add(new PointLatLng(48.388784, 24.321097));
                points.Add(new PointLatLng(48.366892, 24.183768));
                points.Add(new PointLatLng(48.523576, 24.112357));
                points.Add(new PointLatLng(48.468974, 23.947562));
                points.Add(new PointLatLng(48.639864, 23.782767));
                points.Add(new PointLatLng(48.730528, 23.541068));
                points.Add(new PointLatLng(48.773989, 23.184012));
                points.Add(new PointLatLng(48.860798, 23.090628));
                points.Add(new PointLatLng(48.842725, 22.969779));
                points.Add(new PointLatLng(48.951064, 22.865408));

                List<GMap.NET.PointLatLng> points2 = new List<GMap.NET.PointLatLng>();
                points2.Add(new PointLatLng(48.074101, 28.847464));
                points2.Add(new PointLatLng(48.187756, 29.078177));
                points2.Add(new PointLatLng(48.096118, 29.292410));
                points2.Add(new PointLatLng(48.121793, 29.654959));
                points2.Add(new PointLatLng(48.202403, 29.649466));
                points2.Add(new PointLatLng(48.220706, 29.984549));
                points2.Add(new PointLatLng(48.143790, 30.083426));
                points2.Add(new PointLatLng(48.143790, 30.297659));
                points2.Add(new PointLatLng(48.092449, 30.314139));
                points2.Add(new PointLatLng(48.070430, 30.220755));
                points2.Add(new PointLatLng(47.823911, 30.325125));
                points2.Add(new PointLatLng(47.827599, 30.424002));
                points2.Add(new PointLatLng(47.761174, 30.429495));
                points2.Add(new PointLatLng(47.746401, 30.380057));
                points2.Add(new PointLatLng(47.628069, 30.445975));
                points2.Add(new PointLatLng(47.642876, 30.770072));
                points2.Add(new PointLatLng(47.297547, 30.912894));
                points2.Add(new PointLatLng(47.267735, 31.176566));
                points2.Add(new PointLatLng(47.028636, 31.302908));
                points2.Add(new PointLatLng(46.961196, 31.011771));
                points2.Add(new PointLatLng(46.822301, 31.149100));
                points2.Add(new PointLatLng(46.630262, 31.176566));
                points2.Add(new PointLatLng(46.532097, 30.764578));
                points2.Add(new PointLatLng(46.384515, 30.753592));
                points2.Add(new PointLatLng(46.130044, 30.522879));
                points2.Add(new PointLatLng(45.847613, 30.209769));
                points2.Add(new PointLatLng(45.498324, 29.622000));
                points2.Add(new PointLatLng(45.486772, 29.742850));
                points2.Add(new PointLatLng(45.239761, 29.759329));
                points2.Add(new PointLatLng(45.208809, 29.682425));
                points2.Add(new PointLatLng(45.340239, 29.649466));
                points2.Add(new PointLatLng(45.440539, 29.418753));
                points2.Add(new PointLatLng(45.425120, 29.215506));
                points2.Add(new PointLatLng(45.282293, 28.940848));
                points2.Add(new PointLatLng(45.332516, 28.809012));
                points2.Add(new PointLatLng(45.239761, 28.721121));
                points2.Add(new PointLatLng(45.320930, 28.347586));
                points2.Add(new PointLatLng(45.471365, 28.210257));
                points2.Add(new PointLatLng(45.548356, 28.298148));
                points2.Add(new PointLatLng(45.490623, 28.418997));
                points2.Add(new PointLatLng(45.509874, 28.512381));
                points2.Add(new PointLatLng(45.728869, 28.512381));
                points2.Add(new PointLatLng(45.836133, 28.776053));
                points2.Add(new PointLatLng(45.969922, 28.765067));
                points2.Add(new PointLatLng(46.004273, 28.984793));
                points2.Add(new PointLatLng(46.118622, 28.973807));
                points2.Add(new PointLatLng(46.198525, 29.067191));
                points2.Add(new PointLatLng(46.259326, 28.962821));
                points2.Add(new PointLatLng(46.342817, 28.990286));
                points2.Add(new PointLatLng(46.452679, 28.935355));
                points2.Add(new PointLatLng(46.558543, 29.215506));
                points2.Add(new PointLatLng(46.384515, 29.215506));
                points2.Add(new PointLatLng(46.482947, 29.352835));
                points2.Add(new PointLatLng(46.441325, 29.391287));
                points2.Add(new PointLatLng(46.494293, 29.435233));
                points2.Add(new PointLatLng(46.361775, 29.583548));
                points2.Add(new PointLatLng(46.361775, 29.687918));
                points2.Add(new PointLatLng(46.433754, 29.665946));
                points2.Add(new PointLatLng(46.456464, 29.742850));
                points2.Add(new PointLatLng(46.392093, 29.797782));
                points2.Add(new PointLatLng(46.346609, 29.880179));
                points2.Add(new PointLatLng(46.418608, 30.160330));
                points2.Add(new PointLatLng(46.570050, 29.912612));
                points2.Add(new PointLatLng(46.681163, 29.987296));
                points2.Add(new PointLatLng(46.891794, 29.893912));
                points2.Add(new PointLatLng(46.966819, 29.608267));
                points2.Add(new PointLatLng(47.377582, 29.591788));
                points2.Add(new PointLatLng(47.314308, 29.443472));
                points2.Add(new PointLatLng(47.548414, 29.141348));
                points2.Add(new PointLatLng(47.895780, 29.267691));
                points2.Add(new PointLatLng(48.006154, 29.157828));
                points2.Add(new PointLatLng(47.980421, 28.916129));

                List<GMap.NET.PointLatLng> points3 = new List<GMap.NET.PointLatLng>();
                points3.Add(new PointLatLng(46.061655, 33.571060));
                points3.Add(new PointLatLng(46.221511, 33.658950));
                points3.Add(new PointLatLng(46.107376, 34.010513));
                points3.Add(new PointLatLng(46.031154, 34.329116));
                points3.Add(new PointLatLng(45.954853, 34.498803));
                points3.Add(new PointLatLng(45.993030, 34.553735));
                points3.Add(new PointLatLng(45.901361, 34.784448));
                points3.Add(new PointLatLng(45.798053, 34.784448));
                points3.Add(new PointLatLng(45.748244, 34.872338));
                points3.Add(new PointLatLng(45.644652, 35.020654));
                points3.Add(new PointLatLng(45.486978, 35.103051));
                points3.Add(new PointLatLng(45.386760, 34.965722));
                points3.Add(new PointLatLng(45.355888, 35.196435));
                points3.Add(new PointLatLng(45.274769, 35.394189));
                points3.Add(new PointLatLng(45.321137, 35.657861));
                points3.Add(new PointLatLng(45.463867, 35.839135));
                points3.Add(new PointLatLng(45.375185, 35.970971));
                points3.Add(new PointLatLng(45.402190, 36.075341));
                points3.Add(new PointLatLng(45.460014, 36.080835));
                points3.Add(new PointLatLng(45.494680, 36.284082));
                points3.Add(new PointLatLng(45.440746, 36.586206));
                points3.Add(new PointLatLng(45.390618, 36.641137));
                points3.Add(new PointLatLng(45.340446, 36.575219));
                points3.Add(new PointLatLng(45.352028, 36.465356));
                points3.Add(new PointLatLng(45.181919, 36.399438));
                points3.Add(new PointLatLng(45.100551, 36.454370));
                points3.Add(new PointLatLng(45.026833, 36.229150));
                points3.Add(new PointLatLng(45.057884, 36.031396));
                points3.Add(new PointLatLng(44.999649, 35.850122));
                points3.Add(new PointLatLng(45.077282, 35.767724));
                points3.Add(new PointLatLng(45.127687, 35.591943));
                points3.Add(new PointLatLng(45.108306, 35.465600));
                points3.Add(new PointLatLng(45.038479, 35.388696));
                points3.Add(new PointLatLng(44.964680, 35.361230));
                points3.Add(new PointLatLng(44.960793, 35.245874));
                points3.Add(new PointLatLng(44.805106, 35.092065));
                points3.Add(new PointLatLng(44.801209, 34.740502));
                points3.Add(new PointLatLng(44.711492, 34.443872));
                points3.Add(new PointLatLng(44.555131, 34.334008));
                points3.Add(new PointLatLng(44.504223, 34.240625));
                points3.Add(new PointLatLng(44.488550, 34.158227));
                points3.Add(new PointLatLng(44.425816, 34.086816));
                points3.Add(new PointLatLng(44.398348, 33.949487));
                points3.Add(new PointLatLng(44.394423, 33.746240));
                points3.Add(new PointLatLng(44.484631, 33.625390));
                points3.Add(new PointLatLng(44.566872, 33.405664));
                points3.Add(new PointLatLng(44.625545, 33.526513));
                points3.Add(new PointLatLng(44.820694, 33.537500));
                points3.Add(new PointLatLng(44.863538, 33.608911));
                points3.Add(new PointLatLng(45.050123, 33.586938));
                points3.Add(new PointLatLng(45.143187, 33.510034));
                points3.Add(new PointLatLng(45.193534, 33.400170));
                points3.Add(new PointLatLng(45.154810, 33.251855));
                points3.Add(new PointLatLng(45.212886, 33.131005));
                points3.Add(new PointLatLng(45.336585, 32.971704));
                points3.Add(new PointLatLng(45.375185, 32.856347));
                points3.Add(new PointLatLng(45.367467, 32.746484));
                points3.Add(new PointLatLng(45.317275, 32.647607));
                points3.Add(new PointLatLng(45.352028, 32.499292));
                points3.Add(new PointLatLng(45.406047, 32.477319));
                points3.Add(new PointLatLng(45.498530, 32.570703));
                points3.Add(new PointLatLng(45.706063, 33.043115));
                points3.Add(new PointLatLng(45.855470, 33.482568));
                points3.Add(new PointLatLng(45.939575, 33.746240));
                points3.Add(new PointLatLng(45.958672, 33.625390));
                points3.Add(new PointLatLng(46.084546, 33.630883));

                GMapPolygon polygon = new GMapPolygon(points, "mypolygon");
                GMapPolygon polygon2 = new GMapPolygon(points2, "mypolygon2");
                GMapPolygon polygon3 = new GMapPolygon(points3, "mypolygon3");
                polygon.Fill = new SolidBrush(Color.FromArgb(60, Color.Red));
                polygon.Stroke = new Pen(Color.Red, 1);
                polyOverlay.Polygons.Add(polygon);
                polygon2.Fill = new SolidBrush(Color.FromArgb(60, Color.Red));
                polygon2.Stroke = new Pen(Color.Red, 1);
                polyOverlay.Polygons.Add(polygon2);
                polygon3.Fill = new SolidBrush(Color.FromArgb(60, Color.Red));
                polygon3.Stroke = new Pen(Color.Red, 1);
                polyOverlay.Polygons.Add(polygon3);
                maps.map.Overlays.Add(polyOverlay);
            }
            else if (res_pos == 6)
            {
                maps.Text = "Водні ресурси";
                maps.center = new GMap.NET.PointLatLng(48.473526, 31.388053);
                maps.zoom = 6; maps.pos = 6;

                GMapOverlay polyOverlay = new GMapOverlay("polylines");
                List<GMap.NET.PointLatLng> points = new List<GMap.NET.PointLatLng>();
                points.Add(new PointLatLng(51.266480, 30.534247));
                points.Add(new PointLatLng(51.097755, 30.490302));
                points.Add(new PointLatLng(50.633186, 30.495795));
                points.Add(new PointLatLng(50.360630, 30.578192));
                points.Add(new PointLatLng(50.146382, 30.781440));
                points.Add(new PointLatLng(50.118212, 30.918769));
                points.Add(new PointLatLng(49.998306, 31.050605));
                points.Add(new PointLatLng(49.977115, 31.237372));
                points.Add(new PointLatLng(50.019487, 31.402167));
                points.Add(new PointLatLng(49.970049, 31.512030));
                points.Add(new PointLatLng(49.793068, 31.468085));
                points.Add(new PointLatLng(49.651015, 31.632880));
                points.Add(new PointLatLng(49.633230, 31.770209));
                points.Add(new PointLatLng(49.103826, 33.192939));
                points.Add(new PointLatLng(49.042651, 33.473090));
                points.Add(new PointLatLng(48.995819, 33.615912));
                points.Add(new PointLatLng(48.945335, 33.808173));
                points.Add(new PointLatLng(48.771861, 34.198188));
                points.Add(new PointLatLng(48.586885, 34.511298));
                points.Add(new PointLatLng(48.535988, 34.665107));
                points.Add(new PointLatLng(48.481399, 34.769477));
                points.Add(new PointLatLng(48.510521, 34.945258));
                points.Add(new PointLatLng(48.459547, 35.093573));
                points.Add(new PointLatLng(48.404876, 35.093573));
                points.Add(new PointLatLng(48.390287, 35.148505));
                points.Add(new PointLatLng(48.309974, 35.153998));
                points.Add(new PointLatLng(48.251484, 35.186957));
                points.Add(new PointLatLng(48.137970, 35.088080));
                points.Add(new PointLatLng(48.097630, 35.038642));
                points.Add(new PointLatLng(48.031522, 35.134743));
                points.Add(new PointLatLng(47.910157, 35.118263));
                points.Add(new PointLatLng(47.847525, 35.085304));
                points.Add(new PointLatLng(47.799578, 35.162209));
                points.Add(new PointLatLng(47.562888, 35.162209));
                points.Add(new PointLatLng(47.507258, 34.920509));
                points.Add(new PointLatLng(47.566594, 34.546974));
                points.Add(new PointLatLng(47.484990, 34.030617));
                points.Add(new PointLatLng(47.239414, 33.942726));
                points.Add(new PointLatLng(47.172240, 33.821877));
                points.Add(new PointLatLng(47.033893, 33.684547));
                points.Add(new PointLatLng(46.861394, 33.602150));
                points.Add(new PointLatLng(46.827579, 33.442848));
                points.Add(new PointLatLng(46.771173, 33.316505));
                points.Add(new PointLatLng(46.786220, 33.140724));
                points.Add(new PointLatLng(46.744830, 33.036354));
                points.Add(new PointLatLng(46.741066, 32.920998));
                points.Add(new PointLatLng(46.673263, 32.772682));
                points.Add(new PointLatLng(46.631787, 32.624367));
                points.Add(new PointLatLng(46.507165, 32.272804));
                points.Add(new PointLatLng(46.631787, 32.624367));
                points.Add(new PointLatLng(46.673263, 32.772682));
                points.Add(new PointLatLng(46.741066, 32.920998));
                points.Add(new PointLatLng(46.744830, 33.036354));
                points.Add(new PointLatLng(46.786220, 33.140724));
                points.Add(new PointLatLng(46.771173, 33.316505));
                points.Add(new PointLatLng(46.827579, 33.442848));
                points.Add(new PointLatLng(46.861394, 33.602150));
                points.Add(new PointLatLng(47.033893, 33.684547));
                points.Add(new PointLatLng(47.172240, 33.821877));
                points.Add(new PointLatLng(47.239414, 33.942726));
                points.Add(new PointLatLng(47.484990, 34.030617));
                points.Add(new PointLatLng(47.566594, 34.546974));
                points.Add(new PointLatLng(47.507258, 34.920509));
                points.Add(new PointLatLng(47.562888, 35.162209));
                points.Add(new PointLatLng(47.799578, 35.162209));
                points.Add(new PointLatLng(47.847525, 35.085304));
                points.Add(new PointLatLng(47.910157, 35.118263));
                points.Add(new PointLatLng(48.031522, 35.134743));
                points.Add(new PointLatLng(48.097630, 35.038642));
                points.Add(new PointLatLng(48.137970, 35.088080));
                points.Add(new PointLatLng(48.251484, 35.186957));
                points.Add(new PointLatLng(48.309974, 35.153998));
                points.Add(new PointLatLng(48.390287, 35.148505));
                points.Add(new PointLatLng(48.404876, 35.093573));
                points.Add(new PointLatLng(48.459547, 35.093573));
                points.Add(new PointLatLng(48.510521, 34.945258));
                points.Add(new PointLatLng(48.481399, 34.769477));
                points.Add(new PointLatLng(48.535988, 34.665107));
                points.Add(new PointLatLng(48.586885, 34.511298));
                points.Add(new PointLatLng(48.771861, 34.198188));
                points.Add(new PointLatLng(48.945335, 33.808173));
                points.Add(new PointLatLng(48.995819, 33.615912));
                points.Add(new PointLatLng(49.042651, 33.473090));
                points.Add(new PointLatLng(49.103826, 33.192939));
                points.Add(new PointLatLng(49.633230, 31.770209));
                points.Add(new PointLatLng(49.651015, 31.632880));
                points.Add(new PointLatLng(49.793068, 31.468085));
                points.Add(new PointLatLng(49.970049, 31.512030));
                points.Add(new PointLatLng(50.019487, 31.402167));
                points.Add(new PointLatLng(49.977115, 31.237372));
                points.Add(new PointLatLng(49.998306, 31.050605));
                points.Add(new PointLatLng(50.118212, 30.918769));
                points.Add(new PointLatLng(50.146382, 30.781440));
                points.Add(new PointLatLng(50.360630, 30.578192));
                points.Add(new PointLatLng(50.633186, 30.495795));
                points.Add(new PointLatLng(51.097755, 30.490302));
                points.Add(new PointLatLng(51.266480, 30.534247));

                List<GMap.NET.PointLatLng> points1 = new List<GMap.NET.PointLatLng>();
                points1.Add(new PointLatLng(49.212342, 22.895715));
                points1.Add(new PointLatLng(49.243731, 22.837351));
                points1.Add(new PointLatLng(49.283835, 22.840097));
                points1.Add(new PointLatLng(49.306001, 22.875803));
                points1.Add(new PointLatLng(49.324354, 22.965753));
                points1.Add(new PointLatLng(49.344712, 22.994249));
                points1.Add(new PointLatLng(49.357013, 23.015878));
                points1.Add(new PointLatLng(49.399483, 22.966783));
                points1.Add(new PointLatLng(49.448612, 23.017938));
                points1.Add(new PointLatLng(49.504159, 23.201273));
                points1.Add(new PointLatLng(49.527117, 23.291223));
                points1.Add(new PointLatLng(49.595706, 23.473871));
                points1.Add(new PointLatLng(49.526226, 23.736170));
                points1.Add(new PointLatLng(49.493679, 23.840196));
                points1.Add(new PointLatLng(49.506611, 23.869379));
                points1.Add(new PointLatLng(49.500591, 23.911264));
                points1.Add(new PointLatLng(49.512184, 23.920191));
                points1.Add(new PointLatLng(49.513745, 23.937357));
                points1.Add(new PointLatLng(49.489219, 23.975122));
                points1.Add(new PointLatLng(49.475613, 23.972376));
                points1.Add(new PointLatLng(49.458655, 24.031084));
                points1.Add(new PointLatLng(49.436781, 24.036234));
                points1.Add(new PointLatLng(49.439014, 24.067133));
                points1.Add(new PointLatLng(49.423607, 24.108331));
                points1.Add(new PointLatLng(49.435441, 24.134081));
                points1.Add(new PointLatLng(49.434995, 24.170129));
                points1.Add(new PointLatLng(49.413780, 24.209612));
                points1.Add(new PointLatLng(49.355895, 24.256303));
                points1.Add(new PointLatLng(49.337107, 24.253557));
                points1.Add(new PointLatLng(49.324354, 24.267633));
                points1.Add(new PointLatLng(49.297718, 24.253214));
                points1.Add(new PointLatLng(49.294416, 24.271751));
                points1.Add(new PointLatLng(49.273141, 24.291320));
                points1.Add(new PointLatLng(49.265076, 24.286857));
                points1.Add(new PointLatLng(49.257906, 24.295783));
                points1.Add(new PointLatLng(49.258802, 24.307800));
                points1.Add(new PointLatLng(49.241995, 24.331832));
                points1.Add(new PointLatLng(49.243564, 24.342819));
                points1.Add(new PointLatLng(49.233252, 24.358268));
                points1.Add(new PointLatLng(49.232355, 24.462295));
                points1.Add(new PointLatLng(49.223387, 24.474311));
                points1.Add(new PointLatLng(49.229889, 24.486328));
                points1.Add(new PointLatLng(49.218678, 24.508987));
                points1.Add(new PointLatLng(49.226302, 24.526840));
                points1.Add(new PointLatLng(49.211502, 24.549499));
                points1.Add(new PointLatLng(49.198492, 24.549499));
                points1.Add(new PointLatLng(49.199389, 24.562545));
                points1.Add(new PointLatLng(49.187273, 24.605117));
                points1.Add(new PointLatLng(49.167972, 24.616104));
                points1.Add(new PointLatLng(49.167074, 24.633956));
                points1.Add(new PointLatLng(49.154501, 24.638763));
                points1.Add(new PointLatLng(49.154501, 24.670349));
                points1.Add(new PointLatLng(49.127549, 24.727340));
                points1.Add(new PointLatLng(49.125752, 24.751373));
                points1.Add(new PointLatLng(49.084395, 24.752746));
                points1.Add(new PointLatLng(49.054705, 24.811111));
                points1.Add(new PointLatLng(49.044804, 24.806304));
                points1.Add(new PointLatLng(49.044354, 24.820724));
                points1.Add(new PointLatLng(49.033101, 24.827590));
                points1.Add(new PointLatLng(49.022295, 24.851623));
                points1.Add(new PointLatLng(49.025447, 24.866729));
                points1.Add(new PointLatLng(49.037602, 24.882522));
                points1.Add(new PointLatLng(49.037152, 24.892822));
                points1.Add(new PointLatLng(49.029049, 24.892822));
                points1.Add(new PointLatLng(49.030400, 24.905868));
                points1.Add(new PointLatLng(49.040753, 24.917541));
                points1.Add(new PointLatLng(49.010587, 24.970413));
                points1.Add(new PointLatLng(48.979050, 24.973159));
                points1.Add(new PointLatLng(48.968683, 24.995132));
                points1.Add(new PointLatLng(48.983556, 25.052810));
                points1.Add(new PointLatLng(48.980402, 25.071350));
                points1.Add(new PointLatLng(48.961922, 25.083022));
                points1.Add(new PointLatLng(48.945238, 25.146881));
                points1.Add(new PointLatLng(48.927196, 25.118728));
                points1.Add(new PointLatLng(48.871675, 25.137267));
                points1.Add(new PointLatLng(48.865803, 25.163360));
                points1.Add(new PointLatLng(48.898315, 25.173660));
                points1.Add(new PointLatLng(48.915014, 25.198379));
                points1.Add(new PointLatLng(48.927196, 25.190139));
                points1.Add(new PointLatLng(48.923136, 25.209365));
                points1.Add(new PointLatLng(48.932158, 25.223785));
                points1.Add(new PointLatLng(48.931707, 25.234084));
                points1.Add(new PointLatLng(48.886577, 25.229965));
                points1.Add(new PointLatLng(48.848636, 25.207992));
                points1.Add(new PointLatLng(48.845925, 25.216232));
                points1.Add(new PointLatLng(48.848636, 25.227905));
                points1.Add(new PointLatLng(48.866707, 25.236144));
                points1.Add(new PointLatLng(48.862642, 25.283523));
                points1.Add(new PointLatLng(48.848636, 25.289016));
                points1.Add(new PointLatLng(48.844118, 25.300002));
                points1.Add(new PointLatLng(48.847733, 25.317855));
                points1.Add(new PointLatLng(48.839147, 25.326782));
                points1.Add(new PointLatLng(48.828751, 25.317169));
                points1.Add(new PointLatLng(48.826943, 25.259490));
                points1.Add(new PointLatLng(48.813832, 25.239578));
                points1.Add(new PointLatLng(48.786695, 25.248504));
                points1.Add(new PointLatLng(48.782623, 25.280090));
                points1.Add(new PointLatLng(48.806145, 25.292449));
                points1.Add(new PointLatLng(48.820614, 25.321975));
                points1.Add(new PointLatLng(48.807501, 25.339828));
                points1.Add(new PointLatLng(48.807049, 25.356307));
                points1.Add(new PointLatLng(48.816997, 25.358367));
                points1.Add(new PointLatLng(48.851799, 25.339141));
                points1.Add(new PointLatLng(48.861286, 25.350814));
                points1.Add(new PointLatLng(48.843214, 25.427719));
                points1.Add(new PointLatLng(48.849088, 25.439391));
                points1.Add(new PointLatLng(48.867159, 25.425659));
                points1.Add(new PointLatLng(48.873482, 25.436645));
                points1.Add(new PointLatLng(48.856317, 25.470291));
                points1.Add(new PointLatLng(48.840954, 25.449691));
                points1.Add(new PointLatLng(48.833271, 25.453124));
                points1.Add(new PointLatLng(48.811571, 25.499816));
                points1.Add(new PointLatLng(48.802979, 25.542388));
                points1.Add(new PointLatLng(48.750037, 25.639205));
                points1.Add(new PointLatLng(48.738264, 25.635085));
                points1.Add(new PointLatLng(48.729659, 25.610366));
                points1.Add(new PointLatLng(48.717882, 25.613113));
                points1.Add(new PointLatLng(48.711539, 25.648132));
                points1.Add(new PointLatLng(48.697945, 25.650192));
                points1.Add(new PointLatLng(48.685254, 25.624786));
                points1.Add(new PointLatLng(48.675280, 25.623412));
                points1.Add(new PointLatLng(48.663489, 25.710616));
                points1.Add(new PointLatLng(48.639901, 25.724349));
                points1.Add(new PointLatLng(48.634002, 25.739456));
                points1.Add(new PointLatLng(48.641262, 25.748382));
                points1.Add(new PointLatLng(48.663489, 25.755935));
                points1.Add(new PointLatLng(48.675280, 25.780654));
                points1.Add(new PointLatLng(48.618118, 25.856185));
                points1.Add(new PointLatLng(48.604952, 25.845886));
                points1.Add(new PointLatLng(48.596779, 25.850692));
                points1.Add(new PointLatLng(48.600865, 25.875411));
                points1.Add(new PointLatLng(48.587695, 25.905624));
                points1.Add(new PointLatLng(48.591783, 25.935150));
                points1.Add(new PointLatLng(48.621749, 25.977035));
                points1.Add(new PointLatLng(48.610854, 26.009994));
                points1.Add(new PointLatLng(48.610400, 26.022354));
                points1.Add(new PointLatLng(48.621749, 26.030593));
                points1.Add(new PointLatLng(48.642170, 26.029220));
                points1.Add(new PointLatLng(48.649429, 26.043640));
                points1.Add(new PointLatLng(48.639447, 26.060806));
                points1.Add(new PointLatLng(48.622203, 26.058059));
                points1.Add(new PointLatLng(48.607676, 26.064926));
                points1.Add(new PointLatLng(48.589058, 26.042953));
                points1.Add(new PointLatLng(48.570887, 26.056686));
                points1.Add(new PointLatLng(48.558618, 26.078658));
                points1.Add(new PointLatLng(48.540891, 26.089645));
                points1.Add(new PointLatLng(48.539073, 26.104064));
                points1.Add(new PointLatLng(48.550438, 26.121917));
                points1.Add(new PointLatLng(48.569070, 26.125350));
                points1.Add(new PointLatLng(48.581336, 26.105438));
                points1.Add(new PointLatLng(48.592237, 26.104064));
                points1.Add(new PointLatLng(48.599503, 26.094451));
                points1.Add(new PointLatLng(48.615848, 26.102004));
                points1.Add(new PointLatLng(48.619933, 26.119857));
                points1.Add(new PointLatLng(48.599049, 26.143890));
                points1.Add(new PointLatLng(48.548619, 26.145263));
                points1.Add(new PointLatLng(48.530889, 26.163803));
                points1.Add(new PointLatLng(48.526342, 26.198821));
                points1.Add(new PointLatLng(48.540437, 26.268173));
                points1.Add(new PointLatLng(48.537254, 26.278472));
                points1.Add(new PointLatLng(48.514062, 26.294265));
                points1.Add(new PointLatLng(48.509058, 26.305938));
                points1.Add(new PointLatLng(48.517701, 26.323104));
                points1.Add(new PointLatLng(48.509968, 26.345077));
                points1.Add(new PointLatLng(48.512697, 26.364990));
                points1.Add(new PointLatLng(48.533162, 26.384902));
                points1.Add(new PointLatLng(48.535890, 26.432281));
                points1.Add(new PointLatLng(48.547710, 26.472793));
                points1.Add(new PointLatLng(48.542710, 26.489959));
                points1.Add(new PointLatLng(48.460360, 26.555877));
                points1.Add(new PointLatLng(48.452619, 26.606689));
                points1.Add(new PointLatLng(48.466279, 26.624542));
                points1.Add(new PointLatLng(48.503599, 26.606002));
                points1.Add(new PointLatLng(48.509968, 26.620422));
                points1.Add(new PointLatLng(48.505874, 26.636215));
                points1.Add(new PointLatLng(48.488583, 26.664367));
                points1.Add(new PointLatLng(48.488128, 26.702133));
                points1.Add(new PointLatLng(48.498139, 26.713119));
                points1.Add(new PointLatLng(48.512242, 26.702819));
                points1.Add(new PointLatLng(48.524068, 26.679473));
                points1.Add(new PointLatLng(48.538618, 26.676040));
                points1.Add(new PointLatLng(48.541801, 26.647888));
                points1.Add(new PointLatLng(48.539073, 26.623168));
                points1.Add(new PointLatLng(48.554074, 26.618362));
                points1.Add(new PointLatLng(48.562254, 26.661620));
                points1.Add(new PointLatLng(48.532253, 26.714492));
                points1.Add(new PointLatLng(48.541346, 26.739211));
                points1.Add(new PointLatLng(48.567252, 26.739211));
                points1.Add(new PointLatLng(48.579519, 26.719985));
                points1.Add(new PointLatLng(48.588604, 26.727538));
                points1.Add(new PointLatLng(48.581791, 26.747451));
                points1.Add(new PointLatLng(48.551347, 26.759124));
                points1.Add(new PointLatLng(48.549528, 26.782470));
                points1.Add(new PointLatLng(48.570887, 26.793456));
                points1.Add(new PointLatLng(48.594962, 26.777664));
                points1.Add(new PointLatLng(48.610854, 26.802383));
                points1.Add(new PointLatLng(48.607676, 26.822982));
                points1.Add(new PointLatLng(48.585879, 26.817489));
                points1.Add(new PointLatLng(48.559527, 26.840148));
                points1.Add(new PointLatLng(48.543164, 26.875854));
                points1.Add(new PointLatLng(48.550892, 26.903320));
                points1.Add(new PointLatLng(48.574976, 26.922546));
                points1.Add(new PointLatLng(48.576794, 26.963058));
                points1.Add(new PointLatLng(48.587241, 26.980911));
                points1.Add(new PointLatLng(48.574068, 26.995330));
                points1.Add(new PointLatLng(48.558164, 27.102447));
                points1.Add(new PointLatLng(48.585424, 27.169738));
                points1.Add(new PointLatLng(48.566798, 27.238403));
                points1.Add(new PointLatLng(48.582699, 27.263122));
                points1.Add(new PointLatLng(48.611762, 27.243896));
                points1.Add(new PointLatLng(48.624926, 27.277542));
                points1.Add(new PointLatLng(48.604044, 27.307754));
                points1.Add(new PointLatLng(48.607676, 27.340026));
                points1.Add(new PointLatLng(48.627649, 27.347579));
                points1.Add(new PointLatLng(48.629918, 27.372299));
                points1.Add(new PointLatLng(48.598595, 27.449890));
                points1.Add(new PointLatLng(48.538164, 27.480789));
                points1.Add(new PointLatLng(48.509513, 27.478729));
                points1.Add(new PointLatLng(48.484943, 27.496581));
                points1.Add(new PointLatLng(48.471742, 27.519241));
                points1.Add(new PointLatLng(48.470832, 27.534347));
                points1.Add(new PointLatLng(48.491314, 27.580352));
                points1.Add(new PointLatLng(48.491769, 27.598892));
                points1.Add(new PointLatLng(48.485853, 27.603698));
                points1.Add(new PointLatLng(48.464003, 27.589279));
                points1.Add(new PointLatLng(48.441232, 27.645584));
                points1.Add(new PointLatLng(48.440321, 27.678542));
                points1.Add(new PointLatLng(48.458539, 27.743087));
                points1.Add(new PointLatLng(48.456717, 27.762313));
                points1.Add(new PointLatLng(48.427565, 27.815185));
                points1.Add(new PointLatLng(48.420274, 27.819305));
                points1.Add(new PointLatLng(48.407513, 27.870117));
                points1.Add(new PointLatLng(48.393837, 27.879730));
                points1.Add(new PointLatLng(48.377420, 27.879043));
                points1.Add(new PointLatLng(48.327226, 27.958007));
                points1.Add(new PointLatLng(48.324487, 28.061004));
                points1.Add(new PointLatLng(48.295716, 28.091903));
                points1.Add(new PointLatLng(48.244984, 28.078857));
                points1.Add(new PointLatLng(48.236295, 28.085723));
                points1.Add(new PointLatLng(48.231722, 28.104263));
                points1.Add(new PointLatLng(48.245899, 28.135162));
                points1.Add(new PointLatLng(48.263729, 28.142028));
                points1.Add(new PointLatLng(48.253672, 28.185974));
                points1.Add(new PointLatLng(48.237210, 28.181167));
                points1.Add(new PointLatLng(48.215253, 28.185974));
                points1.Add(new PointLatLng(48.205644, 28.204513));
                points1.Add(new PointLatLng(48.232636, 28.286910));
                points1.Add(new PointLatLng(48.242240, 28.303390));
                points1.Add(new PointLatLng(48.234009, 28.318496));
                points1.Add(new PointLatLng(48.248185, 28.346649));
                points1.Add(new PointLatLng(48.242240, 28.359008));
                points1.Add(new PointLatLng(48.214338, 28.369995));
                points1.Add(new PointLatLng(48.180469, 28.350768));
                points1.Add(new PointLatLng(48.164441, 28.310943));
                points1.Add(new PointLatLng(48.141537, 28.305450));
                points1.Add(new PointLatLng(48.132830, 28.322616));
                points1.Add(new PointLatLng(48.136955, 28.352142));
                points1.Add(new PointLatLng(48.175432, 28.381667));
                points1.Add(new PointLatLng(48.178637, 28.410507));
                points1.Add(new PointLatLng(48.169021, 28.433166));
                points1.Add(new PointLatLng(48.143370, 28.438659));
                points1.Add(new PointLatLng(48.133289, 28.423553));
                points1.Add(new PointLatLng(48.122289, 28.420806));
                points1.Add(new PointLatLng(48.082396, 28.448959));
                points1.Add(new PointLatLng(48.072303, 28.477111));
                points1.Add(new PointLatLng(48.065420, 28.488784));
                points1.Add(new PointLatLng(48.067256, 28.500457));
                points1.Add(new PointLatLng(48.076891, 28.490844));
                points1.Add(new PointLatLng(48.089735, 28.497024));
                points1.Add(new PointLatLng(48.114955, 28.496337));
                points1.Add(new PointLatLng(48.119539, 28.502517));
                points1.Add(new PointLatLng(48.114955, 28.496337));
                points1.Add(new PointLatLng(48.089735, 28.497024));
                points1.Add(new PointLatLng(48.076891, 28.490844));
                points1.Add(new PointLatLng(48.067256, 28.500457));
                points1.Add(new PointLatLng(48.065420, 28.488784));
                points1.Add(new PointLatLng(48.072303, 28.477111));
                points1.Add(new PointLatLng(48.082396, 28.448959));
                points1.Add(new PointLatLng(48.122289, 28.420806));
                points1.Add(new PointLatLng(48.133289, 28.423553));
                points1.Add(new PointLatLng(48.143370, 28.438659));
                points1.Add(new PointLatLng(48.169021, 28.433166));
                points1.Add(new PointLatLng(48.178637, 28.410507));
                points1.Add(new PointLatLng(48.175432, 28.381667));
                points1.Add(new PointLatLng(48.136955, 28.352142));
                points1.Add(new PointLatLng(48.132830, 28.322616));
                points1.Add(new PointLatLng(48.141537, 28.305450));
                points1.Add(new PointLatLng(48.164441, 28.310943));
                points1.Add(new PointLatLng(48.180469, 28.350768));
                points1.Add(new PointLatLng(48.214338, 28.369995));
                points1.Add(new PointLatLng(48.242240, 28.359008));
                points1.Add(new PointLatLng(48.248185, 28.346649));
                points1.Add(new PointLatLng(48.234009, 28.318496));
                points1.Add(new PointLatLng(48.242240, 28.303390));
                points1.Add(new PointLatLng(48.232636, 28.286910));
                points1.Add(new PointLatLng(48.205644, 28.204513));
                points1.Add(new PointLatLng(48.215253, 28.185974));
                points1.Add(new PointLatLng(48.237210, 28.181167));
                points1.Add(new PointLatLng(48.253672, 28.185974));
                points1.Add(new PointLatLng(48.263729, 28.142028));
                points1.Add(new PointLatLng(48.245899, 28.135162));
                points1.Add(new PointLatLng(48.231722, 28.104263));
                points1.Add(new PointLatLng(48.236295, 28.085723));
                points1.Add(new PointLatLng(48.244984, 28.078857));
                points1.Add(new PointLatLng(48.295716, 28.091903));
                points1.Add(new PointLatLng(48.324487, 28.061004));
                points1.Add(new PointLatLng(48.327226, 27.958007));
                points1.Add(new PointLatLng(48.377420, 27.879043));
                points1.Add(new PointLatLng(48.393837, 27.879730));
                points1.Add(new PointLatLng(48.407513, 27.870117));
                points1.Add(new PointLatLng(48.420274, 27.819305));
                points1.Add(new PointLatLng(48.427565, 27.815185));
                points1.Add(new PointLatLng(48.456717, 27.762313));
                points1.Add(new PointLatLng(48.458539, 27.743087));
                points1.Add(new PointLatLng(48.440321, 27.678542));
                points1.Add(new PointLatLng(48.441232, 27.645584));
                points1.Add(new PointLatLng(48.464003, 27.589279));
                points1.Add(new PointLatLng(48.485853, 27.603698));
                points1.Add(new PointLatLng(48.491769, 27.598892));
                points1.Add(new PointLatLng(48.491314, 27.580352));
                points1.Add(new PointLatLng(48.470832, 27.534347));
                points1.Add(new PointLatLng(48.471742, 27.519241));
                points1.Add(new PointLatLng(48.484943, 27.496581));
                points1.Add(new PointLatLng(48.509513, 27.478729));
                points1.Add(new PointLatLng(48.538164, 27.480789));
                points1.Add(new PointLatLng(48.598595, 27.449890));
                points1.Add(new PointLatLng(48.629918, 27.372299));
                points1.Add(new PointLatLng(48.627649, 27.347579));
                points1.Add(new PointLatLng(48.607676, 27.340026));
                points1.Add(new PointLatLng(48.604044, 27.307754));
                points1.Add(new PointLatLng(48.624926, 27.277542));
                points1.Add(new PointLatLng(48.611762, 27.243896));
                points1.Add(new PointLatLng(48.582699, 27.263122));
                points1.Add(new PointLatLng(48.566798, 27.238403));
                points1.Add(new PointLatLng(48.585424, 27.169738));
                points1.Add(new PointLatLng(48.558164, 27.102447));
                points1.Add(new PointLatLng(48.574068, 26.995330));
                points1.Add(new PointLatLng(48.587241, 26.980911));
                points1.Add(new PointLatLng(48.576794, 26.963058));
                points1.Add(new PointLatLng(48.574976, 26.922546));
                points1.Add(new PointLatLng(48.550892, 26.903320));
                points1.Add(new PointLatLng(48.543164, 26.875854));
                points1.Add(new PointLatLng(48.559527, 26.840148));
                points1.Add(new PointLatLng(48.585879, 26.817489));
                points1.Add(new PointLatLng(48.607676, 26.822982));
                points1.Add(new PointLatLng(48.610854, 26.802383));
                points1.Add(new PointLatLng(48.594962, 26.777664));
                points1.Add(new PointLatLng(48.570887, 26.793456));
                points1.Add(new PointLatLng(48.549528, 26.782470));
                points1.Add(new PointLatLng(48.551347, 26.759124));
                points1.Add(new PointLatLng(48.581791, 26.747451));
                points1.Add(new PointLatLng(48.588604, 26.727538));
                points1.Add(new PointLatLng(48.579519, 26.719985));
                points1.Add(new PointLatLng(48.567252, 26.739211));
                points1.Add(new PointLatLng(48.541346, 26.739211));
                points1.Add(new PointLatLng(48.532253, 26.714492));
                points1.Add(new PointLatLng(48.562254, 26.661620));
                points1.Add(new PointLatLng(48.554074, 26.618362));
                points1.Add(new PointLatLng(48.539073, 26.623168));
                points1.Add(new PointLatLng(48.541801, 26.647888));
                points1.Add(new PointLatLng(48.538618, 26.676040));
                points1.Add(new PointLatLng(48.524068, 26.679473));
                points1.Add(new PointLatLng(48.512242, 26.702819));
                points1.Add(new PointLatLng(48.498139, 26.713119));
                points1.Add(new PointLatLng(48.488128, 26.702133));
                points1.Add(new PointLatLng(48.488583, 26.664367));
                points1.Add(new PointLatLng(48.505874, 26.636215));
                points1.Add(new PointLatLng(48.509968, 26.620422));
                points1.Add(new PointLatLng(48.503599, 26.606002));
                points1.Add(new PointLatLng(48.466279, 26.624542));
                points1.Add(new PointLatLng(48.452619, 26.606689));
                points1.Add(new PointLatLng(48.460360, 26.555877));
                points1.Add(new PointLatLng(48.542710, 26.489959));
                points1.Add(new PointLatLng(48.547710, 26.472793));
                points1.Add(new PointLatLng(48.535890, 26.432281));
                points1.Add(new PointLatLng(48.533162, 26.384902));
                points1.Add(new PointLatLng(48.512697, 26.364990));
                points1.Add(new PointLatLng(48.509968, 26.345077));
                points1.Add(new PointLatLng(48.517701, 26.323104));
                points1.Add(new PointLatLng(48.509058, 26.305938));
                points1.Add(new PointLatLng(48.514062, 26.294265));
                points1.Add(new PointLatLng(48.537254, 26.278472));
                points1.Add(new PointLatLng(48.540437, 26.268173));
                points1.Add(new PointLatLng(48.526342, 26.198821));
                points1.Add(new PointLatLng(48.530889, 26.163803));
                points1.Add(new PointLatLng(48.548619, 26.145263));
                points1.Add(new PointLatLng(48.599049, 26.143890));
                points1.Add(new PointLatLng(48.619933, 26.119857));
                points1.Add(new PointLatLng(48.615848, 26.102004));
                points1.Add(new PointLatLng(48.599503, 26.094451));
                points1.Add(new PointLatLng(48.592237, 26.104064));
                points1.Add(new PointLatLng(48.581336, 26.105438));
                points1.Add(new PointLatLng(48.569070, 26.125350));
                points1.Add(new PointLatLng(48.550438, 26.121917));
                points1.Add(new PointLatLng(48.539073, 26.104064));
                points1.Add(new PointLatLng(48.540891, 26.089645));
                points1.Add(new PointLatLng(48.558618, 26.078658));
                points1.Add(new PointLatLng(48.570887, 26.056686));
                points1.Add(new PointLatLng(48.589058, 26.042953));
                points1.Add(new PointLatLng(48.607676, 26.064926));
                points1.Add(new PointLatLng(48.622203, 26.058059));
                points1.Add(new PointLatLng(48.639447, 26.060806));
                points1.Add(new PointLatLng(48.649429, 26.043640));
                points1.Add(new PointLatLng(48.642170, 26.029220));
                points1.Add(new PointLatLng(48.621749, 26.030593));
                points1.Add(new PointLatLng(48.610400, 26.022354));
                points1.Add(new PointLatLng(48.610854, 26.009994));
                points1.Add(new PointLatLng(48.621749, 25.977035));
                points1.Add(new PointLatLng(48.591783, 25.935150));
                points1.Add(new PointLatLng(48.587695, 25.905624));
                points1.Add(new PointLatLng(48.600865, 25.875411));
                points1.Add(new PointLatLng(48.596779, 25.850692));
                points1.Add(new PointLatLng(48.604952, 25.845886));
                points1.Add(new PointLatLng(48.618118, 25.856185));
                points1.Add(new PointLatLng(48.675280, 25.780654));
                points1.Add(new PointLatLng(48.663489, 25.755935));
                points1.Add(new PointLatLng(48.641262, 25.748382));
                points1.Add(new PointLatLng(48.634002, 25.739456));
                points1.Add(new PointLatLng(48.639901, 25.724349));
                points1.Add(new PointLatLng(48.663489, 25.710616));
                points1.Add(new PointLatLng(48.675280, 25.623412));
                points1.Add(new PointLatLng(48.685254, 25.624786));
                points1.Add(new PointLatLng(48.697945, 25.650192));
                points1.Add(new PointLatLng(48.711539, 25.648132));
                points1.Add(new PointLatLng(48.717882, 25.613113));
                points1.Add(new PointLatLng(48.729659, 25.610366));
                points1.Add(new PointLatLng(48.738264, 25.635085));
                points1.Add(new PointLatLng(48.750037, 25.639205));
                points1.Add(new PointLatLng(48.802979, 25.542388));
                points1.Add(new PointLatLng(48.811571, 25.499816));
                points1.Add(new PointLatLng(48.833271, 25.453124));
                points1.Add(new PointLatLng(48.840954, 25.449691));
                points1.Add(new PointLatLng(48.856317, 25.470291));
                points1.Add(new PointLatLng(48.873482, 25.436645));
                points1.Add(new PointLatLng(48.867159, 25.425659));
                points1.Add(new PointLatLng(48.849088, 25.439391));
                points1.Add(new PointLatLng(48.843214, 25.427719));
                points1.Add(new PointLatLng(48.861286, 25.350814));
                points1.Add(new PointLatLng(48.851799, 25.339141));
                points1.Add(new PointLatLng(48.816997, 25.358367));
                points1.Add(new PointLatLng(48.807049, 25.356307));
                points1.Add(new PointLatLng(48.807501, 25.339828));
                points1.Add(new PointLatLng(48.820614, 25.321975));
                points1.Add(new PointLatLng(48.806145, 25.292449));
                points1.Add(new PointLatLng(48.782623, 25.280090));
                points1.Add(new PointLatLng(48.786695, 25.248504));
                points1.Add(new PointLatLng(48.813832, 25.239578));
                points1.Add(new PointLatLng(48.826943, 25.259490));
                points1.Add(new PointLatLng(48.828751, 25.317169));
                points1.Add(new PointLatLng(48.839147, 25.326782));
                points1.Add(new PointLatLng(48.847733, 25.317855));
                points1.Add(new PointLatLng(48.844118, 25.300002));
                points1.Add(new PointLatLng(48.848636, 25.289016));
                points1.Add(new PointLatLng(48.862642, 25.283523));
                points1.Add(new PointLatLng(48.866707, 25.236144));
                points1.Add(new PointLatLng(48.848636, 25.227905));
                points1.Add(new PointLatLng(48.845925, 25.216232));
                points1.Add(new PointLatLng(48.848636, 25.207992));
                points1.Add(new PointLatLng(48.886577, 25.229965));
                points1.Add(new PointLatLng(48.931707, 25.234084));
                points1.Add(new PointLatLng(48.932158, 25.223785));
                points1.Add(new PointLatLng(48.923136, 25.209365));
                points1.Add(new PointLatLng(48.927196, 25.190139));
                points1.Add(new PointLatLng(48.915014, 25.198379));
                points1.Add(new PointLatLng(48.898315, 25.173660));
                points1.Add(new PointLatLng(48.865803, 25.163360));
                points1.Add(new PointLatLng(48.871675, 25.137267));
                points1.Add(new PointLatLng(48.927196, 25.118728));
                points1.Add(new PointLatLng(48.945238, 25.146881));
                points1.Add(new PointLatLng(48.961922, 25.083022));
                points1.Add(new PointLatLng(48.980402, 25.071350));
                points1.Add(new PointLatLng(48.983556, 25.052810));
                points1.Add(new PointLatLng(48.968683, 24.995132));
                points1.Add(new PointLatLng(48.979050, 24.973159));
                points1.Add(new PointLatLng(49.010587, 24.970413));
                points1.Add(new PointLatLng(49.040753, 24.917541));
                points1.Add(new PointLatLng(49.030400, 24.905868));
                points1.Add(new PointLatLng(49.029049, 24.892822));
                points1.Add(new PointLatLng(49.037152, 24.892822));
                points1.Add(new PointLatLng(49.037602, 24.882522));
                points1.Add(new PointLatLng(49.025447, 24.866729));
                points1.Add(new PointLatLng(49.022295, 24.851623));
                points1.Add(new PointLatLng(49.033101, 24.827590));
                points1.Add(new PointLatLng(49.044354, 24.820724));
                points1.Add(new PointLatLng(49.044804, 24.806304));
                points1.Add(new PointLatLng(49.054705, 24.811111));
                points1.Add(new PointLatLng(49.084395, 24.752746));
                points1.Add(new PointLatLng(49.125752, 24.751373));
                points1.Add(new PointLatLng(49.127549, 24.727340));
                points1.Add(new PointLatLng(49.154501, 24.670349));
                points1.Add(new PointLatLng(49.154501, 24.638763));
                points1.Add(new PointLatLng(49.167074, 24.633956));
                points1.Add(new PointLatLng(49.167972, 24.616104));
                points1.Add(new PointLatLng(49.187273, 24.605117));
                points1.Add(new PointLatLng(49.199389, 24.562545));
                points1.Add(new PointLatLng(49.198492, 24.549499));
                points1.Add(new PointLatLng(49.211502, 24.549499));
                points1.Add(new PointLatLng(49.226302, 24.526840));
                points1.Add(new PointLatLng(49.218678, 24.508987));
                points1.Add(new PointLatLng(49.229889, 24.486328));
                points1.Add(new PointLatLng(49.223387, 24.474311));
                points1.Add(new PointLatLng(49.232355, 24.462295));
                points1.Add(new PointLatLng(49.233252, 24.358268));
                points1.Add(new PointLatLng(49.243564, 24.342819));
                points1.Add(new PointLatLng(49.241995, 24.331832));
                points1.Add(new PointLatLng(49.258802, 24.307800));
                points1.Add(new PointLatLng(49.257906, 24.295783));
                points1.Add(new PointLatLng(49.265076, 24.286857));
                points1.Add(new PointLatLng(49.273141, 24.291320));
                points1.Add(new PointLatLng(49.294416, 24.271751));
                points1.Add(new PointLatLng(49.297718, 24.253214));
                points1.Add(new PointLatLng(49.324354, 24.267633));
                points1.Add(new PointLatLng(49.337107, 24.253557));
                points1.Add(new PointLatLng(49.355895, 24.256303));
                points1.Add(new PointLatLng(49.413780, 24.209612));
                points1.Add(new PointLatLng(49.434995, 24.170129));
                points1.Add(new PointLatLng(49.435441, 24.134081));
                points1.Add(new PointLatLng(49.423607, 24.108331));
                points1.Add(new PointLatLng(49.439014, 24.067133));
                points1.Add(new PointLatLng(49.436781, 24.036234));
                points1.Add(new PointLatLng(49.458655, 24.031084));
                points1.Add(new PointLatLng(49.475613, 23.972376));
                points1.Add(new PointLatLng(49.489219, 23.975122));
                points1.Add(new PointLatLng(49.513745, 23.937357));
                points1.Add(new PointLatLng(49.512184, 23.920191));
                points1.Add(new PointLatLng(49.500591, 23.911264));
                points1.Add(new PointLatLng(49.506611, 23.869379));
                points1.Add(new PointLatLng(49.493679, 23.840196));
                points1.Add(new PointLatLng(49.526226, 23.736170));
                points1.Add(new PointLatLng(49.595706, 23.473871));
                points1.Add(new PointLatLng(49.527117, 23.291223));
                points1.Add(new PointLatLng(49.504159, 23.201273));
                points1.Add(new PointLatLng(49.448612, 23.017938));
                points1.Add(new PointLatLng(49.399483, 22.966783));
                points1.Add(new PointLatLng(49.357013, 23.015878));
                points1.Add(new PointLatLng(49.344712, 22.994249));
                points1.Add(new PointLatLng(49.324354, 22.965753));
                points1.Add(new PointLatLng(49.306001, 22.875803));
                points1.Add(new PointLatLng(49.283835, 22.840097));
                points1.Add(new PointLatLng(49.243731, 22.837351));
                points1.Add(new PointLatLng(49.212342, 22.895715));

                List<GMap.NET.PointLatLng> points2 = new List<GMap.NET.PointLatLng>();
                points2.Add(new PointLatLng(49.620130, 26.469055));
                points2.Add(new PointLatLng(49.610454, 26.507078));
                points2.Add(new PointLatLng(49.597883, 26.544758));
                points2.Add(new PointLatLng(49.573510, 26.602007));
                points2.Add(new PointLatLng(49.567944, 26.618486));
                points2.Add(new PointLatLng(49.565105, 26.618572));
                points2.Add(new PointLatLng(49.562656, 26.621061));
                points2.Add(new PointLatLng(49.554248, 26.660372));
                points2.Add(new PointLatLng(49.545233, 26.671132));
                points2.Add(new PointLatLng(49.543618, 26.678857));
                points2.Add(new PointLatLng(49.539497, 26.680917));
                points2.Add(new PointLatLng(49.535876, 26.701087));
                points2.Add(new PointLatLng(49.521225, 26.750182));
                points2.Add(new PointLatLng(49.512365, 26.748037));
                points2.Add(new PointLatLng(49.509021, 26.751470));
                points2.Add(new PointLatLng(49.507070, 26.761598));
                points2.Add(new PointLatLng(49.494138, 26.792325));
                points2.Add(new PointLatLng(49.487002, 26.829490));
                points2.Add(new PointLatLng(49.452699, 26.937637));
                points2.Add(new PointLatLng(49.433501, 26.973857));
                points2.Add(new PointLatLng(49.415636, 27.048530));
                points2.Add(new PointLatLng(49.387150, 27.138137));
                points2.Add(new PointLatLng(49.376757, 27.238731));
                points2.Add(new PointLatLng(49.380222, 27.282161));
                points2.Add(new PointLatLng(49.381339, 27.335548));
                points2.Add(new PointLatLng(49.388491, 27.343444));
                points2.Add(new PointLatLng(49.393184, 27.333831));
                points2.Add(new PointLatLng(49.396200, 27.332286));
                points2.Add(new PointLatLng(49.398882, 27.337264));
                points2.Add(new PointLatLng(49.406142, 27.342071));
                points2.Add(new PointLatLng(49.407259, 27.349796));
                points2.Add(new PointLatLng(49.412732, 27.356147));
                points2.Add(new PointLatLng(49.415078, 27.369537));
                points2.Add(new PointLatLng(49.417981, 27.372112));
                points2.Add(new PointLatLng(49.417423, 27.381381));
                points2.Add(new PointLatLng(49.428031, 27.380351));
                points2.Add(new PointLatLng(49.436962, 27.423267));
                points2.Add(new PointLatLng(49.439529, 27.430133));
                points2.Add(new PointLatLng(49.439194, 27.436656));
                points2.Add(new PointLatLng(49.435176, 27.440089));
                points2.Add(new PointLatLng(49.429035, 27.440089));
                points2.Add(new PointLatLng(49.427361, 27.442836));
                points2.Add(new PointLatLng(49.429259, 27.448329));
                points2.Add(new PointLatLng(49.426133, 27.462234));
                points2.Add(new PointLatLng(49.426914, 27.467555));
                points2.Add(new PointLatLng(49.424458, 27.468585));
                points2.Add(new PointLatLng(49.398435, 27.596988));
                points2.Add(new PointLatLng(49.421778, 27.645225));
                points2.Add(new PointLatLng(49.454149, 27.636470));
                points2.Add(new PointLatLng(49.468654, 27.661704));
                points2.Add(new PointLatLng(49.466869, 27.701358));
                points2.Add(new PointLatLng(49.498208, 27.737235));
                points2.Add(new PointLatLng(49.530417, 27.741355));
                points2.Add(new PointLatLng(49.530752, 27.761955));
                points2.Add(new PointLatLng(49.523732, 27.768649));
                points2.Add(new PointLatLng(49.520835, 27.775859));
                points2.Add(new PointLatLng(49.529972, 27.789249));
                points2.Add(new PointLatLng(49.530863, 27.794055));
                points2.Add(new PointLatLng(49.539775, 27.791309));
                points2.Add(new PointLatLng(49.545122, 27.802638));
                points2.Add(new PointLatLng(49.550468, 27.805385));
                points2.Add(new PointLatLng(49.552250, 27.820491));
                points2.Add(new PointLatLng(49.556259, 27.829074));
                points2.Add(new PointLatLng(49.555814, 27.871989));
                points2.Add(new PointLatLng(49.559600, 27.882633));
                points2.Add(new PointLatLng(49.558931, 27.891216));
                points2.Add(new PointLatLng(49.555814, 27.894992));
                points2.Add(new PointLatLng(49.554255, 27.904605));
                points2.Add(new PointLatLng(49.560045, 27.932758));
                points2.Add(new PointLatLng(49.552695, 27.955417));
                points2.Add(new PointLatLng(49.548686, 27.957820));
                points2.Add(new PointLatLng(49.548909, 27.973956));
                points2.Add(new PointLatLng(49.545568, 27.978076));
                points2.Add(new PointLatLng(49.542003, 27.973956));
                points2.Add(new PointLatLng(49.528412, 27.972926));
                points2.Add(new PointLatLng(49.521726, 27.984256));
                points2.Add(new PointLatLng(49.523955, 28.001765));
                points2.Add(new PointLatLng(49.519274, 28.031635));
                points2.Add(new PointLatLng(49.510135, 28.055667));
                points2.Add(new PointLatLng(49.509913, 28.091373));
                points2.Add(new PointLatLng(49.504339, 28.119525));
                points2.Add(new PointLatLng(49.509913, 28.142871));
                points2.Add(new PointLatLng(49.487838, 28.201236));
                points2.Add(new PointLatLng(49.486277, 28.220119));
                points2.Add(new PointLatLng(49.493413, 28.232822));
                points2.Add(new PointLatLng(49.497204, 28.251018));
                points2.Add(new PointLatLng(49.483377, 28.276080));
                points2.Add(new PointLatLng(49.476685, 28.343715));
                points2.Add(new PointLatLng(49.466646, 28.354701));
                points2.Add(new PointLatLng(49.401227, 28.396930));
                points2.Add(new PointLatLng(49.392066, 28.398990));
                points2.Add(new PointLatLng(49.379104, 28.413753));
                points2.Add(new PointLatLng(49.374857, 28.433666));
                points2.Add(new PointLatLng(49.350261, 28.465251));
                points2.Add(new PointLatLng(49.342209, 28.490314));
                points2.Add(new PointLatLng(49.321178, 28.496150));
                points2.Add(new PointLatLng(49.320730, 28.483791));
                points2.Add(new PointLatLng(49.315807, 28.477268));
                points2.Add(new PointLatLng(49.242791, 28.467311));
                points2.Add(new PointLatLng(49.239353, 28.470058));
                points2.Add(new PointLatLng(49.238232, 28.476924));
                points2.Add(new PointLatLng(49.231955, 28.482761));
                points2.Add(new PointLatLng(49.227023, 28.465595));
                points2.Add(new PointLatLng(49.215811, 28.459758));
                points2.Add(new PointLatLng(49.209307, 28.446025));
                points2.Add(new PointLatLng(49.202354, 28.449802));
                points2.Add(new PointLatLng(49.193828, 28.462848));
                points2.Add(new PointLatLng(49.190687, 28.458041));
                points2.Add(new PointLatLng(49.192931, 28.452548));
                points2.Add(new PointLatLng(49.187322, 28.430232));
                points2.Add(new PointLatLng(49.148711, 28.382510));
                points2.Add(new PointLatLng(49.143097, 28.371181));
                points2.Add(new PointLatLng(49.138380, 28.370151));
                points2.Add(new PointLatLng(49.138829, 28.363284));
                points2.Add(new PointLatLng(49.127373, 28.355731));
                points2.Add(new PointLatLng(49.126699, 28.348178));
                points2.Add(new PointLatLng(49.133663, 28.339595));
                points2.Add(new PointLatLng(49.111869, 28.333072));
                points2.Add(new PointLatLng(49.103103, 28.312473));
                points2.Add(new PointLatLng(49.105126, 28.309726));
                points2.Add(new PointLatLng(49.104227, 28.306293));
                points2.Add(new PointLatLng(49.096809, 28.308696));
                points2.Add(new PointLatLng(49.090514, 28.324832));
                points2.Add(new PointLatLng(49.067576, 28.316936));
                points2.Add(new PointLatLng(49.046428, 28.328265));
                points2.Add(new PointLatLng(49.044853, 28.332385));
                points2.Add(new PointLatLng(49.047553, 28.333759));
                points2.Add(new PointLatLng(49.044853, 28.360538));
                points2.Add(new PointLatLng(49.032249, 28.388347));
                points2.Add(new PointLatLng(49.037876, 28.403453));
                points2.Add(new PointLatLng(49.030898, 28.425082));
                points2.Add(new PointLatLng(49.037426, 28.435039));
                points2.Add(new PointLatLng(49.035625, 28.474178));
                points2.Add(new PointLatLng(49.022344, 28.506793));
                points2.Add(new PointLatLng(49.024145, 28.511943));
                points2.Add(new PointLatLng(49.029998, 28.513660));
                points2.Add(new PointLatLng(49.027972, 28.527049));
                points2.Add(new PointLatLng(49.006582, 28.555202));
                points2.Add(new PointLatLng(49.000050, 28.565501));
                points2.Add(new PointLatLng(49.003654, 28.574771));
                points2.Add(new PointLatLng(49.015589, 28.573398));
                points2.Add(new PointLatLng(49.019192, 28.575458));
                points2.Add(new PointLatLng(49.022119, 28.584041));
                points2.Add(new PointLatLng(49.012887, 28.588504));
                points2.Add(new PointLatLng(49.011536, 28.596744));
                points2.Add(new PointLatLng(49.017391, 28.607387));
                points2.Add(new PointLatLng(49.018066, 28.627986));
                points2.Add(new PointLatLng(49.014463, 28.633823));
                points2.Add(new PointLatLng(49.014689, 28.642406));
                points2.Add(new PointLatLng(49.018066, 28.643436));
                points2.Add(new PointLatLng(49.021218, 28.655109));
                points2.Add(new PointLatLng(49.017841, 28.659915));
                points2.Add(new PointLatLng(49.007257, 28.658885));
                points2.Add(new PointLatLng(49.006807, 28.665408));
                points2.Add(new PointLatLng(49.001176, 28.666095));
                points2.Add(new PointLatLng(48.991941, 28.653735));
                points2.Add(new PointLatLng(48.976620, 28.656139));
                points2.Add(new PointLatLng(48.959265, 28.681545));
                points2.Add(new PointLatLng(48.919575, 28.659915));
                points2.Add(new PointLatLng(48.906488, 28.637256));
                points2.Add(new PointLatLng(48.894978, 28.632449));
                points2.Add(new PointLatLng(48.887754, 28.656482));
                points2.Add(new PointLatLng(48.871724, 28.666095));
                points2.Add(new PointLatLng(48.870595, 28.681201));
                points2.Add(new PointLatLng(48.874659, 28.708324));
                points2.Add(new PointLatLng(48.870561, 28.715807));
                points2.Add(new PointLatLng(48.864915, 28.713747));
                points2.Add(new PointLatLng(48.858139, 28.726107));
                points2.Add(new PointLatLng(48.862431, 28.742586));
                points2.Add(new PointLatLng(48.874174, 28.747049));
                points2.Add(new PointLatLng(48.886367, 28.767649));
                points2.Add(new PointLatLng(48.893364, 28.771082));
                points2.Add(new PointLatLng(48.897202, 28.785158));
                points2.Add(new PointLatLng(48.902618, 28.791338));
                points2.Add(new PointLatLng(48.901264, 28.803011));
                points2.Add(new PointLatLng(48.875078, 28.809191));
                points2.Add(new PointLatLng(48.870787, 28.813654));
                points2.Add(new PointLatLng(48.872594, 28.819490));
                points2.Add(new PointLatLng(48.875981, 28.824984));
                points2.Add(new PointLatLng(48.869658, 28.827730));
                points2.Add(new PointLatLng(48.848652, 28.824640));
                points2.Add(new PointLatLng(48.845263, 28.828417));
                points2.Add(new PointLatLng(48.846392, 28.833567));
                points2.Add(new PointLatLng(48.853396, 28.844896));
                points2.Add(new PointLatLng(48.848652, 28.874079));
                points2.Add(new PointLatLng(48.833286, 28.900858));
                points2.Add(new PointLatLng(48.837128, 28.911501));
                points2.Add(new PointLatLng(48.832834, 28.923174));
                points2.Add(new PointLatLng(48.825150, 28.969523));
                points2.Add(new PointLatLng(48.826732, 28.993898));
                points2.Add(new PointLatLng(48.831930, 28.998018));
                points2.Add(new PointLatLng(48.850233, 28.991152));
                points2.Add(new PointLatLng(48.855203, 28.994928));
                points2.Add(new PointLatLng(48.851589, 29.043680));
                points2.Add(new PointLatLng(48.853848, 29.047800));
                points2.Add(new PointLatLng(48.864915, 29.044024));
                points2.Add(new PointLatLng(48.873497, 29.057070));
                points2.Add(new PointLatLng(48.879368, 29.064966));
                points2.Add(new PointLatLng(48.873271, 29.072863));
                points2.Add(new PointLatLng(48.870561, 29.082819));
                points2.Add(new PointLatLng(48.864464, 29.085566));
                points2.Add(new PointLatLng(48.855203, 29.111658));
                points2.Add(new PointLatLng(48.848878, 29.116121));
                points2.Add(new PointLatLng(48.843455, 29.113375));
                points2.Add(new PointLatLng(48.836224, 29.130198));
                points2.Add(new PointLatLng(48.817690, 29.142901));
                points2.Add(new PointLatLng(48.811360, 29.139124));
                points2.Add(new PointLatLng(48.819047, 29.116121));
                points2.Add(new PointLatLng(48.817690, 29.105135));
                points2.Add(new PointLatLng(48.811134, 29.099642));
                points2.Add(new PointLatLng(48.802994, 29.111315));
                points2.Add(new PointLatLng(48.778566, 29.116465));
                points2.Add(new PointLatLng(48.774268, 29.112345));
                points2.Add(new PointLatLng(48.769968, 29.074236));
                points2.Add(new PointLatLng(48.751184, 29.079386));
                points2.Add(new PointLatLng(48.732845, 29.098269));
                points2.Add(new PointLatLng(48.726051, 29.136034));
                points2.Add(new PointLatLng(48.707250, 29.164187));
                points2.Add(new PointLatLng(48.708383, 29.172083));
                points2.Add(new PointLatLng(48.721748, 29.178606));
                points2.Add(new PointLatLng(48.708610, 29.236284));
                points2.Add(new PointLatLng(48.695241, 29.237658));
                points2.Add(new PointLatLng(48.679829, 29.273363));
                points2.Add(new PointLatLng(48.673935, 29.278513));
                points2.Add(new PointLatLng(48.664185, 29.305635));
                points2.Add(new PointLatLng(48.629480, 29.310099));
                points2.Add(new PointLatLng(48.626530, 29.318338));
                points2.Add(new PointLatLng(48.620856, 29.316279));
                points2.Add(new PointLatLng(48.606784, 29.301859));
                points2.Add(new PointLatLng(48.597929, 29.305979));
                points2.Add(new PointLatLng(48.592480, 29.319025));
                points2.Add(new PointLatLng(48.582487, 29.321085));
                points2.Add(new PointLatLng(48.528403, 29.391123));
                points2.Add(new PointLatLng(48.515669, 29.389063));
                points2.Add(new PointLatLng(48.509301, 29.398333));
                points2.Add(new PointLatLng(48.489281, 29.466654));
                points2.Add(new PointLatLng(48.496107, 29.486567));
                points2.Add(new PointLatLng(48.507936, 29.538065));
                points2.Add(new PointLatLng(48.495652, 29.576517));
                points2.Add(new PointLatLng(48.501112, 29.616343));
                points2.Add(new PointLatLng(48.489737, 29.657541));
                points2.Add(new PointLatLng(48.491557, 29.672648));
                points2.Add(new PointLatLng(48.453317, 29.716593));
                points2.Add(new PointLatLng(48.433731, 29.741999));
                points2.Add(new PointLatLng(48.417328, 29.728952));
                points2.Add(new PointLatLng(48.375840, 29.742685));
                points2.Add(new PointLatLng(48.373103, 29.756418));
                points2.Add(new PointLatLng(48.371278, 29.785257));
                points2.Add(new PointLatLng(48.360786, 29.806543));
                points2.Add(new PointLatLng(48.334318, 29.814096));
                points2.Add(new PointLatLng(48.333861, 29.849115));
                points2.Add(new PointLatLng(48.324731, 29.876581));
                points2.Add(new PointLatLng(48.299157, 29.884821));
                points2.Add(new PointLatLng(48.292305, 29.897867));
                points2.Add(new PointLatLng(48.298701, 29.937693));
                points2.Add(new PointLatLng(48.281797, 29.945246));
                points2.Add(new PointLatLng(48.278141, 29.965159));
                points2.Add(new PointLatLng(48.220987, 29.991938));
                points2.Add(new PointLatLng(48.208176, 30.009790));
                points2.Add(new PointLatLng(48.195820, 29.999491));
                points2.Add(new PointLatLng(48.184376, 30.001551));
                points2.Add(new PointLatLng(48.181629, 30.010477));
                points2.Add(new PointLatLng(48.185291, 30.022150));
                points2.Add(new PointLatLng(48.151403, 30.048243));
                points2.Add(new PointLatLng(48.152319, 30.061289));
                points2.Add(new PointLatLng(48.145447, 30.067469));
                points2.Add(new PointLatLng(48.140865, 30.107294));
                points2.Add(new PointLatLng(48.144072, 30.150553));
                points2.Add(new PointLatLng(48.154609, 30.186258));
                points2.Add(new PointLatLng(48.140865, 30.243250));
                points2.Add(new PointLatLng(48.147280, 30.263849));
                points2.Add(new PointLatLng(48.142240, 30.316034));
                points2.Add(new PointLatLng(48.148196, 30.328394));
                points2.Add(new PointLatLng(48.160564, 30.328394));
                points2.Add(new PointLatLng(48.173845, 30.364786));
                points2.Add(new PointLatLng(48.175219, 30.384699));
                points2.Add(new PointLatLng(48.166518, 30.389505));
                points2.Add(new PointLatLng(48.165144, 30.427271));
                points2.Add(new PointLatLng(48.172013, 30.436884));
                points2.Add(new PointLatLng(48.164228, 30.467783));
                points2.Add(new PointLatLng(48.169266, 30.497309));
                points2.Add(new PointLatLng(48.127576, 30.552240));
                points2.Add(new PointLatLng(48.121617, 30.542627));
                points2.Add(new PointLatLng(48.110615, 30.552927));
                points2.Add(new PointLatLng(48.101445, 30.590006));
                points2.Add(new PointLatLng(48.112449, 30.596872));
                points2.Add(new PointLatLng(48.122992, 30.583826));
                points2.Add(new PointLatLng(48.139949, 30.596872));
                points2.Add(new PointLatLng(48.122534, 30.628458));
                points2.Add(new PointLatLng(48.110615, 30.627085));
                points2.Add(new PointLatLng(48.084017, 30.709482));
                points2.Add(new PointLatLng(48.049143, 30.715662));
                points2.Add(new PointLatLng(48.039044, 30.708796));
                points2.Add(new PointLatLng(48.033075, 30.721155));
                points2.Add(new PointLatLng(48.041339, 30.745188));
                points2.Add(new PointLatLng(48.026187, 30.772654));
                points2.Add(new PointLatLng(48.017461, 30.774714));
                points2.Add(new PointLatLng(48.008275, 30.796686));
                points2.Add(new PointLatLng(48.009193, 30.814539));
                points2.Add(new PointLatLng(48.019299, 30.813852));
                points2.Add(new PointLatLng(48.048684, 30.846811));
                points2.Add(new PointLatLng(48.044094, 30.863291));
                points2.Add(new PointLatLng(48.034453, 30.872904));
                points2.Add(new PointLatLng(48.028483, 30.940882));
                points2.Add(new PointLatLng(48.032616, 30.956675));
                points2.Add(new PointLatLng(48.023432, 30.961481));
                points2.Add(new PointLatLng(47.998167, 30.993067));
                points2.Add(new PointLatLng(47.995410, 31.008860));
                points2.Add(new PointLatLng(47.958178, 31.045939));
                points2.Add(new PointLatLng(47.927360, 31.050058));
                points2.Add(new PointLatLng(47.904810, 31.091944));
                points2.Add(new PointLatLng(47.862906, 31.125589));
                points2.Add(new PointLatLng(47.845397, 31.124216));
                points2.Add(new PointLatLng(47.832492, 31.110483));
                points2.Add(new PointLatLng(47.822350, 31.131769));
                points2.Add(new PointLatLng(47.822350, 31.159922));
                points2.Add(new PointLatLng(47.812668, 31.177088));
                points2.Add(new PointLatLng(47.803906, 31.172281));
                points2.Add(new PointLatLng(47.711122, 31.199747));
                points2.Add(new PointLatLng(47.703267, 31.227213));
                points2.Add(new PointLatLng(47.710198, 31.234766));
                points2.Add(new PointLatLng(47.702343, 31.253305));
                points2.Add(new PointLatLng(47.675072, 31.271158));
                points2.Add(new PointLatLng(47.666749, 31.229960));
                points2.Add(new PointLatLng(47.649637, 31.222406));
                points2.Add(new PointLatLng(47.629280, 31.215540));
                points2.Add(new PointLatLng(47.631131, 31.198374));
                points2.Add(new PointLatLng(47.607989, 31.180521));
                points2.Add(new PointLatLng(47.598266, 31.183954));
                points2.Add(new PointLatLng(47.557968, 31.302744));
                points2.Add(new PointLatLng(47.534330, 31.319223));
                points2.Add(new PointLatLng(47.537575, 31.374842));
                points2.Add(new PointLatLng(47.529694, 31.406427));
                points2.Add(new PointLatLng(47.482383, 31.424967));
                points2.Add(new PointLatLng(47.461032, 31.440760));
                points2.Add(new PointLatLng(47.448961, 31.480585));
                points2.Add(new PointLatLng(47.429455, 31.491571));
                points2.Add(new PointLatLng(47.422022, 31.512857));
                points2.Add(new PointLatLng(47.412265, 31.523844));
                points2.Add(new PointLatLng(47.400647, 31.516977));
                points2.Add(new PointLatLng(47.388097, 31.527964));
                points2.Add(new PointLatLng(47.383913, 31.547876));
                points2.Add(new PointLatLng(47.388097, 31.560236));
                points2.Add(new PointLatLng(47.377404, 31.600061));
                points2.Add(new PointLatLng(47.351359, 31.630274));
                points2.Add(new PointLatLng(47.334143, 31.674219));
                points2.Add(new PointLatLng(47.329490, 31.723658));
                points2.Add(new PointLatLng(47.315526, 31.720224));
                points2.Add(new PointLatLng(47.291313, 31.747004));
                points2.Add(new PointLatLng(47.295039, 31.761423));
                points2.Add(new PointLatLng(47.281066, 31.778589));
                points2.Add(new PointLatLng(47.266158, 31.755243));
                points2.Add(new PointLatLng(47.234464, 31.741510));
                points2.Add(new PointLatLng(47.209281, 31.755930));
                points2.Add(new PointLatLng(47.186420, 31.825968));
                points2.Add(new PointLatLng(47.158880, 31.850000));
                points2.Add(new PointLatLng(47.123851, 31.834208));
                points2.Add(new PointLatLng(47.104693, 31.893259));
                points2.Add(new PointLatLng(47.068693, 31.897379));
                points2.Add(new PointLatLng(47.035476, 31.852060));
                points2.Add(new PointLatLng(46.994747, 31.886393));
                points2.Add(new PointLatLng(47.015169, 31.955196));
                points2.Add(new PointLatLng(46.995502, 31.976482));
                points2.Add(new PointLatLng(46.948649, 31.918117));
                points2.Add(new PointLatLng(46.926613, 31.954509));
                points2.Add(new PointLatLng(46.939273, 31.997768));
                points2.Add(new PointLatLng(46.882046, 31.997768));
                points2.Add(new PointLatLng(46.797501, 31.896831));
                points2.Add(new PointLatLng(46.734947, 31.929790));
                points2.Add(new PointLatLng(46.643571, 31.940776));
                points2.Add(new PointLatLng(46.734947, 31.929790));
                points2.Add(new PointLatLng(46.797501, 31.896831));
                points2.Add(new PointLatLng(46.882046, 31.997768));
                points2.Add(new PointLatLng(46.939273, 31.997768));
                points2.Add(new PointLatLng(46.926613, 31.954509));
                points2.Add(new PointLatLng(46.948649, 31.918117));
                points2.Add(new PointLatLng(46.995502, 31.976482));
                points2.Add(new PointLatLng(47.015169, 31.955196));
                points2.Add(new PointLatLng(46.994747, 31.886393));
                points2.Add(new PointLatLng(47.035476, 31.852060));
                points2.Add(new PointLatLng(47.068693, 31.897379));
                points2.Add(new PointLatLng(47.104693, 31.893259));
                points2.Add(new PointLatLng(47.123851, 31.834208));
                points2.Add(new PointLatLng(47.158880, 31.850000));
                points2.Add(new PointLatLng(47.186420, 31.825968));
                points2.Add(new PointLatLng(47.209281, 31.755930));
                points2.Add(new PointLatLng(47.234464, 31.741510));
                points2.Add(new PointLatLng(47.266158, 31.755243));
                points2.Add(new PointLatLng(47.281066, 31.778589));
                points2.Add(new PointLatLng(47.295039, 31.761423));
                points2.Add(new PointLatLng(47.291313, 31.747004));
                points2.Add(new PointLatLng(47.315526, 31.720224));
                points2.Add(new PointLatLng(47.329490, 31.723658));
                points2.Add(new PointLatLng(47.334143, 31.674219));
                points2.Add(new PointLatLng(47.351359, 31.630274));
                points2.Add(new PointLatLng(47.377404, 31.600061));
                points2.Add(new PointLatLng(47.388097, 31.560236));
                points2.Add(new PointLatLng(47.383913, 31.547876));
                points2.Add(new PointLatLng(47.388097, 31.527964));
                points2.Add(new PointLatLng(47.400647, 31.516977));
                points2.Add(new PointLatLng(47.412265, 31.523844));
                points2.Add(new PointLatLng(47.422022, 31.512857));
                points2.Add(new PointLatLng(47.429455, 31.491571));
                points2.Add(new PointLatLng(47.448961, 31.480585));
                points2.Add(new PointLatLng(47.461032, 31.440760));
                points2.Add(new PointLatLng(47.482383, 31.424967));
                points2.Add(new PointLatLng(47.529694, 31.406427));
                points2.Add(new PointLatLng(47.537575, 31.374842));
                points2.Add(new PointLatLng(47.534330, 31.319223));
                points2.Add(new PointLatLng(47.557968, 31.302744));
                points2.Add(new PointLatLng(47.598266, 31.183954));
                points2.Add(new PointLatLng(47.607989, 31.180521));
                points2.Add(new PointLatLng(47.631131, 31.198374));
                points2.Add(new PointLatLng(47.629280, 31.215540));
                points2.Add(new PointLatLng(47.649637, 31.222406));
                points2.Add(new PointLatLng(47.666749, 31.229960));
                points2.Add(new PointLatLng(47.675072, 31.271158));
                points2.Add(new PointLatLng(47.702343, 31.253305));
                points2.Add(new PointLatLng(47.710198, 31.234766));
                points2.Add(new PointLatLng(47.703267, 31.227213));
                points2.Add(new PointLatLng(47.711122, 31.199747));
                points2.Add(new PointLatLng(47.803906, 31.172281));
                points2.Add(new PointLatLng(47.812668, 31.177088));
                points2.Add(new PointLatLng(47.822350, 31.159922));
                points2.Add(new PointLatLng(47.822350, 31.131769));
                points2.Add(new PointLatLng(47.832492, 31.110483));
                points2.Add(new PointLatLng(47.845397, 31.124216));
                points2.Add(new PointLatLng(47.862906, 31.125589));
                points2.Add(new PointLatLng(47.904810, 31.091944));
                points2.Add(new PointLatLng(47.927360, 31.050058));
                points2.Add(new PointLatLng(47.958178, 31.045939));
                points2.Add(new PointLatLng(47.995410, 31.008860));
                points2.Add(new PointLatLng(47.998167, 30.993067));
                points2.Add(new PointLatLng(48.023432, 30.961481));
                points2.Add(new PointLatLng(48.032616, 30.956675));
                points2.Add(new PointLatLng(48.028483, 30.940882));
                points2.Add(new PointLatLng(48.034453, 30.872904));
                points2.Add(new PointLatLng(48.044094, 30.863291));
                points2.Add(new PointLatLng(48.048684, 30.846811));
                points2.Add(new PointLatLng(48.019299, 30.813852));
                points2.Add(new PointLatLng(48.009193, 30.814539));
                points2.Add(new PointLatLng(48.008275, 30.796686));
                points2.Add(new PointLatLng(48.017461, 30.774714));
                points2.Add(new PointLatLng(48.026187, 30.772654));
                points2.Add(new PointLatLng(48.041339, 30.745188));
                points2.Add(new PointLatLng(48.033075, 30.721155));
                points2.Add(new PointLatLng(48.039044, 30.708796));
                points2.Add(new PointLatLng(48.049143, 30.715662));
                points2.Add(new PointLatLng(48.084017, 30.709482));
                points2.Add(new PointLatLng(48.110615, 30.627085));
                points2.Add(new PointLatLng(48.122534, 30.628458));
                points2.Add(new PointLatLng(48.139949, 30.596872));
                points2.Add(new PointLatLng(48.122992, 30.583826));
                points2.Add(new PointLatLng(48.112449, 30.596872));
                points2.Add(new PointLatLng(48.101445, 30.590006));
                points2.Add(new PointLatLng(48.110615, 30.552927));
                points2.Add(new PointLatLng(48.121617, 30.542627));
                points2.Add(new PointLatLng(48.127576, 30.552240));
                points2.Add(new PointLatLng(48.169266, 30.497309));
                points2.Add(new PointLatLng(48.164228, 30.467783));
                points2.Add(new PointLatLng(48.172013, 30.436884));
                points2.Add(new PointLatLng(48.165144, 30.427271));
                points2.Add(new PointLatLng(48.166518, 30.389505));
                points2.Add(new PointLatLng(48.175219, 30.384699));
                points2.Add(new PointLatLng(48.173845, 30.364786));
                points2.Add(new PointLatLng(48.160564, 30.328394));
                points2.Add(new PointLatLng(48.148196, 30.328394));
                points2.Add(new PointLatLng(48.142240, 30.316034));
                points2.Add(new PointLatLng(48.147280, 30.263849));
                points2.Add(new PointLatLng(48.140865, 30.243250));
                points2.Add(new PointLatLng(48.154609, 30.186258));
                points2.Add(new PointLatLng(48.144072, 30.150553));
                points2.Add(new PointLatLng(48.140865, 30.107294));
                points2.Add(new PointLatLng(48.145447, 30.067469));
                points2.Add(new PointLatLng(48.152319, 30.061289));
                points2.Add(new PointLatLng(48.151403, 30.048243));
                points2.Add(new PointLatLng(48.185291, 30.022150));
                points2.Add(new PointLatLng(48.181629, 30.010477));
                points2.Add(new PointLatLng(48.184376, 30.001551));
                points2.Add(new PointLatLng(48.195820, 29.999491));
                points2.Add(new PointLatLng(48.208176, 30.009790));
                points2.Add(new PointLatLng(48.220987, 29.991938));
                points2.Add(new PointLatLng(48.278141, 29.965159));
                points2.Add(new PointLatLng(48.281797, 29.945246));
                points2.Add(new PointLatLng(48.298701, 29.937693));
                points2.Add(new PointLatLng(48.292305, 29.897867));
                points2.Add(new PointLatLng(48.299157, 29.884821));
                points2.Add(new PointLatLng(48.324731, 29.876581));
                points2.Add(new PointLatLng(48.333861, 29.849115));
                points2.Add(new PointLatLng(48.334318, 29.814096));
                points2.Add(new PointLatLng(48.360786, 29.806543));
                points2.Add(new PointLatLng(48.371278, 29.785257));
                points2.Add(new PointLatLng(48.373103, 29.756418));
                points2.Add(new PointLatLng(48.375840, 29.742685));
                points2.Add(new PointLatLng(48.417328, 29.728952));
                points2.Add(new PointLatLng(48.433731, 29.741999));
                points2.Add(new PointLatLng(48.453317, 29.716593));
                points2.Add(new PointLatLng(48.491557, 29.672648));
                points2.Add(new PointLatLng(48.489737, 29.657541));
                points2.Add(new PointLatLng(48.501112, 29.616343));
                points2.Add(new PointLatLng(48.495652, 29.576517));
                points2.Add(new PointLatLng(48.507936, 29.538065));
                points2.Add(new PointLatLng(48.496107, 29.486567));
                points2.Add(new PointLatLng(48.489281, 29.466654));
                points2.Add(new PointLatLng(48.509301, 29.398333));
                points2.Add(new PointLatLng(48.515669, 29.389063));
                points2.Add(new PointLatLng(48.528403, 29.391123));
                points2.Add(new PointLatLng(48.582487, 29.321085));
                points2.Add(new PointLatLng(48.592480, 29.319025));
                points2.Add(new PointLatLng(48.597929, 29.305979));
                points2.Add(new PointLatLng(48.606784, 29.301859));
                points2.Add(new PointLatLng(48.620856, 29.316279));
                points2.Add(new PointLatLng(48.626530, 29.318338));
                points2.Add(new PointLatLng(48.629480, 29.310099));
                points2.Add(new PointLatLng(48.664185, 29.305635));
                points2.Add(new PointLatLng(48.673935, 29.278513));
                points2.Add(new PointLatLng(48.679829, 29.273363));
                points2.Add(new PointLatLng(48.695241, 29.237658));
                points2.Add(new PointLatLng(48.708610, 29.236284));
                points2.Add(new PointLatLng(48.721748, 29.178606));
                points2.Add(new PointLatLng(48.708383, 29.172083));
                points2.Add(new PointLatLng(48.707250, 29.164187));
                points2.Add(new PointLatLng(48.726051, 29.136034));
                points2.Add(new PointLatLng(48.732845, 29.098269));
                points2.Add(new PointLatLng(48.751184, 29.079386));
                points2.Add(new PointLatLng(48.769968, 29.074236));
                points2.Add(new PointLatLng(48.774268, 29.112345));
                points2.Add(new PointLatLng(48.778566, 29.116465));
                points2.Add(new PointLatLng(48.802994, 29.111315));
                points2.Add(new PointLatLng(48.811134, 29.099642));
                points2.Add(new PointLatLng(48.817690, 29.105135));
                points2.Add(new PointLatLng(48.819047, 29.116121));
                points2.Add(new PointLatLng(48.811360, 29.139124));
                points2.Add(new PointLatLng(48.817690, 29.142901));
                points2.Add(new PointLatLng(48.836224, 29.130198));
                points2.Add(new PointLatLng(48.843455, 29.113375));
                points2.Add(new PointLatLng(48.848878, 29.116121));
                points2.Add(new PointLatLng(48.855203, 29.111658));
                points2.Add(new PointLatLng(48.864464, 29.085566));
                points2.Add(new PointLatLng(48.870561, 29.082819));
                points2.Add(new PointLatLng(48.873271, 29.072863));
                points2.Add(new PointLatLng(48.879368, 29.064966));
                points2.Add(new PointLatLng(48.873497, 29.057070));
                points2.Add(new PointLatLng(48.864915, 29.044024));
                points2.Add(new PointLatLng(48.853848, 29.047800));
                points2.Add(new PointLatLng(48.851589, 29.043680));
                points2.Add(new PointLatLng(48.855203, 28.994928));
                points2.Add(new PointLatLng(48.850233, 28.991152));
                points2.Add(new PointLatLng(48.831930, 28.998018));
                points2.Add(new PointLatLng(48.826732, 28.993898));
                points2.Add(new PointLatLng(48.825150, 28.969523));
                points2.Add(new PointLatLng(48.832834, 28.923174));
                points2.Add(new PointLatLng(48.837128, 28.911501));
                points2.Add(new PointLatLng(48.833286, 28.900858));
                points2.Add(new PointLatLng(48.848652, 28.874079));
                points2.Add(new PointLatLng(48.853396, 28.844896));
                points2.Add(new PointLatLng(48.846392, 28.833567));
                points2.Add(new PointLatLng(48.845263, 28.828417));
                points2.Add(new PointLatLng(48.848652, 28.824640));
                points2.Add(new PointLatLng(48.869658, 28.827730));
                points2.Add(new PointLatLng(48.875981, 28.824984));
                points2.Add(new PointLatLng(48.872594, 28.819490));
                points2.Add(new PointLatLng(48.870787, 28.813654));
                points2.Add(new PointLatLng(48.875078, 28.809191));
                points2.Add(new PointLatLng(48.901264, 28.803011));
                points2.Add(new PointLatLng(48.902618, 28.791338));
                points2.Add(new PointLatLng(48.897202, 28.785158));
                points2.Add(new PointLatLng(48.893364, 28.771082));
                points2.Add(new PointLatLng(48.886367, 28.767649));
                points2.Add(new PointLatLng(48.874174, 28.747049));
                points2.Add(new PointLatLng(48.862431, 28.742586));
                points2.Add(new PointLatLng(48.858139, 28.726107));
                points2.Add(new PointLatLng(48.864915, 28.713747));
                points2.Add(new PointLatLng(48.870561, 28.715807));
                points2.Add(new PointLatLng(48.874659, 28.708324));
                points2.Add(new PointLatLng(48.870595, 28.681201));
                points2.Add(new PointLatLng(48.871724, 28.666095));
                points2.Add(new PointLatLng(48.887754, 28.656482));
                points2.Add(new PointLatLng(48.894978, 28.632449));
                points2.Add(new PointLatLng(48.906488, 28.637256));
                points2.Add(new PointLatLng(48.919575, 28.659915));
                points2.Add(new PointLatLng(48.959265, 28.681545));
                points2.Add(new PointLatLng(48.976620, 28.656139));
                points2.Add(new PointLatLng(48.991941, 28.653735));
                points2.Add(new PointLatLng(49.001176, 28.666095));
                points2.Add(new PointLatLng(49.006807, 28.665408));
                points2.Add(new PointLatLng(49.007257, 28.658885));
                points2.Add(new PointLatLng(49.017841, 28.659915));
                points2.Add(new PointLatLng(49.021218, 28.655109));
                points2.Add(new PointLatLng(49.018066, 28.643436));
                points2.Add(new PointLatLng(49.014689, 28.642406));
                points2.Add(new PointLatLng(49.014463, 28.633823));
                points2.Add(new PointLatLng(49.018066, 28.627986));
                points2.Add(new PointLatLng(49.017391, 28.607387));
                points2.Add(new PointLatLng(49.011536, 28.596744));
                points2.Add(new PointLatLng(49.012887, 28.588504));
                points2.Add(new PointLatLng(49.022119, 28.584041));
                points2.Add(new PointLatLng(49.019192, 28.575458));
                points2.Add(new PointLatLng(49.015589, 28.573398));
                points2.Add(new PointLatLng(49.003654, 28.574771));
                points2.Add(new PointLatLng(49.000050, 28.565501));
                points2.Add(new PointLatLng(49.006582, 28.555202));
                points2.Add(new PointLatLng(49.027972, 28.527049));
                points2.Add(new PointLatLng(49.029998, 28.513660));
                points2.Add(new PointLatLng(49.024145, 28.511943));
                points2.Add(new PointLatLng(49.022344, 28.506793));
                points2.Add(new PointLatLng(49.035625, 28.474178));
                points2.Add(new PointLatLng(49.037426, 28.435039));
                points2.Add(new PointLatLng(49.030898, 28.425082));
                points2.Add(new PointLatLng(49.037876, 28.403453));
                points2.Add(new PointLatLng(49.032249, 28.388347));
                points2.Add(new PointLatLng(49.044853, 28.360538));
                points2.Add(new PointLatLng(49.047553, 28.333759));
                points2.Add(new PointLatLng(49.044853, 28.332385));
                points2.Add(new PointLatLng(49.046428, 28.328265));
                points2.Add(new PointLatLng(49.067576, 28.316936));
                points2.Add(new PointLatLng(49.090514, 28.324832));
                points2.Add(new PointLatLng(49.096809, 28.308696));
                points2.Add(new PointLatLng(49.104227, 28.306293));
                points2.Add(new PointLatLng(49.105126, 28.309726));
                points2.Add(new PointLatLng(49.103103, 28.312473));
                points2.Add(new PointLatLng(49.111869, 28.333072));
                points2.Add(new PointLatLng(49.133663, 28.339595));
                points2.Add(new PointLatLng(49.126699, 28.348178));
                points2.Add(new PointLatLng(49.127373, 28.355731));
                points2.Add(new PointLatLng(49.138829, 28.363284));
                points2.Add(new PointLatLng(49.138380, 28.370151));
                points2.Add(new PointLatLng(49.143097, 28.371181));
                points2.Add(new PointLatLng(49.148711, 28.382510));
                points2.Add(new PointLatLng(49.187322, 28.430232));
                points2.Add(new PointLatLng(49.192931, 28.452548));
                points2.Add(new PointLatLng(49.190687, 28.458041));
                points2.Add(new PointLatLng(49.193828, 28.462848));
                points2.Add(new PointLatLng(49.202354, 28.449802));
                points2.Add(new PointLatLng(49.209307, 28.446025));
                points2.Add(new PointLatLng(49.215811, 28.459758));
                points2.Add(new PointLatLng(49.227023, 28.465595));
                points2.Add(new PointLatLng(49.231955, 28.482761));
                points2.Add(new PointLatLng(49.238232, 28.476924));
                points2.Add(new PointLatLng(49.239353, 28.470058));
                points2.Add(new PointLatLng(49.242791, 28.467311));
                points2.Add(new PointLatLng(49.315807, 28.477268));
                points2.Add(new PointLatLng(49.320730, 28.483791));
                points2.Add(new PointLatLng(49.321178, 28.496150));
                points2.Add(new PointLatLng(49.342209, 28.490314));
                points2.Add(new PointLatLng(49.350261, 28.465251));
                points2.Add(new PointLatLng(49.374857, 28.433666));
                points2.Add(new PointLatLng(49.379104, 28.413753));
                points2.Add(new PointLatLng(49.392066, 28.398990));
                points2.Add(new PointLatLng(49.401227, 28.396930));
                points2.Add(new PointLatLng(49.466646, 28.354701));
                points2.Add(new PointLatLng(49.476685, 28.343715));
                points2.Add(new PointLatLng(49.483377, 28.276080));
                points2.Add(new PointLatLng(49.497204, 28.251018));
                points2.Add(new PointLatLng(49.493413, 28.232822));
                points2.Add(new PointLatLng(49.486277, 28.220119));
                points2.Add(new PointLatLng(49.487838, 28.201236));
                points2.Add(new PointLatLng(49.509913, 28.142871));
                points2.Add(new PointLatLng(49.504339, 28.119525));
                points2.Add(new PointLatLng(49.509913, 28.091373));
                points2.Add(new PointLatLng(49.510135, 28.055667));
                points2.Add(new PointLatLng(49.519274, 28.031635));
                points2.Add(new PointLatLng(49.523955, 28.001765));
                points2.Add(new PointLatLng(49.521726, 27.984256));
                points2.Add(new PointLatLng(49.528412, 27.972926));
                points2.Add(new PointLatLng(49.542003, 27.973956));
                points2.Add(new PointLatLng(49.545568, 27.978076));
                points2.Add(new PointLatLng(49.548909, 27.973956));
                points2.Add(new PointLatLng(49.548686, 27.957820));
                points2.Add(new PointLatLng(49.552695, 27.955417));
                points2.Add(new PointLatLng(49.560045, 27.932758));
                points2.Add(new PointLatLng(49.554255, 27.904605));
                points2.Add(new PointLatLng(49.555814, 27.894992));
                points2.Add(new PointLatLng(49.558931, 27.891216));
                points2.Add(new PointLatLng(49.559600, 27.882633));
                points2.Add(new PointLatLng(49.555814, 27.871989));
                points2.Add(new PointLatLng(49.556259, 27.829074));
                points2.Add(new PointLatLng(49.552250, 27.820491));
                points2.Add(new PointLatLng(49.550468, 27.805385));
                points2.Add(new PointLatLng(49.545122, 27.802638));
                points2.Add(new PointLatLng(49.539775, 27.791309));
                points2.Add(new PointLatLng(49.530863, 27.794055));
                points2.Add(new PointLatLng(49.529972, 27.789249));
                points2.Add(new PointLatLng(49.520835, 27.775859));
                points2.Add(new PointLatLng(49.523732, 27.768649));
                points2.Add(new PointLatLng(49.530752, 27.761955));
                points2.Add(new PointLatLng(49.530417, 27.741355));
                points2.Add(new PointLatLng(49.498208, 27.737235));
                points2.Add(new PointLatLng(49.466869, 27.701358));
                points2.Add(new PointLatLng(49.468654, 27.661704));
                points2.Add(new PointLatLng(49.454149, 27.636470));
                points2.Add(new PointLatLng(49.421778, 27.645225));
                points2.Add(new PointLatLng(49.398435, 27.596988));
                points2.Add(new PointLatLng(49.424458, 27.468585));
                points2.Add(new PointLatLng(49.426914, 27.467555));
                points2.Add(new PointLatLng(49.426133, 27.462234));
                points2.Add(new PointLatLng(49.429259, 27.448329));
                points2.Add(new PointLatLng(49.427361, 27.442836));
                points2.Add(new PointLatLng(49.429035, 27.440089));
                points2.Add(new PointLatLng(49.435176, 27.440089));
                points2.Add(new PointLatLng(49.439194, 27.436656));
                points2.Add(new PointLatLng(49.439529, 27.430133));
                points2.Add(new PointLatLng(49.436962, 27.423267));
                points2.Add(new PointLatLng(49.428031, 27.380351));
                points2.Add(new PointLatLng(49.417423, 27.381381));
                points2.Add(new PointLatLng(49.417981, 27.372112));
                points2.Add(new PointLatLng(49.415078, 27.369537));
                points2.Add(new PointLatLng(49.412732, 27.356147));
                points2.Add(new PointLatLng(49.407259, 27.349796));
                points2.Add(new PointLatLng(49.406142, 27.342071));
                points2.Add(new PointLatLng(49.398882, 27.337264));
                points2.Add(new PointLatLng(49.396200, 27.332286));
                points2.Add(new PointLatLng(49.393184, 27.333831));
                points2.Add(new PointLatLng(49.388491, 27.343444));
                points2.Add(new PointLatLng(49.381339, 27.335548));
                points2.Add(new PointLatLng(49.380222, 27.282161));
                points2.Add(new PointLatLng(49.376757, 27.238731));
                points2.Add(new PointLatLng(49.387150, 27.138137));
                points2.Add(new PointLatLng(49.415636, 27.048530));
                points2.Add(new PointLatLng(49.433501, 26.973857));
                points2.Add(new PointLatLng(49.452699, 26.937637));
                points2.Add(new PointLatLng(49.487002, 26.829490));
                points2.Add(new PointLatLng(49.494138, 26.792325));
                points2.Add(new PointLatLng(49.507070, 26.761598));
                points2.Add(new PointLatLng(49.509021, 26.751470));
                points2.Add(new PointLatLng(49.512365, 26.748037));
                points2.Add(new PointLatLng(49.521225, 26.750182));
                points2.Add(new PointLatLng(49.535876, 26.701087));
                points2.Add(new PointLatLng(49.539497, 26.680917));
                points2.Add(new PointLatLng(49.543618, 26.678857));
                points2.Add(new PointLatLng(49.545233, 26.671132));
                points2.Add(new PointLatLng(49.554248, 26.660372));
                points2.Add(new PointLatLng(49.562656, 26.621061));
                points2.Add(new PointLatLng(49.565105, 26.618572));
                points2.Add(new PointLatLng(49.567944, 26.618486));
                points2.Add(new PointLatLng(49.573510, 26.602007));
                points2.Add(new PointLatLng(49.597883, 26.544758));
                points2.Add(new PointLatLng(49.610454, 26.507078));
                points2.Add(new PointLatLng(49.620130, 26.469055));

                List<GMap.NET.PointLatLng> points3 = new List<GMap.NET.PointLatLng>();
                points3.Add(new PointLatLng(50.312905, 36.832963));
                points3.Add(new PointLatLng(50.306766, 36.839143));
                points3.Add(new PointLatLng(50.304573, 36.852876));
                points3.Add(new PointLatLng(50.297117, 36.859056));
                points3.Add(new PointLatLng(50.290098, 36.848756));
                points3.Add(new PointLatLng(50.271669, 36.862489));
                points3.Add(new PointLatLng(50.256306, 36.830903));
                points3.Add(new PointLatLng(50.244452, 36.830217));
                points3.Add(new PointLatLng(50.243134, 36.835023));
                points3.Add(new PointLatLng(50.234790, 36.839830));
                points3.Add(new PointLatLng(50.228201, 36.836397));
                points3.Add(new PointLatLng(50.206233, 36.856309));
                points3.Add(new PointLatLng(50.197882, 36.852876));
                points3.Add(new PointLatLng(50.200080, 36.845323));
                points3.Add(new PointLatLng(50.197003, 36.832277));
                points3.Add(new PointLatLng(50.186892, 36.832963));
                points3.Add(new PointLatLng(50.161826, 36.850816));
                points3.Add(new PointLatLng(50.148188, 36.819917));
                points3.Add(new PointLatLng(50.086547, 36.806184));
                points3.Add(new PointLatLng(50.038501, 36.808244));
                points3.Add(new PointLatLng(50.017327, 36.842576));
                points3.Add(new PointLatLng(49.989965, 36.883088));
                points3.Add(new PointLatLng(49.944915, 36.913988));
                points3.Add(new PointLatLng(49.910879, 36.981965));
                points3.Add(new PointLatLng(49.886110, 36.980592));
                points3.Add(new PointLatLng(49.875048, 36.974412));
                points3.Add(new PointLatLng(49.869074, 36.950380));
                points3.Add(new PointLatLng(49.858894, 36.932184));
                points3.Add(new PointLatLng(49.832770, 36.920167));
                points3.Add(new PointLatLng(49.815050, 36.903001));
                points3.Add(new PointLatLng(49.844062, 36.777345));
                points3.Add(new PointLatLng(49.866197, 36.782495));
                points3.Add(new PointLatLng(49.874606, 36.746446));
                points3.Add(new PointLatLng(49.863542, 36.720697));
                points3.Add(new PointLatLng(49.838527, 36.702157));
                points3.Add(new PointLatLng(49.832327, 36.718637));
                points3.Add(new PointLatLng(49.829226, 36.721040));
                points3.Add(new PointLatLng(49.825240, 36.717607));
                points3.Add(new PointLatLng(49.821696, 36.693231));
                points3.Add(new PointLatLng(49.806632, 36.673318));
                points3.Add(new PointLatLng(49.792449, 36.621477));
                points3.Add(new PointLatLng(49.798654, 36.612207));
                points3.Add(new PointLatLng(49.786021, 36.589891));
                points3.Add(new PointLatLng(49.786243, 36.581651));
                points3.Add(new PointLatLng(49.774050, 36.565172));
                points3.Add(new PointLatLng(49.768285, 36.568948));
                points3.Add(new PointLatLng(49.765402, 36.566888));
                points3.Add(new PointLatLng(49.759858, 36.546289));
                points3.Add(new PointLatLng(49.737009, 36.540452));
                points3.Add(new PointLatLng(49.733458, 36.532556));
                points3.Add(new PointLatLng(49.709931, 36.528093));
                points3.Add(new PointLatLng(49.699717, 36.498910));
                points3.Add(new PointLatLng(49.701715, 36.488954));
                points3.Add(new PointLatLng(49.698163, 36.465608));
                points3.Add(new PointLatLng(49.677950, 36.442262));
                points3.Add(new PointLatLng(49.674840, 36.429559));
                points3.Add(new PointLatLng(49.671952, 36.432992));
                points3.Add(new PointLatLng(49.670174, 36.430246));
                points3.Add(new PointLatLng(49.672174, 36.424753));
                points3.Add(new PointLatLng(49.672396, 36.417543));
                points3.Add(new PointLatLng(49.675507, 36.414110));
                points3.Add(new PointLatLng(49.677062, 36.415140));
                points3.Add(new PointLatLng(49.673951, 36.419946));
                points3.Add(new PointLatLng(49.676173, 36.423036));
                points3.Add(new PointLatLng(49.678839, 36.420289));
                points3.Add(new PointLatLng(49.682393, 36.422693));
                points3.Add(new PointLatLng(49.681949, 36.417543));
                points3.Add(new PointLatLng(49.678839, 36.414453));
                points3.Add(new PointLatLng(49.681283, 36.403467));
                points3.Add(new PointLatLng(49.675507, 36.391107));
                points3.Add(new PointLatLng(49.677950, 36.378061));
                points3.Add(new PointLatLng(49.673285, 36.371881));
                points3.Add(new PointLatLng(49.672396, 36.366388));
                points3.Add(new PointLatLng(49.662841, 36.360208));
                points3.Add(new PointLatLng(49.642837, 36.336175));
                points3.Add(new PointLatLng(49.628829, 36.339265));
                points3.Add(new PointLatLng(49.621489, 36.327249));
                points3.Add(new PointLatLng(49.599910, 36.336862));
                points3.Add(new PointLatLng(49.597240, 36.344415));
                points3.Add(new PointLatLng(49.590341, 36.348192));
                points3.Add(new PointLatLng(49.588116, 36.356431));
                points3.Add(new PointLatLng(49.584554, 36.356775));
                points3.Add(new PointLatLng(49.585890, 36.353341));
                points3.Add(new PointLatLng(49.570975, 36.358835));
                points3.Add(new PointLatLng(49.562736, 36.372568));
                points3.Add(new PointLatLng(49.554942, 36.374971));
                points3.Add(new PointLatLng(49.550710, 36.383211));
                points3.Add(new PointLatLng(49.552714, 36.389047));
                points3.Add(new PointLatLng(49.551823, 36.393510));
                points3.Add(new PointLatLng(49.548928, 36.392824));
                points3.Add(new PointLatLng(49.543136, 36.408616));
                points3.Add(new PointLatLng(49.543136, 36.434366));
                points3.Add(new PointLatLng(49.540017, 36.436082));
                points3.Add(new PointLatLng(49.541799, 36.439172));
                points3.Add(new PointLatLng(49.540685, 36.444665));
                points3.Add(new PointLatLng(49.536898, 36.448099));
                points3.Add(new PointLatLng(49.534224, 36.471788));
                points3.Add(new PointLatLng(49.534670, 36.477624));
                points3.Add(new PointLatLng(49.528876, 36.487924));
                points3.Add(new PointLatLng(49.524865, 36.482431));
                points3.Add(new PointLatLng(49.513275, 36.493417));
                points3.Add(new PointLatLng(49.502351, 36.520540));
                points3.Add(new PointLatLng(49.504581, 36.535989));
                points3.Add(new PointLatLng(49.519739, 36.576158));
                points3.Add(new PointLatLng(49.515950, 36.592981));
                points3.Add(new PointLatLng(49.510823, 36.601221));
                points3.Add(new PointLatLng(49.512829, 36.608430));
                points3.Add(new PointLatLng(49.517510, 36.610834));
                points3.Add(new PointLatLng(49.513052, 36.615983));
                points3.Add(new PointLatLng(49.505250, 36.613923));
                points3.Add(new PointLatLng(49.490533, 36.623536));
                points3.Add(new PointLatLng(49.471573, 36.622850));
                points3.Add(new PointLatLng(49.466888, 36.636583));
                points3.Add(new PointLatLng(49.465995, 36.669885));
                points3.Add(new PointLatLng(49.469565, 36.690141));
                points3.Add(new PointLatLng(49.461086, 36.694948));
                points3.Add(new PointLatLng(49.459301, 36.709710));
                points3.Add(new PointLatLng(49.455507, 36.712457));
                points3.Add(new PointLatLng(49.457070, 36.746789));
                points3.Add(new PointLatLng(49.442338, 36.764985));
                points3.Add(new PointLatLng(49.434078, 36.758806));
                points3.Add(new PointLatLng(49.417107, 36.767732));
                points3.Add(new PointLatLng(49.410406, 36.773569));
                points3.Add(new PointLatLng(49.403927, 36.791421));
                points3.Add(new PointLatLng(49.405491, 36.812021));
                points3.Add(new PointLatLng(49.413086, 36.827814));
                points3.Add(new PointLatLng(49.426486, 36.846010));
                points3.Add(new PointLatLng(49.432515, 36.847040));
                points3.Add(new PointLatLng(49.437650, 36.861459));
                points3.Add(new PointLatLng(49.436534, 36.864892));
                points3.Add(new PointLatLng(49.432292, 36.861116));
                points3.Add(new PointLatLng(49.430952, 36.858369));
                points3.Add(new PointLatLng(49.427826, 36.857339));
                points3.Add(new PointLatLng(49.430729, 36.853906));
                points3.Add(new PointLatLng(49.420680, 36.855279));
                points3.Add(new PointLatLng(49.425147, 36.861459));
                points3.Add(new PointLatLng(49.413756, 36.877595));
                points3.Add(new PointLatLng(49.408172, 36.876222));
                points3.Add(new PointLatLng(49.397001, 36.885492));
                points3.Add(new PointLatLng(49.396331, 36.897165));
                points3.Add(new PointLatLng(49.403704, 36.922914));
                points3.Add(new PointLatLng(49.403481, 36.935617));
                points3.Add(new PointLatLng(49.393426, 36.936647));
                points3.Add(new PointLatLng(49.390298, 36.951410));
                points3.Add(new PointLatLng(49.384040, 36.948663));
                points3.Add(new PointLatLng(49.376441, 36.977159));
                points3.Add(new PointLatLng(49.381135, 36.986085));
                points3.Add(new PointLatLng(49.378676, 36.999818));
                points3.Add(new PointLatLng(49.374876, 37.007715));
                points3.Add(new PointLatLng(49.380911, 37.035180));
                points3.Add(new PointLatLng(49.380687, 37.049257));
                points3.Add(new PointLatLng(49.383593, 37.053033));
                points3.Add(new PointLatLng(49.383593, 37.060586));
                points3.Add(new PointLatLng(49.380240, 37.068139));
                points3.Add(new PointLatLng(49.365710, 37.081529));
                points3.Add(new PointLatLng(49.356766, 37.102472));
                points3.Add(new PointLatLng(49.345136, 37.104188));
                points3.Add(new PointLatLng(49.339544, 37.108652));
                points3.Add(new PointLatLng(49.334175, 37.122384));
                points3.Add(new PointLatLng(49.328358, 37.156030));
                points3.Add(new PointLatLng(49.316721, 37.160837));
                points3.Add(new PointLatLng(49.297022, 37.141267));
                points3.Add(new PointLatLng(49.284931, 37.117578));
                points3.Add(new PointLatLng(49.277764, 37.083589));
                points3.Add(new PointLatLng(49.282915, 37.083589));
                points3.Add(new PointLatLng(49.283811, 37.082559));
                points3.Add(new PointLatLng(49.280003, 37.076036));
                points3.Add(new PointLatLng(49.286722, 37.077752));
                points3.Add(new PointLatLng(49.284483, 37.059556));
                points3.Add(new PointLatLng(49.288962, 37.056810));
                points3.Add(new PointLatLng(49.291873, 37.047197));
                points3.Add(new PointLatLng(49.290977, 37.034837));
                points3.Add(new PointLatLng(49.293216, 37.025567));
                points3.Add(new PointLatLng(49.289185, 37.022821));
                points3.Add(new PointLatLng(49.302395, 37.000505));
                points3.Add(new PointLatLng(49.305082, 36.975442));
                points3.Add(new PointLatLng(49.299485, 36.964456));
                points3.Add(new PointLatLng(49.307097, 36.959993));
                points3.Add(new PointLatLng(49.299709, 36.956903));
                points3.Add(new PointLatLng(49.295679, 36.945917));
                points3.Add(new PointLatLng(49.290305, 36.951066));
                points3.Add(new PointLatLng(49.279556, 36.940423));
                points3.Add(new PointLatLng(49.277092, 36.922227));
                points3.Add(new PointLatLng(49.261186, 36.922571));
                points3.Add(new PointLatLng(49.254016, 36.934930));
                points3.Add(new PointLatLng(49.239224, 36.936990));
                points3.Add(new PointLatLng(49.230033, 36.941110));
                points3.Add(new PointLatLng(49.229136, 36.944200));
                points3.Add(new PointLatLng(49.215906, 36.930124));
                points3.Add(new PointLatLng(49.189212, 36.918794));
                points3.Add(new PointLatLng(49.177318, 36.927034));
                points3.Add(new PointLatLng(49.175074, 36.956560));
                points3.Add(new PointLatLng(49.179787, 36.949350));
                points3.Add(new PointLatLng(49.181582, 36.958276));
                points3.Add(new PointLatLng(49.178889, 36.964113));
                points3.Add(new PointLatLng(49.174625, 36.963083));
                points3.Add(new PointLatLng(49.170584, 36.967203));
                points3.Add(new PointLatLng(49.173502, 36.986085));
                points3.Add(new PointLatLng(49.179787, 36.998788));
                points3.Add(new PointLatLng(49.177542, 37.005655));
                points3.Add(new PointLatLng(49.166768, 37.041360));
                points3.Add(new PointLatLng(49.172605, 37.041017));
                points3.Add(new PointLatLng(49.173502, 37.047883));
                points3.Add(new PointLatLng(49.179562, 37.050630));
                points3.Add(new PointLatLng(49.170136, 37.077752));
                points3.Add(new PointLatLng(49.163625, 37.070886));
                points3.Add(new PointLatLng(49.151950, 37.133371));
                points3.Add(new PointLatLng(49.155767, 37.134401));
                points3.Add(new PointLatLng(49.154869, 37.141267));
                points3.Add(new PointLatLng(49.151950, 37.145044));
                points3.Add(new PointLatLng(49.155543, 37.152597));
                points3.Add(new PointLatLng(49.142293, 37.172166));
                points3.Add(new PointLatLng(49.140272, 37.192422));
                points3.Add(new PointLatLng(49.154420, 37.260057));
                points3.Add(new PointLatLng(49.165197, 37.267953));
                points3.Add(new PointLatLng(49.166544, 37.260400));
                points3.Add(new PointLatLng(49.170584, 37.263490));
                points3.Add(new PointLatLng(49.171033, 37.270357));
                points3.Add(new PointLatLng(49.191680, 37.269670));
                points3.Add(new PointLatLng(49.206711, 37.281686));
                points3.Add(new PointLatLng(49.206935, 37.295762));
                points3.Add(new PointLatLng(49.202897, 37.294389));
                points3.Add(new PointLatLng(49.199084, 37.288209));
                points3.Add(new PointLatLng(49.192802, 37.298509));
                points3.Add(new PointLatLng(49.193475, 37.307435));
                points3.Add(new PointLatLng(49.166095, 37.300912));
                points3.Add(new PointLatLng(49.153747, 37.312929));
                points3.Add(new PointLatLng(49.144539, 37.305375));
                points3.Add(new PointLatLng(49.129490, 37.310525));
                points3.Add(new PointLatLng(49.114211, 37.338678));
                points3.Add(new PointLatLng(49.118705, 37.344514));
                points3.Add(new PointLatLng(49.091059, 37.456781));
                points3.Add(new PointLatLng(49.037971, 37.485963));
                points3.Add(new PointLatLng(49.023115, 37.512742));
                points3.Add(new PointLatLng(49.021989, 37.537462));
                points3.Add(new PointLatLng(49.044723, 37.599603));
                points3.Add(new PointLatLng(49.044948, 37.621576));
                points3.Add(new PointLatLng(49.037521, 37.627069));
                points3.Add(new PointLatLng(49.030769, 37.662431));
                points3.Add(new PointLatLng(49.025141, 37.654878));
                points3.Add(new PointLatLng(49.020188, 37.655565));
                points3.Add(new PointLatLng(49.019738, 37.664148));
                points3.Add(new PointLatLng(49.016135, 37.663804));
                points3.Add(new PointLatLng(48.997217, 37.623292));
                points3.Add(new PointLatLng(48.982574, 37.632905));
                points3.Add(new PointLatLng(48.967926, 37.625696));
                points3.Add(new PointLatLng(48.968377, 37.616083));
                points3.Add(new PointLatLng(48.962967, 37.612993));
                points3.Add(new PointLatLng(48.948765, 37.636339));
                points3.Add(new PointLatLng(48.943353, 37.674104));
                points3.Add(new PointLatLng(48.958459, 37.699167));
                points3.Add(new PointLatLng(48.936137, 37.740365));
                points3.Add(new PointLatLng(48.930724, 37.732126));
                points3.Add(new PointLatLng(48.909968, 37.766115));
                points3.Add(new PointLatLng(48.891010, 37.756502));
                points3.Add(new PointLatLng(48.881980, 37.766458));
                points3.Add(new PointLatLng(48.892816, 37.790834));
                points3.Add(new PointLatLng(48.893719, 37.823106));
                points3.Add(new PointLatLng(48.886270, 37.849542));
                points3.Add(new PointLatLng(48.886270, 37.862932));
                points3.Add(new PointLatLng(48.881980, 37.860528));
                points3.Add(new PointLatLng(48.876787, 37.874605));
                points3.Add(new PointLatLng(48.879723, 37.884904));
                points3.Add(new PointLatLng(48.885367, 37.882844));
                points3.Add(new PointLatLng(48.889205, 37.894517));
                points3.Add(new PointLatLng(48.884690, 37.902757));
                points3.Add(new PointLatLng(48.880851, 37.899667));
                points3.Add(new PointLatLng(48.877690, 37.904474));
                points3.Add(new PointLatLng(48.881303, 37.919237));
                points3.Add(new PointLatLng(48.890785, 37.930566));
                points3.Add(new PointLatLng(48.905907, 37.937776));
                points3.Add(new PointLatLng(48.912450, 37.952196));
                points3.Add(new PointLatLng(48.908614, 37.955972));
                points3.Add(new PointLatLng(48.903650, 37.951166));
                points3.Add(new PointLatLng(48.902747, 37.971765));
                points3.Add(new PointLatLng(48.910420, 37.978631));
                points3.Add(new PointLatLng(48.908840, 38.015710));
                points3.Add(new PointLatLng(48.916963, 38.020860));
                points3.Add(new PointLatLng(48.914030, 38.029443));
                points3.Add(new PointLatLng(48.935234, 38.044206));
                points3.Add(new PointLatLng(48.940196, 38.054506));
                points3.Add(new PointLatLng(48.937264, 38.063775));
                points3.Add(new PointLatLng(48.939519, 38.091585));
                points3.Add(new PointLatLng(48.924633, 38.132097));
                points3.Add(new PointLatLng(48.933430, 38.141710));
                points3.Add(new PointLatLng(48.927791, 38.194925));
                points3.Add(new PointLatLng(48.936588, 38.225480));
                points3.Add(new PointLatLng(48.943804, 38.234064));
                points3.Add(new PointLatLng(48.950117, 38.226510));
                points3.Add(new PointLatLng(48.957332, 38.242303));
                points3.Add(new PointLatLng(48.953725, 38.249856));
                points3.Add(new PointLatLng(48.969278, 38.261873));
                points3.Add(new PointLatLng(49.004650, 38.242990));
                points3.Add(new PointLatLng(49.007128, 38.248483));
                points3.Add(new PointLatLng(49.024240, 38.262559));
                points3.Add(new PointLatLng(49.022664, 38.302041));
                points3.Add(new PointLatLng(48.962742, 38.397828));
                points3.Add(new PointLatLng(48.938392, 38.413621));
                points3.Add(new PointLatLng(48.917865, 38.453790));
                points3.Add(new PointLatLng(48.896428, 38.480569));
                points3.Add(new PointLatLng(48.871142, 38.493272));
                points3.Add(new PointLatLng(48.852621, 38.517648));
                points3.Add(new PointLatLng(48.850814, 38.530351));
                points3.Add(new PointLatLng(48.845392, 38.526575));
                points3.Add(new PointLatLng(48.833416, 38.546144));
                points3.Add(new PointLatLng(48.827087, 38.573610));
                points3.Add(new PointLatLng(48.816915, 38.597642));
                points3.Add(new PointLatLng(48.799053, 38.604509));
                points3.Add(new PointLatLng(48.799279, 38.619272));
                points3.Add(new PointLatLng(48.787744, 38.620645));
                points3.Add(new PointLatLng(48.764667, 38.671457));
                points3.Add(new PointLatLng(48.759461, 38.696519));
                points3.Add(new PointLatLng(48.762630, 38.732568));
                points3.Add(new PointLatLng(48.767609, 38.737375));
                points3.Add(new PointLatLng(48.763988, 38.757631));
                points3.Add(new PointLatLng(48.770098, 38.763124));
                points3.Add(new PointLatLng(48.765119, 38.790246));
                points3.Add(new PointLatLng(48.759009, 38.790246));
                points3.Add(new PointLatLng(48.752898, 38.798143));
                points3.Add(new PointLatLng(48.750634, 38.859598));
                points3.Add(new PointLatLng(48.751539, 38.902856));
                points3.Add(new PointLatLng(48.744295, 38.917619));
                points3.Add(new PointLatLng(48.747918, 38.938562));
                points3.Add(new PointLatLng(48.738182, 38.934099));
                points3.Add(new PointLatLng(48.735918, 38.957101));
                points3.Add(new PointLatLng(48.746786, 38.959848));
                points3.Add(new PointLatLng(48.744748, 38.981477));
                points3.Add(new PointLatLng(48.720745, 39.000017));
                points3.Add(new PointLatLng(48.716215, 38.997270));
                points3.Add(new PointLatLng(48.713496, 38.985597));
                points3.Add(new PointLatLng(48.710778, 38.990747));
                points3.Add(new PointLatLng(48.712364, 39.000017));
                points3.Add(new PointLatLng(48.699223, 39.001733));
                points3.Add(new PointLatLng(48.695597, 39.016496));
                points3.Add(new PointLatLng(48.681772, 39.016839));
                points3.Add(new PointLatLng(48.662727, 39.028512));
                points3.Add(new PointLatLng(48.663408, 39.011346));
                points3.Add(new PointLatLng(48.658646, 39.014436));
                points3.Add(new PointLatLng(48.652976, 39.034006));
                points3.Add(new PointLatLng(48.647986, 39.040529));
                points3.Add(new PointLatLng(48.652749, 39.077608));
                points3.Add(new PointLatLng(48.651388, 39.104387));
                points3.Add(new PointLatLng(48.661820, 39.122583));
                points3.Add(new PointLatLng(48.660687, 39.140092));
                points3.Add(new PointLatLng(48.670437, 39.141466));
                points3.Add(new PointLatLng(48.677465, 39.150049));
                points3.Add(new PointLatLng(48.676785, 39.160692));
                points3.Add(new PointLatLng(48.686758, 39.180948));
                points3.Add(new PointLatLng(48.708059, 39.196741));
                points3.Add(new PointLatLng(48.699903, 39.204980));
                points3.Add(new PointLatLng(48.712590, 39.232789));
                points3.Add(new PointLatLng(48.710551, 39.242746));
                points3.Add(new PointLatLng(48.715535, 39.249612));
                points3.Add(new PointLatLng(48.734786, 39.257852));
                points3.Add(new PointLatLng(48.743163, 39.254075));
                points3.Add(new PointLatLng(48.743390, 39.268838));
                points3.Add(new PointLatLng(48.753350, 39.269525));
                points3.Add(new PointLatLng(48.754256, 39.276391));
                points3.Add(new PointLatLng(48.750181, 39.280168));
                points3.Add(new PointLatLng(48.752219, 39.288408));
                points3.Add(new PointLatLng(48.761046, 39.290468));
                points3.Add(new PointLatLng(48.775302, 39.310724));
                points3.Add(new PointLatLng(48.777112, 39.328576));
                points3.Add(new PointLatLng(48.770098, 39.327203));
                points3.Add(new PointLatLng(48.767382, 39.337503));
                points3.Add(new PointLatLng(48.770098, 39.349176));
                points3.Add(new PointLatLng(48.767382, 39.355699));
                points3.Add(new PointLatLng(48.759009, 39.344369));
                points3.Add(new PointLatLng(48.744295, 39.349519));
                points3.Add(new PointLatLng(48.735918, 39.361192));
                points3.Add(new PointLatLng(48.727539, 39.354669));
                points3.Add(new PointLatLng(48.720065, 39.374925));
                points3.Add(new PointLatLng(48.715309, 39.372178));
                points3.Add(new PointLatLng(48.712590, 39.347803));
                points3.Add(new PointLatLng(48.698996, 39.357072));
                points3.Add(new PointLatLng(48.684945, 39.391061));
                points3.Add(new PointLatLng(48.680185, 39.390031));
                points3.Add(new PointLatLng(48.669303, 39.416124));
                points3.Add(new PointLatLng(48.660913, 39.409257));
                points3.Add(new PointLatLng(48.657285, 39.449083));
                points3.Add(new PointLatLng(48.641407, 39.454233));
                points3.Add(new PointLatLng(48.638912, 39.467966));
                points3.Add(new PointLatLng(48.629382, 39.466592));
                points3.Add(new PointLatLng(48.636189, 39.510881));
                points3.Add(new PointLatLng(48.631878, 39.517404));
                points3.Add(new PointLatLng(48.626433, 39.517061));
                points3.Add(new PointLatLng(48.622802, 39.513627));
                points3.Add(new PointLatLng(48.611226, 39.529420));
                points3.Add(new PointLatLng(48.604189, 39.524270));
                points3.Add(new PointLatLng(48.595107, 39.535257));
                points3.Add(new PointLatLng(48.591020, 39.528390));
                points3.Add(new PointLatLng(48.585570, 39.531137));
                points3.Add(new PointLatLng(48.586024, 39.547616));
                points3.Add(new PointLatLng(48.596015, 39.548990));
                points3.Add(new PointLatLng(48.599194, 39.561693));
                points3.Add(new PointLatLng(48.590339, 39.568216));
                points3.Add(new PointLatLng(48.585115, 39.564096));
                points3.Add(new PointLatLng(48.582163, 39.574396));
                points3.Add(new PointLatLng(48.592382, 39.590875));
                points3.Add(new PointLatLng(48.593064, 39.601518));
                points3.Add(new PointLatLng(48.586705, 39.606325));
                points3.Add(new PointLatLng(48.584888, 39.600145));
                points3.Add(new PointLatLng(48.577620, 39.596368));
                points3.Add(new PointLatLng(48.573986, 39.610101));
                points3.Add(new PointLatLng(48.578983, 39.617311));
                points3.Add(new PointLatLng(48.587614, 39.621087));
                points3.Add(new PointLatLng(48.581936, 39.634134));
                points3.Add(new PointLatLng(48.589430, 39.673616));
                points3.Add(new PointLatLng(48.585797, 39.684945));
                points3.Add(new PointLatLng(48.586932, 39.693185));
                points3.Add(new PointLatLng(48.585797, 39.684945));
                points3.Add(new PointLatLng(48.589430, 39.673616));
                points3.Add(new PointLatLng(48.581936, 39.634134));
                points3.Add(new PointLatLng(48.587614, 39.621087));
                points3.Add(new PointLatLng(48.578983, 39.617311));
                points3.Add(new PointLatLng(48.573986, 39.610101));
                points3.Add(new PointLatLng(48.577620, 39.596368));
                points3.Add(new PointLatLng(48.584888, 39.600145));
                points3.Add(new PointLatLng(48.586705, 39.606325));
                points3.Add(new PointLatLng(48.593064, 39.601518));
                points3.Add(new PointLatLng(48.592382, 39.590875));
                points3.Add(new PointLatLng(48.582163, 39.574396));
                points3.Add(new PointLatLng(48.585115, 39.564096));
                points3.Add(new PointLatLng(48.590339, 39.568216));
                points3.Add(new PointLatLng(48.599194, 39.561693));
                points3.Add(new PointLatLng(48.596015, 39.548990));
                points3.Add(new PointLatLng(48.586024, 39.547616));
                points3.Add(new PointLatLng(48.585570, 39.531137));
                points3.Add(new PointLatLng(48.591020, 39.528390));
                points3.Add(new PointLatLng(48.595107, 39.535257));
                points3.Add(new PointLatLng(48.604189, 39.524270));
                points3.Add(new PointLatLng(48.611226, 39.529420));
                points3.Add(new PointLatLng(48.622802, 39.513627));
                points3.Add(new PointLatLng(48.626433, 39.517061));
                points3.Add(new PointLatLng(48.631878, 39.517404));
                points3.Add(new PointLatLng(48.636189, 39.510881));
                points3.Add(new PointLatLng(48.629382, 39.466592));
                points3.Add(new PointLatLng(48.638912, 39.467966));
                points3.Add(new PointLatLng(48.641407, 39.454233));
                points3.Add(new PointLatLng(48.657285, 39.449083));
                points3.Add(new PointLatLng(48.660913, 39.409257));
                points3.Add(new PointLatLng(48.669303, 39.416124));
                points3.Add(new PointLatLng(48.680185, 39.390031));
                points3.Add(new PointLatLng(48.684945, 39.391061));
                points3.Add(new PointLatLng(48.698996, 39.357072));
                points3.Add(new PointLatLng(48.712590, 39.347803));
                points3.Add(new PointLatLng(48.715309, 39.372178));
                points3.Add(new PointLatLng(48.720065, 39.374925));
                points3.Add(new PointLatLng(48.727539, 39.354669));
                points3.Add(new PointLatLng(48.735918, 39.361192));
                points3.Add(new PointLatLng(48.744295, 39.349519));
                points3.Add(new PointLatLng(48.759009, 39.344369));
                points3.Add(new PointLatLng(48.767382, 39.355699));
                points3.Add(new PointLatLng(48.770098, 39.349176));
                points3.Add(new PointLatLng(48.767382, 39.337503));
                points3.Add(new PointLatLng(48.770098, 39.327203));
                points3.Add(new PointLatLng(48.777112, 39.328576));
                points3.Add(new PointLatLng(48.775302, 39.310724));
                points3.Add(new PointLatLng(48.761046, 39.290468));
                points3.Add(new PointLatLng(48.752219, 39.288408));
                points3.Add(new PointLatLng(48.750181, 39.280168));
                points3.Add(new PointLatLng(48.754256, 39.276391));
                points3.Add(new PointLatLng(48.753350, 39.269525));
                points3.Add(new PointLatLng(48.743390, 39.268838));
                points3.Add(new PointLatLng(48.743163, 39.254075));
                points3.Add(new PointLatLng(48.734786, 39.257852));
                points3.Add(new PointLatLng(48.715535, 39.249612));
                points3.Add(new PointLatLng(48.710551, 39.242746));
                points3.Add(new PointLatLng(48.712590, 39.232789));
                points3.Add(new PointLatLng(48.699903, 39.204980));
                points3.Add(new PointLatLng(48.708059, 39.196741));
                points3.Add(new PointLatLng(48.686758, 39.180948));
                points3.Add(new PointLatLng(48.676785, 39.160692));
                points3.Add(new PointLatLng(48.677465, 39.150049));
                points3.Add(new PointLatLng(48.670437, 39.141466));
                points3.Add(new PointLatLng(48.660687, 39.140092));
                points3.Add(new PointLatLng(48.661820, 39.122583));
                points3.Add(new PointLatLng(48.651388, 39.104387));
                points3.Add(new PointLatLng(48.652749, 39.077608));
                points3.Add(new PointLatLng(48.647986, 39.040529));
                points3.Add(new PointLatLng(48.652976, 39.034006));
                points3.Add(new PointLatLng(48.658646, 39.014436));
                points3.Add(new PointLatLng(48.663408, 39.011346));
                points3.Add(new PointLatLng(48.662727, 39.028512));
                points3.Add(new PointLatLng(48.681772, 39.016839));
                points3.Add(new PointLatLng(48.695597, 39.016496));
                points3.Add(new PointLatLng(48.699223, 39.001733));
                points3.Add(new PointLatLng(48.712364, 39.000017));
                points3.Add(new PointLatLng(48.710778, 38.990747));
                points3.Add(new PointLatLng(48.713496, 38.985597));
                points3.Add(new PointLatLng(48.716215, 38.997270));
                points3.Add(new PointLatLng(48.720745, 39.000017));
                points3.Add(new PointLatLng(48.744748, 38.981477));
                points3.Add(new PointLatLng(48.746786, 38.959848));
                points3.Add(new PointLatLng(48.735918, 38.957101));
                points3.Add(new PointLatLng(48.738182, 38.934099));
                points3.Add(new PointLatLng(48.747918, 38.938562));
                points3.Add(new PointLatLng(48.744295, 38.917619));
                points3.Add(new PointLatLng(48.751539, 38.902856));
                points3.Add(new PointLatLng(48.750634, 38.859598));
                points3.Add(new PointLatLng(48.752898, 38.798143));
                points3.Add(new PointLatLng(48.759009, 38.790246));
                points3.Add(new PointLatLng(48.765119, 38.790246));
                points3.Add(new PointLatLng(48.770098, 38.763124));
                points3.Add(new PointLatLng(48.763988, 38.757631));
                points3.Add(new PointLatLng(48.767609, 38.737375));
                points3.Add(new PointLatLng(48.762630, 38.732568));
                points3.Add(new PointLatLng(48.759461, 38.696519));
                points3.Add(new PointLatLng(48.764667, 38.671457));
                points3.Add(new PointLatLng(48.787744, 38.620645));
                points3.Add(new PointLatLng(48.799279, 38.619272));
                points3.Add(new PointLatLng(48.799053, 38.604509));
                points3.Add(new PointLatLng(48.816915, 38.597642));
                points3.Add(new PointLatLng(48.827087, 38.573610));
                points3.Add(new PointLatLng(48.833416, 38.546144));
                points3.Add(new PointLatLng(48.845392, 38.526575));
                points3.Add(new PointLatLng(48.850814, 38.530351));
                points3.Add(new PointLatLng(48.852621, 38.517648));
                points3.Add(new PointLatLng(48.871142, 38.493272));
                points3.Add(new PointLatLng(48.896428, 38.480569));
                points3.Add(new PointLatLng(48.917865, 38.453790));
                points3.Add(new PointLatLng(48.938392, 38.413621));
                points3.Add(new PointLatLng(48.962742, 38.397828));
                points3.Add(new PointLatLng(49.022664, 38.302041));
                points3.Add(new PointLatLng(49.024240, 38.262559));
                points3.Add(new PointLatLng(49.007128, 38.248483));
                points3.Add(new PointLatLng(49.004650, 38.242990));
                points3.Add(new PointLatLng(48.969278, 38.261873));
                points3.Add(new PointLatLng(48.953725, 38.249856));
                points3.Add(new PointLatLng(48.957332, 38.242303));
                points3.Add(new PointLatLng(48.950117, 38.226510));
                points3.Add(new PointLatLng(48.943804, 38.234064));
                points3.Add(new PointLatLng(48.936588, 38.225480));
                points3.Add(new PointLatLng(48.927791, 38.194925));
                points3.Add(new PointLatLng(48.933430, 38.141710));
                points3.Add(new PointLatLng(48.924633, 38.132097));
                points3.Add(new PointLatLng(48.939519, 38.091585));
                points3.Add(new PointLatLng(48.937264, 38.063775));
                points3.Add(new PointLatLng(48.940196, 38.054506));
                points3.Add(new PointLatLng(48.935234, 38.044206));
                points3.Add(new PointLatLng(48.914030, 38.029443));
                points3.Add(new PointLatLng(48.916963, 38.020860));
                points3.Add(new PointLatLng(48.908840, 38.015710));
                points3.Add(new PointLatLng(48.910420, 37.978631));
                points3.Add(new PointLatLng(48.902747, 37.971765));
                points3.Add(new PointLatLng(48.903650, 37.951166));
                points3.Add(new PointLatLng(48.908614, 37.955972));
                points3.Add(new PointLatLng(48.912450, 37.952196));
                points3.Add(new PointLatLng(48.905907, 37.937776));
                points3.Add(new PointLatLng(48.890785, 37.930566));
                points3.Add(new PointLatLng(48.881303, 37.919237));
                points3.Add(new PointLatLng(48.877690, 37.904474));
                points3.Add(new PointLatLng(48.880851, 37.899667));
                points3.Add(new PointLatLng(48.884690, 37.902757));
                points3.Add(new PointLatLng(48.889205, 37.894517));
                points3.Add(new PointLatLng(48.885367, 37.882844));
                points3.Add(new PointLatLng(48.879723, 37.884904));
                points3.Add(new PointLatLng(48.876787, 37.874605));
                points3.Add(new PointLatLng(48.881980, 37.860528));
                points3.Add(new PointLatLng(48.886270, 37.862932));
                points3.Add(new PointLatLng(48.886270, 37.849542));
                points3.Add(new PointLatLng(48.893719, 37.823106));
                points3.Add(new PointLatLng(48.892816, 37.790834));
                points3.Add(new PointLatLng(48.881980, 37.766458));
                points3.Add(new PointLatLng(48.891010, 37.756502));
                points3.Add(new PointLatLng(48.909968, 37.766115));
                points3.Add(new PointLatLng(48.930724, 37.732126));
                points3.Add(new PointLatLng(48.936137, 37.740365));
                points3.Add(new PointLatLng(48.958459, 37.699167));
                points3.Add(new PointLatLng(48.943353, 37.674104));
                points3.Add(new PointLatLng(48.948765, 37.636339));
                points3.Add(new PointLatLng(48.962967, 37.612993));
                points3.Add(new PointLatLng(48.968377, 37.616083));
                points3.Add(new PointLatLng(48.967926, 37.625696));
                points3.Add(new PointLatLng(48.982574, 37.632905));
                points3.Add(new PointLatLng(48.997217, 37.623292));
                points3.Add(new PointLatLng(49.016135, 37.663804));
                points3.Add(new PointLatLng(49.019738, 37.664148));
                points3.Add(new PointLatLng(49.020188, 37.655565));
                points3.Add(new PointLatLng(49.025141, 37.654878));
                points3.Add(new PointLatLng(49.030769, 37.662431));
                points3.Add(new PointLatLng(49.037521, 37.627069));
                points3.Add(new PointLatLng(49.044948, 37.621576));
                points3.Add(new PointLatLng(49.044723, 37.599603));
                points3.Add(new PointLatLng(49.021989, 37.537462));
                points3.Add(new PointLatLng(49.023115, 37.512742));
                points3.Add(new PointLatLng(49.037971, 37.485963));
                points3.Add(new PointLatLng(49.091059, 37.456781));
                points3.Add(new PointLatLng(49.118705, 37.344514));
                points3.Add(new PointLatLng(49.114211, 37.338678));
                points3.Add(new PointLatLng(49.129490, 37.310525));
                points3.Add(new PointLatLng(49.144539, 37.305375));
                points3.Add(new PointLatLng(49.153747, 37.312929));
                points3.Add(new PointLatLng(49.166095, 37.300912));
                points3.Add(new PointLatLng(49.193475, 37.307435));
                points3.Add(new PointLatLng(49.192802, 37.298509));
                points3.Add(new PointLatLng(49.199084, 37.288209));
                points3.Add(new PointLatLng(49.202897, 37.294389));
                points3.Add(new PointLatLng(49.206935, 37.295762));
                points3.Add(new PointLatLng(49.206711, 37.281686));
                points3.Add(new PointLatLng(49.191680, 37.269670));
                points3.Add(new PointLatLng(49.171033, 37.270357));
                points3.Add(new PointLatLng(49.170584, 37.263490));
                points3.Add(new PointLatLng(49.166544, 37.260400));
                points3.Add(new PointLatLng(49.165197, 37.267953));
                points3.Add(new PointLatLng(49.154420, 37.260057));
                points3.Add(new PointLatLng(49.140272, 37.192422));
                points3.Add(new PointLatLng(49.142293, 37.172166));
                points3.Add(new PointLatLng(49.155543, 37.152597));
                points3.Add(new PointLatLng(49.151950, 37.145044));
                points3.Add(new PointLatLng(49.154869, 37.141267));
                points3.Add(new PointLatLng(49.155767, 37.134401));
                points3.Add(new PointLatLng(49.151950, 37.133371));
                points3.Add(new PointLatLng(49.163625, 37.070886));
                points3.Add(new PointLatLng(49.170136, 37.077752));
                points3.Add(new PointLatLng(49.179562, 37.050630));
                points3.Add(new PointLatLng(49.173502, 37.047883));
                points3.Add(new PointLatLng(49.172605, 37.041017));
                points3.Add(new PointLatLng(49.166768, 37.041360));
                points3.Add(new PointLatLng(49.177542, 37.005655));
                points3.Add(new PointLatLng(49.179787, 36.998788));
                points3.Add(new PointLatLng(49.173502, 36.986085));
                points3.Add(new PointLatLng(49.170584, 36.967203));
                points3.Add(new PointLatLng(49.174625, 36.963083));
                points3.Add(new PointLatLng(49.178889, 36.964113));
                points3.Add(new PointLatLng(49.181582, 36.958276));
                points3.Add(new PointLatLng(49.179787, 36.949350));
                points3.Add(new PointLatLng(49.175074, 36.956560));
                points3.Add(new PointLatLng(49.177318, 36.927034));
                points3.Add(new PointLatLng(49.189212, 36.918794));
                points3.Add(new PointLatLng(49.215906, 36.930124));
                points3.Add(new PointLatLng(49.229136, 36.944200));
                points3.Add(new PointLatLng(49.230033, 36.941110));
                points3.Add(new PointLatLng(49.239224, 36.936990));
                points3.Add(new PointLatLng(49.254016, 36.934930));
                points3.Add(new PointLatLng(49.261186, 36.922571));
                points3.Add(new PointLatLng(49.277092, 36.922227));
                points3.Add(new PointLatLng(49.279556, 36.940423));
                points3.Add(new PointLatLng(49.290305, 36.951066));
                points3.Add(new PointLatLng(49.295679, 36.945917));
                points3.Add(new PointLatLng(49.299709, 36.956903));
                points3.Add(new PointLatLng(49.307097, 36.959993));
                points3.Add(new PointLatLng(49.299485, 36.964456));
                points3.Add(new PointLatLng(49.305082, 36.975442));
                points3.Add(new PointLatLng(49.302395, 37.000505));
                points3.Add(new PointLatLng(49.289185, 37.022821));
                points3.Add(new PointLatLng(49.293216, 37.025567));
                points3.Add(new PointLatLng(49.290977, 37.034837));
                points3.Add(new PointLatLng(49.291873, 37.047197));
                points3.Add(new PointLatLng(49.288962, 37.056810));
                points3.Add(new PointLatLng(49.284483, 37.059556));
                points3.Add(new PointLatLng(49.286722, 37.077752));
                points3.Add(new PointLatLng(49.280003, 37.076036));
                points3.Add(new PointLatLng(49.283811, 37.082559));
                points3.Add(new PointLatLng(49.282915, 37.083589));
                points3.Add(new PointLatLng(49.277764, 37.083589));
                points3.Add(new PointLatLng(49.284931, 37.117578));
                points3.Add(new PointLatLng(49.297022, 37.141267));
                points3.Add(new PointLatLng(49.316721, 37.160837));
                points3.Add(new PointLatLng(49.328358, 37.156030));
                points3.Add(new PointLatLng(49.334175, 37.122384));
                points3.Add(new PointLatLng(49.339544, 37.108652));
                points3.Add(new PointLatLng(49.345136, 37.104188));
                points3.Add(new PointLatLng(49.356766, 37.102472));
                points3.Add(new PointLatLng(49.365710, 37.081529));
                points3.Add(new PointLatLng(49.380240, 37.068139));
                points3.Add(new PointLatLng(49.383593, 37.060586));
                points3.Add(new PointLatLng(49.383593, 37.053033));
                points3.Add(new PointLatLng(49.380687, 37.049257));
                points3.Add(new PointLatLng(49.380911, 37.035180));
                points3.Add(new PointLatLng(49.374876, 37.007715));
                points3.Add(new PointLatLng(49.378676, 36.999818));
                points3.Add(new PointLatLng(49.381135, 36.986085));
                points3.Add(new PointLatLng(49.376441, 36.977159));
                points3.Add(new PointLatLng(49.384040, 36.948663));
                points3.Add(new PointLatLng(49.390298, 36.951410));
                points3.Add(new PointLatLng(49.393426, 36.936647));
                points3.Add(new PointLatLng(49.403481, 36.935617));
                points3.Add(new PointLatLng(49.403704, 36.922914));
                points3.Add(new PointLatLng(49.396331, 36.897165));
                points3.Add(new PointLatLng(49.397001, 36.885492));
                points3.Add(new PointLatLng(49.408172, 36.876222));
                points3.Add(new PointLatLng(49.413756, 36.877595));
                points3.Add(new PointLatLng(49.425147, 36.861459));
                points3.Add(new PointLatLng(49.420680, 36.855279));
                points3.Add(new PointLatLng(49.430729, 36.853906));
                points3.Add(new PointLatLng(49.427826, 36.857339));
                points3.Add(new PointLatLng(49.430952, 36.858369));
                points3.Add(new PointLatLng(49.432292, 36.861116));
                points3.Add(new PointLatLng(49.436534, 36.864892));
                points3.Add(new PointLatLng(49.437650, 36.861459));
                points3.Add(new PointLatLng(49.432515, 36.847040));
                points3.Add(new PointLatLng(49.426486, 36.846010));
                points3.Add(new PointLatLng(49.413086, 36.827814));
                points3.Add(new PointLatLng(49.405491, 36.812021));
                points3.Add(new PointLatLng(49.403927, 36.791421));
                points3.Add(new PointLatLng(49.410406, 36.773569));
                points3.Add(new PointLatLng(49.417107, 36.767732));
                points3.Add(new PointLatLng(49.434078, 36.758806));
                points3.Add(new PointLatLng(49.442338, 36.764985));
                points3.Add(new PointLatLng(49.457070, 36.746789));
                points3.Add(new PointLatLng(49.455507, 36.712457));
                points3.Add(new PointLatLng(49.459301, 36.709710));
                points3.Add(new PointLatLng(49.461086, 36.694948));
                points3.Add(new PointLatLng(49.469565, 36.690141));
                points3.Add(new PointLatLng(49.465995, 36.669885));
                points3.Add(new PointLatLng(49.466888, 36.636583));
                points3.Add(new PointLatLng(49.471573, 36.622850));
                points3.Add(new PointLatLng(49.490533, 36.623536));
                points3.Add(new PointLatLng(49.505250, 36.613923));
                points3.Add(new PointLatLng(49.513052, 36.615983));
                points3.Add(new PointLatLng(49.517510, 36.610834));
                points3.Add(new PointLatLng(49.512829, 36.608430));
                points3.Add(new PointLatLng(49.510823, 36.601221));
                points3.Add(new PointLatLng(49.515950, 36.592981));
                points3.Add(new PointLatLng(49.519739, 36.576158));
                points3.Add(new PointLatLng(49.504581, 36.535989));
                points3.Add(new PointLatLng(49.502351, 36.520540));
                points3.Add(new PointLatLng(49.513275, 36.493417));
                points3.Add(new PointLatLng(49.524865, 36.482431));
                points3.Add(new PointLatLng(49.528876, 36.487924));
                points3.Add(new PointLatLng(49.534670, 36.477624));
                points3.Add(new PointLatLng(49.534224, 36.471788));
                points3.Add(new PointLatLng(49.536898, 36.448099));
                points3.Add(new PointLatLng(49.540685, 36.444665));
                points3.Add(new PointLatLng(49.541799, 36.439172));
                points3.Add(new PointLatLng(49.540017, 36.436082));
                points3.Add(new PointLatLng(49.543136, 36.434366));
                points3.Add(new PointLatLng(49.543136, 36.408616));
                points3.Add(new PointLatLng(49.548928, 36.392824));
                points3.Add(new PointLatLng(49.551823, 36.393510));
                points3.Add(new PointLatLng(49.552714, 36.389047));
                points3.Add(new PointLatLng(49.550710, 36.383211));
                points3.Add(new PointLatLng(49.554942, 36.374971));
                points3.Add(new PointLatLng(49.562736, 36.372568));
                points3.Add(new PointLatLng(49.570975, 36.358835));
                points3.Add(new PointLatLng(49.585890, 36.353341));
                points3.Add(new PointLatLng(49.584554, 36.356775));
                points3.Add(new PointLatLng(49.588116, 36.356431));
                points3.Add(new PointLatLng(49.590341, 36.348192));
                points3.Add(new PointLatLng(49.597240, 36.344415));
                points3.Add(new PointLatLng(49.599910, 36.336862));
                points3.Add(new PointLatLng(49.621489, 36.327249));
                points3.Add(new PointLatLng(49.628829, 36.339265));
                points3.Add(new PointLatLng(49.642837, 36.336175));
                points3.Add(new PointLatLng(49.662841, 36.360208));
                points3.Add(new PointLatLng(49.672396, 36.366388));
                points3.Add(new PointLatLng(49.673285, 36.371881));
                points3.Add(new PointLatLng(49.677950, 36.378061));
                points3.Add(new PointLatLng(49.675507, 36.391107));
                points3.Add(new PointLatLng(49.681283, 36.403467));
                points3.Add(new PointLatLng(49.678839, 36.414453));
                points3.Add(new PointLatLng(49.681949, 36.417543));
                points3.Add(new PointLatLng(49.682393, 36.422693));
                points3.Add(new PointLatLng(49.678839, 36.420289));
                points3.Add(new PointLatLng(49.676173, 36.423036));
                points3.Add(new PointLatLng(49.673951, 36.419946));
                points3.Add(new PointLatLng(49.677062, 36.415140));
                points3.Add(new PointLatLng(49.675507, 36.414110));
                points3.Add(new PointLatLng(49.672396, 36.417543));
                points3.Add(new PointLatLng(49.672174, 36.424753));
                points3.Add(new PointLatLng(49.670174, 36.430246));
                points3.Add(new PointLatLng(49.671952, 36.432992));
                points3.Add(new PointLatLng(49.674840, 36.429559));
                points3.Add(new PointLatLng(49.677950, 36.442262));
                points3.Add(new PointLatLng(49.698163, 36.465608));
                points3.Add(new PointLatLng(49.701715, 36.488954));
                points3.Add(new PointLatLng(49.699717, 36.498910));
                points3.Add(new PointLatLng(49.709931, 36.528093));
                points3.Add(new PointLatLng(49.733458, 36.532556));
                points3.Add(new PointLatLng(49.737009, 36.540452));
                points3.Add(new PointLatLng(49.759858, 36.546289));
                points3.Add(new PointLatLng(49.765402, 36.566888));
                points3.Add(new PointLatLng(49.768285, 36.568948));
                points3.Add(new PointLatLng(49.774050, 36.565172));
                points3.Add(new PointLatLng(49.786243, 36.581651));
                points3.Add(new PointLatLng(49.786021, 36.589891));
                points3.Add(new PointLatLng(49.798654, 36.612207));
                points3.Add(new PointLatLng(49.792449, 36.621477));
                points3.Add(new PointLatLng(49.806632, 36.673318));
                points3.Add(new PointLatLng(49.821696, 36.693231));
                points3.Add(new PointLatLng(49.825240, 36.717607));
                points3.Add(new PointLatLng(49.829226, 36.721040));
                points3.Add(new PointLatLng(49.832327, 36.718637));
                points3.Add(new PointLatLng(49.838527, 36.702157));
                points3.Add(new PointLatLng(49.863542, 36.720697));
                points3.Add(new PointLatLng(49.874606, 36.746446));
                points3.Add(new PointLatLng(49.866197, 36.782495));
                points3.Add(new PointLatLng(49.844062, 36.777345));
                points3.Add(new PointLatLng(49.815050, 36.903001));
                points3.Add(new PointLatLng(49.832770, 36.920167));
                points3.Add(new PointLatLng(49.858894, 36.932184));
                points3.Add(new PointLatLng(49.869074, 36.950380));
                points3.Add(new PointLatLng(49.875048, 36.974412));
                points3.Add(new PointLatLng(49.886110, 36.980592));
                points3.Add(new PointLatLng(49.910879, 36.981965));
                points3.Add(new PointLatLng(49.944915, 36.913988));
                points3.Add(new PointLatLng(49.989965, 36.883088));
                points3.Add(new PointLatLng(50.017327, 36.842576));
                points3.Add(new PointLatLng(50.038501, 36.808244));
                points3.Add(new PointLatLng(50.086547, 36.806184));
                points3.Add(new PointLatLng(50.148188, 36.819917));
                points3.Add(new PointLatLng(50.161826, 36.850816));
                points3.Add(new PointLatLng(50.186892, 36.832963));
                points3.Add(new PointLatLng(50.197003, 36.832277));
                points3.Add(new PointLatLng(50.200080, 36.845323));
                points3.Add(new PointLatLng(50.197882, 36.852876));
                points3.Add(new PointLatLng(50.206233, 36.856309));
                points3.Add(new PointLatLng(50.228201, 36.836397));
                points3.Add(new PointLatLng(50.234790, 36.839830));
                points3.Add(new PointLatLng(50.243134, 36.835023));
                points3.Add(new PointLatLng(50.244452, 36.830217));
                points3.Add(new PointLatLng(50.256306, 36.830903));
                points3.Add(new PointLatLng(50.271669, 36.862489));
                points3.Add(new PointLatLng(50.290098, 36.848756));
                points3.Add(new PointLatLng(50.297117, 36.859056));
                points3.Add(new PointLatLng(50.304573, 36.852876));
                points3.Add(new PointLatLng(50.306766, 36.839143));
                points3.Add(new PointLatLng(50.312905, 36.832963));

                List<GMap.NET.PointLatLng> points4 = new List<GMap.NET.PointLatLng>();
                points4.Add(new PointLatLng(45.466975, 28.210905));
                points4.Add(new PointLatLng(45.456380, 28.268584));
                points4.Add(new PointLatLng(45.434220, 28.286436));
                points4.Add(new PointLatLng(45.396624, 28.280943));
                points4.Add(new PointLatLng(45.373476, 28.305662));
                points4.Add(new PointLatLng(45.332943, 28.333128));
                points4.Add(new PointLatLng(45.319426, 28.349608));
                points4.Add(new PointLatLng(45.251791, 28.559721));
                points4.Add(new PointLatLng(45.245990, 28.636626));
                points4.Add(new PointLatLng(45.227616, 28.699797));
                points4.Add(new PointLatLng(45.224715, 28.720396));
                points4.Add(new PointLatLng(45.233419, 28.739622));
                points4.Add(new PointLatLng(45.239222, 28.783568));
                points4.Add(new PointLatLng(45.247924, 28.790434));
                points4.Add(new PointLatLng(45.254692, 28.787688));
                points4.Add(new PointLatLng(45.261458, 28.767088));
                points4.Add(new PointLatLng(45.281754, 28.751982));
                points4.Add(new PointLatLng(45.289483, 28.757475));
                points4.Add(new PointLatLng(45.291416, 28.787688));
                points4.Add(new PointLatLng(45.309769, 28.805540));
                points4.Add(new PointLatLng(45.322323, 28.780821));
                points4.Add(new PointLatLng(45.328116, 28.778075));
                points4.Add(new PointLatLng(45.337770, 28.816527));
                points4.Add(new PointLatLng(45.318460, 28.843992));
                points4.Add(new PointLatLng(45.317495, 28.874205));
                points4.Add(new PointLatLng(45.287551, 28.914030));
                points4.Add(new PointLatLng(45.281754, 28.938750));
                points4.Add(new PointLatLng(45.285619, 28.951109));
                points4.Add(new PointLatLng(45.296246, 28.953856));
                points4.Add(new PointLatLng(45.309769, 28.951109));
                points4.Add(new PointLatLng(45.335839, 28.963469));
                points4.Add(new PointLatLng(45.332943, 28.982695));
                points4.Add(new PointLatLng(45.374440, 29.081572));
                points4.Add(new PointLatLng(45.397588, 29.170836));
                points4.Add(new PointLatLng(45.414943, 29.187315));
                points4.Add(new PointLatLng(45.418799, 29.216154));
                points4.Add(new PointLatLng(45.435184, 29.243620));
                points4.Add(new PointLatLng(45.427474, 29.280699));
                points4.Add(new PointLatLng(45.433256, 29.305418));
                points4.Add(new PointLatLng(45.444819, 29.312285));
                points4.Add(new PointLatLng(45.448673, 29.334257));
                points4.Add(new PointLatLng(45.439038, 29.349364));
                points4.Add(new PointLatLng(45.444819, 29.427641));
                points4.Add(new PointLatLng(45.435184, 29.440001));
                points4.Add(new PointLatLng(45.410123, 29.548491));
                points4.Add(new PointLatLng(45.390838, 29.591063));
                points4.Add(new PointLatLng(45.341631, 29.650114));
                points4.Add(new PointLatLng(45.309769, 29.667967));
                points4.Add(new PointLatLng(45.265325, 29.678953));
                points4.Add(new PointLatLng(45.225682, 29.732512));
                points4.Add(new PointLatLng(45.265325, 29.678953));
                points4.Add(new PointLatLng(45.309769, 29.667967));
                points4.Add(new PointLatLng(45.341631, 29.650114));
                points4.Add(new PointLatLng(45.390838, 29.591063));
                points4.Add(new PointLatLng(45.410123, 29.548491));
                points4.Add(new PointLatLng(45.435184, 29.440001));
                points4.Add(new PointLatLng(45.444819, 29.427641));
                points4.Add(new PointLatLng(45.439038, 29.349364));
                points4.Add(new PointLatLng(45.448673, 29.334257));
                points4.Add(new PointLatLng(45.444819, 29.312285));
                points4.Add(new PointLatLng(45.433256, 29.305418));
                points4.Add(new PointLatLng(45.427474, 29.280699));
                points4.Add(new PointLatLng(45.435184, 29.243620));
                points4.Add(new PointLatLng(45.418799, 29.216154));
                points4.Add(new PointLatLng(45.414943, 29.187315));
                points4.Add(new PointLatLng(45.397588, 29.170836));
                points4.Add(new PointLatLng(45.374440, 29.081572));
                points4.Add(new PointLatLng(45.332943, 28.982695));
                points4.Add(new PointLatLng(45.335839, 28.963469));
                points4.Add(new PointLatLng(45.309769, 28.951109));
                points4.Add(new PointLatLng(45.296246, 28.953856));
                points4.Add(new PointLatLng(45.285619, 28.951109));
                points4.Add(new PointLatLng(45.281754, 28.938750));
                points4.Add(new PointLatLng(45.287551, 28.914030));
                points4.Add(new PointLatLng(45.317495, 28.874205));
                points4.Add(new PointLatLng(45.318460, 28.843992));
                points4.Add(new PointLatLng(45.337770, 28.816527));
                points4.Add(new PointLatLng(45.328116, 28.778075));
                points4.Add(new PointLatLng(45.322323, 28.780821));
                points4.Add(new PointLatLng(45.309769, 28.805540));
                points4.Add(new PointLatLng(45.291416, 28.787688));
                points4.Add(new PointLatLng(45.289483, 28.757475));
                points4.Add(new PointLatLng(45.281754, 28.751982));
                points4.Add(new PointLatLng(45.261458, 28.767088));
                points4.Add(new PointLatLng(45.254692, 28.787688));
                points4.Add(new PointLatLng(45.247924, 28.790434));
                points4.Add(new PointLatLng(45.239222, 28.783568));
                points4.Add(new PointLatLng(45.233419, 28.739622));
                points4.Add(new PointLatLng(45.224715, 28.720396));
                points4.Add(new PointLatLng(45.227616, 28.699797));
                points4.Add(new PointLatLng(45.245990, 28.636626));
                points4.Add(new PointLatLng(45.251791, 28.559721));
                points4.Add(new PointLatLng(45.319426, 28.349608));
                points4.Add(new PointLatLng(45.332943, 28.333128));
                points4.Add(new PointLatLng(45.373476, 28.305662));
                points4.Add(new PointLatLng(45.396624, 28.280943));
                points4.Add(new PointLatLng(45.434220, 28.286436));
                points4.Add(new PointLatLng(45.456380, 28.268584));
                points4.Add(new PointLatLng(45.466975, 28.210905));

                List<GMap.NET.PointLatLng> points5 = new List<GMap.NET.PointLatLng>();
                points5.Add(new PointLatLng(46.546860, 29.876102));
                points5.Add(new PointLatLng(46.542727, 29.877990));
                points5.Add(new PointLatLng(46.542137, 29.880565));
                points5.Add(new PointLatLng(46.547214, 29.884856));
                points5.Add(new PointLatLng(46.547686, 29.887088));
                points5.Add(new PointLatLng(46.545561, 29.890178));
                points5.Add(new PointLatLng(46.539303, 29.893783));
                points5.Add(new PointLatLng(46.532454, 29.902366));
                points5.Add(new PointLatLng(46.531037, 29.896186));
                points5.Add(new PointLatLng(46.527967, 29.895328));
                points5.Add(new PointLatLng(46.526431, 29.897044));
                points5.Add(new PointLatLng(46.522061, 29.913009));
                points5.Add(new PointLatLng(46.519344, 29.911464));
                points5.Add(new PointLatLng(46.517572, 29.905627));
                points5.Add(new PointLatLng(46.514737, 29.905627));
                points5.Add(new PointLatLng(46.512965, 29.909232));
                points5.Add(new PointLatLng(46.513320, 29.927257));
                points5.Add(new PointLatLng(46.510957, 29.927772));
                points5.Add(new PointLatLng(46.505995, 29.923308));
                points5.Add(new PointLatLng(46.502805, 29.924853));
                points5.Add(new PointLatLng(46.501032, 29.933436));
                points5.Add(new PointLatLng(46.502568, 29.940990));
                points5.Add(new PointLatLng(46.501505, 29.943736));
                points5.Add(new PointLatLng(46.496069, 29.935153));
                points5.Add(new PointLatLng(46.494060, 29.937041));
                points5.Add(new PointLatLng(46.495715, 29.947341));
                points5.Add(new PointLatLng(46.501387, 29.955752));
                points5.Add(new PointLatLng(46.509303, 29.957641));
                points5.Add(new PointLatLng(46.509539, 29.962962));
                points5.Add(new PointLatLng(46.505404, 29.964679));
                points5.Add(new PointLatLng(46.490870, 29.952148));
                points5.Add(new PointLatLng(46.490279, 29.958842));
                points5.Add(new PointLatLng(46.493706, 29.966224));
                points5.Add(new PointLatLng(46.501978, 29.971030));
                points5.Add(new PointLatLng(46.498787, 29.982188));
                points5.Add(new PointLatLng(46.510248, 29.983390));
                points5.Add(new PointLatLng(46.509894, 29.986995));
                points5.Add(new PointLatLng(46.500323, 29.992488));
                points5.Add(new PointLatLng(46.495951, 29.987166));
                points5.Add(new PointLatLng(46.493470, 29.988368));
                points5.Add(new PointLatLng(46.497015, 29.998496));
                points5.Add(new PointLatLng(46.496188, 30.000728));
                points5.Add(new PointLatLng(46.494297, 30.000384));
                points5.Add(new PointLatLng(46.489451, 29.991973));
                points5.Add(new PointLatLng(46.482478, 29.989398));
                points5.Add(new PointLatLng(46.476331, 29.993861));
                points5.Add(new PointLatLng(46.472903, 29.995750));
                points5.Add(new PointLatLng(46.470893, 30.000384));
                points5.Add(new PointLatLng(46.471720, 30.020125));
                points5.Add(new PointLatLng(46.468528, 30.022872));
                points5.Add(new PointLatLng(46.465572, 30.020812));
                points5.Add(new PointLatLng(46.464745, 30.015662));
                points5.Add(new PointLatLng(46.464626, 29.995921));
                points5.Add(new PointLatLng(46.462616, 29.993690));
                points5.Add(new PointLatLng(46.453747, 30.007423));
                points5.Add(new PointLatLng(46.455048, 30.014461));
                points5.Add(new PointLatLng(46.454575, 30.023387));
                points5.Add(new PointLatLng(46.452328, 30.026649));
                points5.Add(new PointLatLng(46.444403, 30.022014));
                points5.Add(new PointLatLng(46.441801, 30.023730));
                points5.Add(new PointLatLng(46.441919, 30.034030));
                points5.Add(new PointLatLng(46.443694, 30.049651));
                points5.Add(new PointLatLng(46.442984, 30.064242));
                points5.Add(new PointLatLng(46.440618, 30.064242));
                points5.Add(new PointLatLng(46.435650, 30.054973));
                points5.Add(new PointLatLng(46.427486, 30.057033));
                points5.Add(new PointLatLng(46.426421, 30.069907));
                points5.Add(new PointLatLng(46.423936, 30.078147));
                points5.Add(new PointLatLng(46.425120, 30.079864));
                points5.Add(new PointLatLng(46.431391, 30.078319));
                points5.Add(new PointLatLng(46.434940, 30.081237));
                points5.Add(new PointLatLng(46.433757, 30.087588));
                points5.Add(new PointLatLng(46.426895, 30.091365));
                points5.Add(new PointLatLng(46.425830, 30.094970));
                points5.Add(new PointLatLng(46.427131, 30.098060));
                points5.Add(new PointLatLng(46.431627, 30.097716));
                points5.Add(new PointLatLng(46.438252, 30.089992));
                points5.Add(new PointLatLng(46.440737, 30.091880));
                points5.Add(new PointLatLng(46.438726, 30.098746));
                points5.Add(new PointLatLng(46.428078, 30.104926));
                points5.Add(new PointLatLng(46.427841, 30.107158));
                points5.Add(new PointLatLng(46.431509, 30.111106));
                points5.Add(new PointLatLng(46.427131, 30.122951));
                points5.Add(new PointLatLng(46.419085, 30.132564));
                points5.Add(new PointLatLng(46.417310, 30.135482));
                points5.Add(new PointLatLng(46.415771, 30.134452));
                points5.Add(new PointLatLng(46.414588, 30.130332));
                points5.Add(new PointLatLng(46.412457, 30.130332));
                points5.Add(new PointLatLng(46.411274, 30.134109));
                points5.Add(new PointLatLng(46.412457, 30.140803));
                points5.Add(new PointLatLng(46.416244, 30.147670));
                points5.Add(new PointLatLng(46.427723, 30.149558));
                points5.Add(new PointLatLng(46.429379, 30.154021));
                points5.Add(new PointLatLng(46.426895, 30.160544));
                points5.Add(new PointLatLng(46.415889, 30.161059));
                points5.Add(new PointLatLng(46.413759, 30.163634));
                points5.Add(new PointLatLng(46.414233, 30.169299));
                points5.Add(new PointLatLng(46.420860, 30.173934));
                points5.Add(new PointLatLng(46.434112, 30.167926));
                points5.Add(new PointLatLng(46.435768, 30.169643));
                points5.Add(new PointLatLng(46.436005, 30.173762));
                points5.Add(new PointLatLng(46.433639, 30.184577));
                points5.Add(new PointLatLng(46.435650, 30.189212));
                points5.Add(new PointLatLng(46.445468, 30.192473));
                points5.Add(new PointLatLng(46.444285, 30.216678));
                points5.Add(new PointLatLng(46.427604, 30.247920));
                points5.Add(new PointLatLng(46.412102, 30.261996));
                points5.Add(new PointLatLng(46.402870, 30.255473));
                points5.Add(new PointLatLng(46.389018, 30.259765));
                points5.Add(new PointLatLng(46.374571, 30.271609));
                points5.Add(new PointLatLng(46.371373, 30.287574));
                points5.Add(new PointLatLng(46.356210, 30.293067));
                points5.Add(new PointLatLng(46.347324, 30.299762));
                points5.Add(new PointLatLng(46.341043, 30.284312));
                points5.Add(new PointLatLng(46.326346, 30.292724));
                points5.Add(new PointLatLng(46.303581, 30.272983));
                points5.Add(new PointLatLng(46.326346, 30.292724));
                points5.Add(new PointLatLng(46.341043, 30.284312));
                points5.Add(new PointLatLng(46.347324, 30.299762));
                points5.Add(new PointLatLng(46.356210, 30.293067));
                points5.Add(new PointLatLng(46.371373, 30.287574));
                points5.Add(new PointLatLng(46.374571, 30.271609));
                points5.Add(new PointLatLng(46.389018, 30.259765));
                points5.Add(new PointLatLng(46.402870, 30.255473));
                points5.Add(new PointLatLng(46.412102, 30.261996));
                points5.Add(new PointLatLng(46.427604, 30.247920));
                points5.Add(new PointLatLng(46.444285, 30.216678));
                points5.Add(new PointLatLng(46.445468, 30.192473));
                points5.Add(new PointLatLng(46.435650, 30.189212));
                points5.Add(new PointLatLng(46.433639, 30.184577));
                points5.Add(new PointLatLng(46.436005, 30.173762));
                points5.Add(new PointLatLng(46.435768, 30.169643));
                points5.Add(new PointLatLng(46.434112, 30.167926));
                points5.Add(new PointLatLng(46.420860, 30.173934));
                points5.Add(new PointLatLng(46.414233, 30.169299));
                points5.Add(new PointLatLng(46.413759, 30.163634));
                points5.Add(new PointLatLng(46.415889, 30.161059));
                points5.Add(new PointLatLng(46.426895, 30.160544));
                points5.Add(new PointLatLng(46.429379, 30.154021));
                points5.Add(new PointLatLng(46.427723, 30.149558));
                points5.Add(new PointLatLng(46.416244, 30.147670));
                points5.Add(new PointLatLng(46.412457, 30.140803));
                points5.Add(new PointLatLng(46.411274, 30.134109));
                points5.Add(new PointLatLng(46.412457, 30.130332));
                points5.Add(new PointLatLng(46.414588, 30.130332));
                points5.Add(new PointLatLng(46.415771, 30.134452));
                points5.Add(new PointLatLng(46.417310, 30.135482));
                points5.Add(new PointLatLng(46.419085, 30.132564));
                points5.Add(new PointLatLng(46.427131, 30.122951));
                points5.Add(new PointLatLng(46.431509, 30.111106));
                points5.Add(new PointLatLng(46.427841, 30.107158));
                points5.Add(new PointLatLng(46.428078, 30.104926));
                points5.Add(new PointLatLng(46.438726, 30.098746));
                points5.Add(new PointLatLng(46.440737, 30.091880));
                points5.Add(new PointLatLng(46.438252, 30.089992));
                points5.Add(new PointLatLng(46.431627, 30.097716));
                points5.Add(new PointLatLng(46.427131, 30.098060));
                points5.Add(new PointLatLng(46.425830, 30.094970));
                points5.Add(new PointLatLng(46.426895, 30.091365));
                points5.Add(new PointLatLng(46.433757, 30.087588));
                points5.Add(new PointLatLng(46.434940, 30.081237));
                points5.Add(new PointLatLng(46.431391, 30.078319));
                points5.Add(new PointLatLng(46.425120, 30.079864));
                points5.Add(new PointLatLng(46.423936, 30.078147));
                points5.Add(new PointLatLng(46.426421, 30.069907));
                points5.Add(new PointLatLng(46.427486, 30.057033));
                points5.Add(new PointLatLng(46.435650, 30.054973));
                points5.Add(new PointLatLng(46.440618, 30.064242));
                points5.Add(new PointLatLng(46.442984, 30.064242));
                points5.Add(new PointLatLng(46.443694, 30.049651));
                points5.Add(new PointLatLng(46.441919, 30.034030));
                points5.Add(new PointLatLng(46.441801, 30.023730));
                points5.Add(new PointLatLng(46.444403, 30.022014));
                points5.Add(new PointLatLng(46.452328, 30.026649));
                points5.Add(new PointLatLng(46.454575, 30.023387));
                points5.Add(new PointLatLng(46.455048, 30.014461));
                points5.Add(new PointLatLng(46.453747, 30.007423));
                points5.Add(new PointLatLng(46.462616, 29.993690));
                points5.Add(new PointLatLng(46.464626, 29.995921));
                points5.Add(new PointLatLng(46.464745, 30.015662));
                points5.Add(new PointLatLng(46.465572, 30.020812));
                points5.Add(new PointLatLng(46.468528, 30.022872));
                points5.Add(new PointLatLng(46.471720, 30.020125));
                points5.Add(new PointLatLng(46.470893, 30.000384));
                points5.Add(new PointLatLng(46.472903, 29.995750));
                points5.Add(new PointLatLng(46.476331, 29.993861));
                points5.Add(new PointLatLng(46.482478, 29.989398));
                points5.Add(new PointLatLng(46.489451, 29.991973));
                points5.Add(new PointLatLng(46.494297, 30.000384));
                points5.Add(new PointLatLng(46.496188, 30.000728));
                points5.Add(new PointLatLng(46.497015, 29.998496));
                points5.Add(new PointLatLng(46.493470, 29.988368));
                points5.Add(new PointLatLng(46.495951, 29.987166));
                points5.Add(new PointLatLng(46.500323, 29.992488));
                points5.Add(new PointLatLng(46.509894, 29.986995));
                points5.Add(new PointLatLng(46.510248, 29.983390));
                points5.Add(new PointLatLng(46.498787, 29.982188));
                points5.Add(new PointLatLng(46.501978, 29.971030));
                points5.Add(new PointLatLng(46.493706, 29.966224));
                points5.Add(new PointLatLng(46.490279, 29.958842));
                points5.Add(new PointLatLng(46.490870, 29.952148));
                points5.Add(new PointLatLng(46.505404, 29.964679));
                points5.Add(new PointLatLng(46.509539, 29.962962));
                points5.Add(new PointLatLng(46.509303, 29.957641));
                points5.Add(new PointLatLng(46.501387, 29.955752));
                points5.Add(new PointLatLng(46.495715, 29.947341));
                points5.Add(new PointLatLng(46.494060, 29.937041));
                points5.Add(new PointLatLng(46.496069, 29.935153));
                points5.Add(new PointLatLng(46.501505, 29.943736));
                points5.Add(new PointLatLng(46.502568, 29.940990));
                points5.Add(new PointLatLng(46.501032, 29.933436));
                points5.Add(new PointLatLng(46.502805, 29.924853));
                points5.Add(new PointLatLng(46.505995, 29.923308));
                points5.Add(new PointLatLng(46.510957, 29.927772));
                points5.Add(new PointLatLng(46.513320, 29.927257));
                points5.Add(new PointLatLng(46.512965, 29.909232));
                points5.Add(new PointLatLng(46.514737, 29.905627));
                points5.Add(new PointLatLng(46.517572, 29.905627));
                points5.Add(new PointLatLng(46.519344, 29.911464));
                points5.Add(new PointLatLng(46.522061, 29.913009));
                points5.Add(new PointLatLng(46.526431, 29.897044));
                points5.Add(new PointLatLng(46.527967, 29.895328));
                points5.Add(new PointLatLng(46.531037, 29.896186));
                points5.Add(new PointLatLng(46.532454, 29.902366));
                points5.Add(new PointLatLng(46.539303, 29.893783));
                points5.Add(new PointLatLng(46.545561, 29.890178));
                points5.Add(new PointLatLng(46.547686, 29.887088));
                points5.Add(new PointLatLng(46.547214, 29.884856));
                points5.Add(new PointLatLng(46.542137, 29.880565));
                points5.Add(new PointLatLng(46.542727, 29.877990));
                points5.Add(new PointLatLng(46.546860, 29.876102));

                GMapPolygon polygon = new GMapPolygon(points, "mypolygon");
                GMapPolygon polygon1 = new GMapPolygon(points1, "mypolygon1");
                GMapPolygon polygon2 = new GMapPolygon(points2, "mypolygon2");
                polygon.Fill = new SolidBrush(Color.FromArgb(60, Color.Blue));
                polygon1.Fill = new SolidBrush(Color.FromArgb(60, Color.Blue));
                polygon2.Fill = new SolidBrush(Color.FromArgb(60, Color.Blue));
                polygon.Stroke = new Pen(Color.Blue, 2);
                polygon1.Stroke = new Pen(Color.Blue, 2);
                polygon2.Stroke = new Pen(Color.Blue, 2);
                polyOverlay.Polygons.Add(polygon);
                polyOverlay.Polygons.Add(polygon1);
                polyOverlay.Polygons.Add(polygon2);
                GMapPolygon polygon3 = new GMapPolygon(points3, "mypolygon3");
                GMapPolygon polygon4 = new GMapPolygon(points4, "mypolygon4");
                GMapPolygon polygon5 = new GMapPolygon(points5, "mypolygon5");
                polygon3.Fill = new SolidBrush(Color.FromArgb(60, Color.Blue));
                polygon4.Fill = new SolidBrush(Color.FromArgb(60, Color.Blue));
                polygon5.Fill = new SolidBrush(Color.FromArgb(60, Color.Blue));
                polygon3.Stroke = new Pen(Color.Blue, 2);
                polygon4.Stroke = new Pen(Color.Blue, 2);
                polygon5.Stroke = new Pen(Color.Blue, 2);
                polyOverlay.Polygons.Add(polygon3);
                polyOverlay.Polygons.Add(polygon4);
                polyOverlay.Polygons.Add(polygon5);
                maps.map.Overlays.Add(polyOverlay);

                GMapOverlay markersOverlay = new GMapOverlay("markers");
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(45.423548, 28.630913),
                  GMarkerGoogleType.blue);
                markersOverlay.Markers.Add(marker);
                GMarkerGoogle marker1 = new GMarkerGoogle(new PointLatLng(45.672226, 29.656039),
                  GMarkerGoogleType.blue);
                markersOverlay.Markers.Add(marker1);
                GMarkerGoogle marker2 = new GMarkerGoogle(new PointLatLng(51.499430, 23.845473),
                  GMarkerGoogleType.blue);
                markersOverlay.Markers.Add(marker2);
                GMarkerGoogle marker3 = new GMarkerGoogle(new PointLatLng(48.616762, 23.683861),
                  GMarkerGoogleType.blue);
                markersOverlay.Markers.Add(marker3);
                maps.map.Overlays.Add(markersOverlay);
            }
            maps.ShowDialog();
        }

    }
}
