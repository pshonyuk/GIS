﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GIS
{
    public partial class Form16 : Form
    {
        public Form16()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form17 karpaty = new Form17();
            karpaty.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form18 shatsk = new Form18();
            shatsk.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form19 dnistro = new Form19();
            dnistro.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form20 sant = new Form20();
            sant.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form21 sinevir = new Form21();
            sinevir.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Form22 golos = new Form22();
            golos.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Form23 gutsul = new Form23();
            gutsul.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Form24 beskidi = new Form24();
            beskidi.ShowDialog();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Form25 getman = new Form25();
            getman.ShowDialog();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Form26 derman = new Form26();
            derman.ShowDialog();
        }
    }
}
