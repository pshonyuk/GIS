﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GIS
{
    public partial class main : Form
    {
        public main()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            about about = new about();
            about.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tourist turist = new tourist();
            turist.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            aspect aspect = new aspect();
            aspect.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            doslid doslid = new doslid();
            doslid.ShowDialog();
        }
    }
}
