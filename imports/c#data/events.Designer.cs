﻿namespace GIS
{
    partial class events
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(events));
            this.text = new System.Windows.Forms.Label();
            this.up_but = new System.Windows.Forms.PictureBox();
            this.down_but = new System.Windows.Forms.PictureBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.up_but)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.down_but)).BeginInit();
            this.SuspendLayout();
            // 
            // text
            // 
            this.text.BackColor = System.Drawing.Color.Transparent;
            this.text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.text.ForeColor = System.Drawing.SystemColors.InfoText;
            this.text.Location = new System.Drawing.Point(12, 62);
            this.text.Name = "text";
            this.text.Size = new System.Drawing.Size(776, 493);
            this.text.TabIndex = 1;
            this.text.Text = "none";
            this.text.Click += new System.EventHandler(this.text_Click);
            // 
            // up_but
            // 
            this.up_but.BackColor = System.Drawing.Color.Transparent;
            this.up_but.Cursor = System.Windows.Forms.Cursors.Hand;
            this.up_but.Image = global::GIS.Properties.Resources.up_blue;
            this.up_but.Location = new System.Drawing.Point(710, 562);
            this.up_but.Name = "up_but";
            this.up_but.Size = new System.Drawing.Size(36, 26);
            this.up_but.TabIndex = 2;
            this.up_but.TabStop = false;
            this.up_but.Click += new System.EventHandler(this.up_but_Click);
            // 
            // down_but
            // 
            this.down_but.BackColor = System.Drawing.Color.Transparent;
            this.down_but.Cursor = System.Windows.Forms.Cursors.Hand;
            this.down_but.Image = global::GIS.Properties.Resources.down_blue;
            this.down_but.Location = new System.Drawing.Point(752, 562);
            this.down_but.Name = "down_but";
            this.down_but.Size = new System.Drawing.Size(36, 26);
            this.down_but.TabIndex = 3;
            this.down_but.TabStop = false;
            this.down_but.Click += new System.EventHandler(this.down_but_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(603, 20);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(149, 18);
            this.progressBar1.TabIndex = 4;
            this.progressBar1.Visible = false;
            // 
            // events
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::GIS.Properties.Resources.events1;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.down_but);
            this.Controls.Add(this.up_but);
            this.Controls.Add(this.text);
            this.ForeColor = System.Drawing.SystemColors.Desktop;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "events";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Подієві ресурси";
            ((System.ComponentModel.ISupportInitialize)(this.up_but)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.down_but)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label text;
        private System.Windows.Forms.PictureBox up_but;
        private System.Windows.Forms.PictureBox down_but;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}