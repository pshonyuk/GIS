﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;

namespace GIS
{
    public partial class maps : Form
    {
        public maps()
        {
            InitializeComponent();
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            gMapControl1.MapProvider = GMap.NET.MapProviders.GoogleMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerOnly;
            
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            gMapControl1.Zoom = trackBar1.Value;
        }

        private void gMapControl1_OnMapZoomChanged()
        {
            trackBar1.Value = (int)gMapControl1.Zoom;
        }

        public GMap.NET.PointLatLng center
        {
            get { return gMapControl1.Position; }
            set { gMapControl1.Position = value; }
        }

        public double zoom
        {
            get { return gMapControl1.Zoom; }
            set { gMapControl1.Zoom = value; }
        }

        public int pos
        {
            get { return trackBar1.Value; }
            set { trackBar1.Value = value; }
        }

        public GMap.NET.WindowsForms.GMapControl map
        {
            get { return gMapControl1; }
            set { gMapControl1 = value; }
        }
    }
}
