﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;

namespace GIS
{
    public partial class Form40 : Form
    {
        public Form40()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            slideshow picture = new slideshow();
            picture.Text = "\"Інгулецький лиман\"";
            picture.img = pictureBox1.Image;
            picture.ShowDialog();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            slideshow picture = new slideshow();
            picture.Text = "\"Інгулецький лиман\"";
            picture.img = pictureBox2.Image;
            picture.ShowDialog();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            maps maps = new maps();
            maps.Text = "\"Інгулецький лиман\"";
            maps.center = new GMap.NET.PointLatLng(46.697502, 32.829981);
            maps.zoom = 11; maps.pos = 11;

            GMapOverlay markersOverlay = new GMapOverlay("markers");
            GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.697502, 32.829981),
              GMarkerGoogleType.green);
            markersOverlay.Markers.Add(marker);
            maps.map.Overlays.Add(markersOverlay);

            maps.ShowDialog();
        }

    }
}
