﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GIS
{
    public partial class n_antropo : Form
    {
        public n_antropo()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            n_climate prclim = new n_climate();
            prclim.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            parks park = new parks();
            park.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            reserves biosphere = new reserves();
            biosphere.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            memo zakaz = new memo();
            zakaz.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            dendro dpark = new dendro();
            dpark.ShowDialog();
        }
    }
}
