<h1>
Бальнеологічні ресурси
</h1>
<div class='body'>
<p>	природні лікувальні речовини, що використовуються для немедикаментозного лікування на курортах і в позакурортних умовах. Ці ресурси беруть участь в 
основному процесі суспільного виробництва — неперервному відтворенні фізичних сил і розумових здібностей людини, психічного тонусу, відновленні та підвищенні 
кваліфікації працівників, зростанні їхнього загальноосвітнього та фахового рівнів.
<p> 
До бальнеологічних ресурсів належать лікувальні мінеральні води та пелоїди (грязі). Основними природними лікувальними ресурсами є ті, що безпосередньо використовуються 
у бальнеолікуванні, визначають його санаторно-курортну спеціалізацію і профілізацію: питні та купальні води, лікувальні грязі та озокерит. До них належать також 
лікувальний клімат, різноманітні природні водойми та мальовничі ландшафти, які сприяють оздоровленню та гартуванню тих, хто одужує після хвороби.
<p> 
В Україні наявні мінеральні води всіх основних бальнеологічних груп. Деякі з них, зокрема Миргородська, Куяльник, Поляна квасова, Берегівські мінеральні води, 
Нафтуся і радонові води, мають світове значення та є унікальними на нашій планеті. Попереду інтенсивне освоєння сульфідних, 
<p>	залізистих, миш'яковистих та інших мінеральних лікувальних вод. Україна належить до найбагатших на бальнеологічні ресурси країн світу.</div>
