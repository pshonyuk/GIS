<h1>
Музей бурштину
</h1>
<div class='body'>
<p>	Музей бурштину, як виставкова зала Рівненського обласного краєзнавчого музею, почав функціонувати в Рівному 24 серпня 2010 р. за підтримки 
Рівненської обласної державної адміністрації.
<p> 
Це наймолодша експозиція краєзнавчого музею. Знаходиться вона на вулиці Симона Петлюри, 17, у Рівненському будинку вчених, що є пам’яткою архітектури початку ХХ ст. 
місцевого значення.
<p> 
Музей Бурштину містить дві експозиційні зали, де представлені фрагменти бурштину різноманітних форм та відтінків, знайдені на Рівненщині в різні часи. Художні 
вироби вражають своїм розмаїттям.
<p> 
Родзинка музею – унікальний фрагмент бурштину вагою 988 грамів, знайдений у Сарненському районі на Клесівському родовищі. Окраса експозиції – янголи, виконані з 
кераміки, скла, соломи, оздоблені бурштином тканини.
<p> 
Серед коштовних експонатів – бурштинова корона, яка була символом конкурсу краси «Бурштинова корона України». Його організатори вирішили передати корону до музею. 
Найновіший експонат – колекційні шахи, виготовлені з бурштину у формі різноманітних комах.
<p> 
<p>	Музей проводить екскурсію «Сонячне диво – бурштин», театралізовану екскурсію для дітей «Подорож бурштиновою стежинкою», нічну екскурсія «Бурштинова казка» (метод 
проведення – анімація, організовується за попереднім замовленням).
<p> 
Музей бурштину перетворив Рівненський будинок вчених у справжній бурштиновий палац. Тут також діє бурштинова крамниця.
<p> 
Музей працює щодня, крім понеділка та суботи, з 10. 00 до 18. 00. Каса – з 10. 00 до 17. 30.</div>
